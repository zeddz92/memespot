import React from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList, AsyncStorage } from 'react-native';
import MainNavigator from './app/navigation/MainNavigator';
import NavigatorService from './app/navigation/navigator';
import { NavigationActions } from 'react-navigation';

import { AppLoading, Asset, Font } from 'expo';
import { MaterialCommunityIcons, MaterialIcons, Entypo, Ionicons, SimpleLineIcons } from '@expo/vector-icons';
import configureStore from './app/store/configureStore';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import ModalHost from 'expo/src/modal/ModalHost';
import FCM, { NotificationActionType } from "react-native-fcm";
import { registerKilledListener, registerAppListener } from "./app/gcm/listener";
import { api } from "./app/utils/api";

console.disableYellowBox = true;


function cacheImages(images) {
    return images.map(image => {
        if (typeof image === 'string') {
            return Asset.fromModule(image).downloadAsync();
        } else {
            return Asset.fromModule(image).downloadAsync();
        }
    });
}


function cacheFonts(fonts) {
    return fonts.map(font => Font.loadAsync(font));
}

registerKilledListener();

export default class App extends React.Component {
    state = {
        isReady: false,
    };

    async componentDidMount() {
        let { store, persistor } = configureStore();

        // alert(JSON.stringify(store));
        // FCM.createNotificationChannel({
        //     id: 'default',
        //     name: 'Default',
        //     description: 'used for example',
        //     priority: 'high'
        // })
        // console.log(this.refs.navigator.navigate("NewPost"))

        registerAppListener(NavigatorService);
        // FCM.getInitialNotification().then(notif => {
        //     this.setState({
        //         initNotif: notif
        //     });
        //     if (notif && notif.targetScreen === "detail") {
        //         setTimeout(() => {
        //             this.props.navigation.navigate("Detail");
        //         }, 500);
        //     }
        // });

        try {
            let result = await FCM.requestPermissions({
                badge: false,
                sound: true,
                alert: true
            });
        } catch (e) {
            console.error(e);
        }

        const oldToken = await AsyncStorage.getItem("deviceToken");


        FCM.getFCMToken().then(token => {
            AsyncStorage.setItem('deviceToken', token);
            if (oldToken !== token) {
                this.attemptUpdateDeviceToken(token);
            }

            console.log("TOKEN (getFCMToken)", token);

        });

        if (Platform.OS === "ios") {
            FCM.getAPNSToken().then(token => {
                AsyncStorage.setItem('deviceToken', token);
                if (oldToken !== token) {
                    this.attemptUpdateDeviceToken(token);
                }

                console.log("APNS TOKEN (getFCMToken)", token);
            });
        }

        // topic example
        // FCM.subscribeToTopic('sometopic')
        // FCM.unsubscribeFromTopic('sometopic')
    }


    async attemptUpdateDeviceToken (token) {
        const userIsLogged = await AsyncStorage.getItem("userId");
        if (userIsLogged && token) {
            api.user.updateDeviceToken(userIsLogged, token).then(_ => {

            }).catch(error => {
                alert(error);
            });
        }
    }


    static async _loadAssetsAsync() {
        const imageAssets = cacheImages([
            require('./app/assets/img/profile.png')
        ]);

        const fontAssets = cacheFonts([
            MaterialCommunityIcons.font,
            MaterialIcons.font,
            Ionicons.font,
            Entypo.font,
            SimpleLineIcons.font,
            { "roboto-mono-regular": require('./app/assets/fonts/RobotoMono/RobotoMono-Regular.ttf') },
            { "roboto-mono-bold": require('./app/assets/fonts/RobotoMono/RobotoMono-Bold.ttf') },
            { "roboto-mono-medium": require('./app/assets/fonts/RobotoMono/RobotoMono-Medium.ttf') },
            { "roboto-mono-thin": require('./app/assets/fonts/RobotoMono/RobotoMono-Thin.ttf') },

            { "montserrat-regular": require('./app/assets/fonts/Montserrat/Montserrat-Regular.ttf') },
            { "montserrat-bold": require('./app/assets/fonts/Montserrat/Montserrat-Bold.ttf') },
            { "montserrat-semi-bold": require('./app/assets/fonts/Montserrat/Montserrat-SemiBold.ttf') },

            { "montserrat-medium": require('./app/assets/fonts/Montserrat/Montserrat-Medium.ttf') },
            { "montserrat-thin": require('./app/assets/fonts/Montserrat/Montserrat-Thin.ttf') }
            ,

        ]);

        await Promise.all([...imageAssets, ...fontAssets]);
    }

    render() {
        let { store, persistor } = configureStore();

        if (!this.state.isReady) {
            return (
                <AppLoading
                    startAsync={App._loadAssetsAsync}
                    onFinish={() => this.setState({ isReady: true })}
                    onError={console.warn}
                />
            );
        }
        return (
            <ModalHost>
                <Provider store={store}>
                    <PersistGate persistor={persistor}>
                        <MainNavigator ref={navigatorRef => {
                            NavigatorService.setContainer(navigatorRef);
                        }} />
                    </PersistGate>
                </Provider>
            </ModalHost>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 40,
        /*  alignItems: 'center',
          justifyContent: 'center',*/
    },
});
