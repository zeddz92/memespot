import * as auth from './resources/auth';
import * as post from './resources/post';
import * as comment from './resources/comment';
import * as follow from './resources/follow';
import * as tag from './resources/tag';
import * as user from './resources/user';

export const getAll = () => {

}


export const api = {
    auth,
    post,
    comment,
    follow,
    tag,
    user
};