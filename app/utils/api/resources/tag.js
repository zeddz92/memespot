import {url, headers} from '../config';

export const getAll = (page = 1, perPage = 15, searchTerm = null) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/tags?searchTerm=${searchTerm}&page=${page}&per_page=${perPage}`, {headers})
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};