import { url, headers } from '../config';

export const update = (userId, profilePicture, name, username, birthDate, shortBio) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}`, {
            method: 'PUT',
            headers: {
                ...headers,
            },
            body: JSON.stringify({
                name,
                profile_picture: profilePicture || null,
                username,
                dob: birthDate || null,
                short_bio: shortBio || null
            })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};

export const updateSetting = (userId, setting, value) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}/settings`, {
            method: 'PUT',
            headers: {
                ...headers,
            },
            body: JSON.stringify({
                setting,
                value
            })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));

    });
};

export const updateDeviceToken = (userId, token) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}`, {
            method: 'PUT',
            headers: {
                ...headers,
            },
            body: JSON.stringify({
                token
            })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};

export const getUser = (userId) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}`, {headers})
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
}





