import { url, headers } from '../config';

export const getAll = (userId, page = 1, search, category = null, perPage = 15) => {
    return new Promise((resolve, reject) => {
        console.log(`${url}/posts?page=${page}&per_page=${perPage}&category=${category}&search=${search}`);
        fetch(`${url}/posts?page=${page}&per_page=${perPage}&category=${category}&search=${search}`, {
            headers: {
                ...headers,
                user_id: userId,
            }
        })
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};

export const getPost = (postId) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/posts/${postId}`, {
            headers: {
                ...headers,
            }
        })
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};

export const getPosts = (userId, search, page = 1, category, perPage = 15) => {

    switch (category) {
        case "HOT":
        case "FRESH":
        case "TRENDING":
            return getAll(userId, page, search, category, perPage);
        case "OWN":
        case "COMMENTS":
        case "FAVORITES":
            return getUserPosts(userId, page, category, perPage);
        case "FEED":
            return getUserFeeds(userId, page, perPage);

        default:
            return getAll(page, search, category, perPage);
    }

};


export const getUserFeeds = (userId, page = 1, perPage = 15) => {

    console.log(`${url}/users/${userId}/feed?page=${page}&per_page=${perPage}`);
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}/feed?page=${page}&per_page=${perPage}`, {
            headers: {
                ...headers,
                user_id: userId
            },
        })
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};


export const getUserPosts = (userId, page = 1, category = null, perPage = 15) => {
    return new Promise((resolve, reject) => {

        console.log(`${url}/users/${userId}/posts?&category=${category}&page=${page}&per_page=${perPage}`);
        fetch(`${url}/users/${userId}/posts?&category=${category}&page=${page}&per_page=${perPage}`, {
            headers: {
                ...headers,
                user_id: userId,
            }
        })
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};


export const create = (userId, media, title, tags = null) => {
    return new Promise((resolve, reject) => {
        const data = new FormData();

        data.append("title", title);
        data.append("user_id", userId);
        if (tags) {
            data.append("tags", tags);
        }

        data.append('media', {
            uri: media,
            name: 'media',
            type: 'image/png'
        });


        fetch(`${url}/posts`, {
            method: 'POST',
            body: data
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};

export const feedback = (userId, postId, feedback) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/posts/${postId}/feedbacks`, {
            method: 'POST',
            headers: {
                ...headers,
                user_id: userId
            },
            body: JSON.stringify({ user_id: userId, post_id: postId, feedback })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};

export const destroy = (userId, postId) => {

    // alert(`${postId} ${commentId}`);
    return new Promise((resolve, reject) => {
        console.log(`${url}/posts/${postId}`);
        fetch(`${url}/posts/${postId}`, {
            method: 'DELETE',
            headers: {
                ...headers,
                user_id: userId
            },
            body: JSON.stringify({ user_id: userId })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};

export const strike = (userId, postId, reasonId) => {

    // alert(`${postId} ${commentId}`);
    return new Promise((resolve, reject) => {
        console.log(`${url}/posts/${postId}/strike`);
        fetch(`${url}/posts/${postId}/strikes`, {
            method: 'POST',
            headers: {
                ...headers,
                user_id: userId
            },
            body: JSON.stringify({ user_id: userId, reason_id: reasonId })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};



export const handleFavorites = (userId, postId, alreadySaved) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}/favorites`, {
            method: alreadySaved ? 'POST' : 'DELETE',
            headers: {
                ...headers,
                user_id: userId
            },
            body: JSON.stringify({ post_id: postId })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};
