import {url, headers} from '../config';
import queryString from 'query-string'

export const login = (email, password, token) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/login`, {
            method: 'POST',
            headers: {
                ...headers,
            },
            body: JSON.stringify({email, password, token})
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));

    });
};


export const changePassword = (userId, currentPassword, password, passwordConfirmation) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}/change_password`, {
            method: 'POST',
            headers: {
                ...headers,
            },
            body: JSON.stringify({
                current_password: currentPassword,
                password,
                password_confirmation: passwordConfirmation
            })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));

    });
};

export const register = (name, username, email, password, token) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/register`, {
            method: 'POST',
            headers: {
                ...headers,
            },
            body: JSON.stringify({
                name,
                username,
                email,
                password,
                device_registration_token: token
            })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};

export const loginWithSocialMedia = (obj) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/login_social`, {
            method: 'POST',
            headers: {
                ...headers,
            },
            body: JSON.stringify({...obj})
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));

    });
};