import { url, headers } from '../config';




export const get = (id) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/comments/${id}`, {
            headers: {
                ...headers,
            }
        })
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};

export const getAll = (userId, postId, page = 1, sort = null, perPage = 6) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/posts/${postId}/comments?page=${page}&per_page=${perPage}&sort=${sort}`, {
            headers: {
                ...headers,
                user_id: userId
            }
        })
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};


export const getReplies = (id, page = 1, sort = null, perPage = 6) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/comments/${id}/replies?page=${page}&per_page=${perPage}&sort=${sort}`, {
            headers: {
                ...headers,
                user_id: 1
            }
        })
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};

export const feedback = (userId, commentId, feedback) => {
    return new Promise((resolve, reject) => {
        console.log(`${url}/comments/${commentId}/feedbacks`);
        fetch(`${url}/comments/${commentId}/feedbacks`, {
            method: 'POST',
            headers: {
                ...headers,
                user_id: userId
            },
            body: JSON.stringify({ user_id: userId, action: feedback })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};


export const create = (postId, userId, text, media = null, parentId = null) => {
    return new Promise((resolve, reject) => {


        // console.log(`${url}/posts/${postId}/comments`);

        const data = new FormData();
        data.append("user_id", userId);
        data.append("text", text);
        if (parentId) {
            data.append("parent_id", parentId);
        }
        if (media) {
            data.append('media', {
                uri: media,
                name: 'media',
                type: 'image/png'
            });
        }

        fetch(`${url}/posts/${postId}/comments`, {
            method: 'POST',
            headers: {
                ...headers,
                'Content-Type': 'multipart/form-data',
                user_id: userId
            },
            body: data
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};

export const update = (postId, commentId, userId, text, media = null) => {
    return new Promise((resolve, reject) => {


        const data = new FormData();
        data.append("user_id", userId);
        data.append("text", text);

        if (media) {
            data.append('media', {
                uri: media,
                name: 'media',
                type: 'image/png'
            });
        }

        fetch(`${url}/posts/${postId}/comments/${commentId}`, {
            method: 'PUT',
            headers: {
                ...headers,
                user_id: userId
            },
            body: JSON.stringify({ text })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};


export const destroy = (userId, postId, commentId) => {

    // alert(`${postId} ${commentId}`);
    return new Promise((resolve, reject) => {
        console.log(`${url}/posts/${postId}/comments/${commentId}`);
        fetch(`${url}/posts/${postId}/comments/${commentId}`, {
            method: 'DELETE',
            headers: {
                ...headers,
                user_id: userId
            },
            body: JSON.stringify({ user_id: userId })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};


export const strike = (userId, commentId, reasonId) => {

    // alert(`${postId} ${commentId}`);
    return new Promise((resolve, reject) => {
        console.log(`${url}/comments/${commentId}/strike`);
        fetch(`${url}/comments/${commentId}/strikes`, {
            method: 'POST',
            headers: {
                ...headers,
                user_id: userId
            },
            body: JSON.stringify({ user_id: userId, reason_id: reasonId })
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
};

