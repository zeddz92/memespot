import {url, headers} from '../config';

export const getFollowers = (userId, page = 1, perPage = 15) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}/followers?page=${page}&per_page=${perPage}`, {headers})
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};

export const getFollows = (criteria, userId, page = 1, perPage = 15) => {
    switch (criteria) {
        case "FOLLOWERS":
            return getFollowers(userId, page, perPage);
        case "FOLLOWING":
            return getFollowing(userId, page, perPage)
    }
}

export const getFollowing = (userId, page = 1, perPage = 15) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/${userId}/following?page=${page}&per_page=${perPage}`, {headers})
            .then(res => resolve(res.json()))
            .catch(error => {
                reject(error)
            });
    });
};


export const followUser = (followerId, userId) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/following/${followerId}`, {
            method: 'POST',
            headers: {
                ...headers,
            },
            body: JSON.stringify({user_id: userId})
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
}

export const unFollowUser = (followerId, userId) => {
    return new Promise((resolve, reject) => {
        fetch(`${url}/users/following/${followerId}`, {
            method: 'DELETE',
            headers: {
                ...headers,
            },
            body: JSON.stringify({user_id: userId})
        })
            .then(res => resolve(res.json()))
            .catch(error => reject(error));
    });
}