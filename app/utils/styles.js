import {Platform, StyleSheet} from 'react-native';
import * as colors from './colors';

export const theme = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#FFFEFF',
    },


    loader: {
      marginTop: 15,
    },

    divider: {
        marginTop: 10,
        marginBottom: 10,
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "#cbcbcb"
    },


    // Position ------------------------------------------------------

    center: {
        alignItems: 'center',
        justifyContent: 'center',
    },

    row: {
      flexDirection: 'row'
    },

    footer: {
        justifyContent: 'flex-end',
    },

    input: {
        borderBottomWidth: 1,
        borderColor: colors.colorBorder,
        fontSize: 18,
        // height: 50,
        marginBottom: 10,

    },

    // Images
    logo: {
        marginTop: 15,
        resizeMode: 'contain',
        marginBottom: 35,
        width: '100%',
        height: 70
    },

    // Inputs ------------------------------------------------------

    inputUnderLined: {
        borderBottomWidth: 1,
        borderColor: colors.colorBorder,
        fontSize: 18,
        height: 50,
        marginBottom: 10,
    },


    // Text ------------------------------------------------------

    bold: {
        fontFamily: Platform.OS === 'android' ? 'roboto-mono-bold' : 'montserrat-bold',
    },

    regular: {
        fontFamily: Platform.OS === 'android' ? 'roboto-mono-regular' : 'montserrat-regular',
    },

    light: {
        fontFamily: Platform.OS === 'android' ? 'roboto-mono-light' : 'montserrat-light',
    },

    medium: {
        fontFamily: Platform.OS === 'android' ? 'roboto-mono-medium' : 'montserrat-semi-bold',
    },

    boldText: {
        fontWeight: 'bold'
    },

    link: {
        color: colors.colorLink,
        fontSize: 14,
        // fontFamily: 'roboto-mono-bold'
    },

    font: {
        color: '#373C3F',
        // fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
        // fontFamily: 'roboto-mono-bold'
    },

    // Buttons ------------------------------------------------------
    buttonLoadingRight: {
        padding: 13,
        flexDirection: 'row',
        backgroundColor: colors.colorPrimary,
        alignItems: 'center',
        justifyContent: 'center'

    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        // elevation: 2,
        borderRadius: 4,
        width: 150,
        padding: 15,
        margin: 15,
        backgroundColor: colors.colorPrimary
    },

    buttonText: {
        fontWeight: 'bold',
        color: colors.white,
    },

});