export const login = {
    "status": {
        "type": "success",
        "message": "Success",
        "code": 200,
        "error": false
    },
    "data":
        {
            "status": "Authenticated",
            "user": {
                "id": 1,
                "name": "Jhon Doe",
                "profile_image": "https://www.ienglishstatus.com/wp-content/uploads/2018/04/Anonymous-Whatsapp-profile-picture.jpg",
                "username": "jDoe",
                "email": "jDoe@gmail.com"
            },
            "summary": {
              "posts": 90,
              "followers": 200,
              "following": 120,
            },
            "configuration": {
                "push_notifications":{
                    "replies_to_comments": false,
                    "replies_to_posts": true,
                    "likes_to_comments": false,
                    "likes_to_posts": true,
                    "follow_request": true
                }
            },
            "return_to_url": null,
            "session_token": "9x8869x31134x7906x6x54474x21x18xxx90857x"
        }

};

