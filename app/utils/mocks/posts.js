export const posts = {
    _metadata:
        {
            page: 1,
            per_page: 10,
            page_count: 5,
            total_count: 3,
        },
    items: [
        {
            id: 0,
            title: '',
            tags: "NSFW, Video",
            user: {
                id: 2,
                username: 'marlon_roy',
                profile_image: 'https://img.wonderhowto.com/img/15/71/63427020936322/0/change-your-facebook-profile-picture-your-favorite-cartoon-character.w1456.jpg'
            },
            comments: 33,
            media: {
                width: 1280,
                height: 720,
                type: 3,
                url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4',
                thumbnail: 'patht-to-thumnail',
                extension: 'mp4'
            },
            likes: 56,
            dislikes: 22,
            upVoted: 0, // 0 nothing done; 1 liked ; 2 disliked
            commented: true,
            time_elapsed: '3h',
            timestamp: "59328472384734",
        },
        {
            id: 1,
            title: '',
            tags: 'GIF, Hot',
            media: {
                width: 500,
                height: 281,
                type: 2,
                url: 'https://media0.giphy.com/media/3YJ2Twr4r5fMHTRJpX/giphy.gif',
                thumbnail: 'patht-to-thumnail',
                extension: 'gif'
            },
            user: {
                id: 1,
                username: 'dNisesse',
                profile_image: 'http://images.all-free-download.com/images/graphiclarge/cute_little_dog_definition_picture_168910.jpg'
            },
            comments: 30,
            likes: 20,
            dislikes: 3,
            upVoted: 2, // 0 nothing done; 1 liked ; 2 disliked
            commented: true,
            timestamp: "59328472384734",
        },


        {
            id: 2,
            title: 'So hot outside!!',
            tags: "Everyday",
            media_type: 1, // Image
            media_url: 'https://pbs.twimg.com/media/CIxILfXW8AAVE-x.jpg',
            media: {
                width: 576,
                height: 576,
                type: 1,
                url: 'https://pbs.twimg.com/media/CIxILfXW8AAVE-x.jpg',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            user: {
                id: 3,
                username: 'jdoe',
                profile_image: 'https://www.ienglishstatus.com/wp-content/uploads/2018/04/Anonymous-Whatsapp-profile-picture.jpg'
            },
            comments: 140,
            likes: 70,
            dislikes: 30,
            upVoted: 2, // 0 nothing done; 1 liked ; 2 disliked
            commented: true,
            time_elapsed: '1d',
            timestamp: "59328472384734",
        },

        {
            id: 3,
            title: 'Very funny',
            tags: "Everyday",
            media_type: 1, // Image
            media_url: 'https://imgix.ranker.com/user_node_img/50040/1000791781/original/the-goggles-they-do-nothing-wait-i-have-no-goggles-photo-u1?w=650&q=50&fm=jpg&fit=crop&crop=faces',
            media: {
                width: 650,
                height: 490,
                url: 'https://imgix.ranker.com/user_node_img/50040/1000791781/original/the-goggles-they-do-nothing-wait-i-have-no-goggles-photo-u1?w=650&q=50&fm=jpg&fit=crop&crop=faces',
                type: 1,
                thumbnail: 'patht-to-thumnail',
                extension: 'mp4'
            },
            user: {
                id: 3,
                username: 'jdoe',
                profile_image: 'https://www.ienglishstatus.com/wp-content/uploads/2018/04/Anonymous-Whatsapp-profile-picture.jpg'
            },
            comments: 140,
            likes: 70,
            dislikes: 30,
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
            commented: true,
            time_elapsed: '2d',
            timestamp: "59328472384734",
        },


        {
            id: 4,
            title: '',
            media: {
                width: 509,
                height: 602,
                url: 'https://tr3.cbsistatic.com/hub/i/2017/03/23/fd2cfe38-d0b2-4fee-9450-03b3d5f521d8/c3f153e108263291254737cffd045982/funnytechmemes1thumb800.jpg',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            tags: "NSFW, Video",
            user: {
                id: 2,
                username: 'marlon_roy',
                    profile_image: 'https://img.wonderhowto.com/img/15/71/63427020936322/0/change-your-facebook-profile-picture-your-favorite-cartoon-character.w1456.jpg'
            },
            comments: 33,
            likes: 56,
            dislikes: 22,
            upVoted: 0, // 0 nothing done; 1 liked ; 2 disliked
            commented: true,
            time_elapsed: '3m',
            timestamp: "59328472384734",
        },


        {
            id: 5,
            title: 'Hhahahaahha xD!!',
            tags: "Dragon ball",
            media_type: 1, // Image
            media_url: 'https://pm1.narvii.com/6381/fd2e24e8b2323d30b38e4095254411a869752d5c_hq.jpg',
            media: {
                width: 362,
                height: 512,
                type: 1,
                url: 'https://pm1.narvii.com/6381/fd2e24e8b2323d30b38e4095254411a869752d5c_hq.jpg',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            user: {
                id: 3,
                username: 'jdoe',
                profile_image: 'https://www.ienglishstatus.com/wp-content/uploads/2018/04/Anonymous-Whatsapp-profile-picture.jpg'
            },
            comments: 140,
            likes: 70,
            dislikes: 30,
            upVoted: 2, // 0 nothing done; 1 liked ; 2 disliked
            commented: true,
            time_elapsed: '13s',
            timestamp: "59328472384734",
        },

    ]
};

