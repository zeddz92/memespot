export const comments = {
    _metadata:
        {
            page: 1,
            per_page: 10,
            page_count: 5,
            total_count: 3,
        },
    items: [
        {
            id: 1,
            user: {
                username: 'jdoe',
                profile_image: 'https://www.ienglishstatus.com/wp-content/uploads/2018/04/Anonymous-Whatsapp-profile-picture.jpg'
            },
            replies: 2,
            likes: 70,
            dislikes: 30,
            text: 'I gotta be kidding me',
            time_elapsed: '3h',
            timestamp: "59328472384734",
            media: {
                width: 658,
                height: 425,
                type: 1,
                url: 'https://static.timesofisrael.com/www/uploads/2016/05/%D7%90%D7%9D%D7%A6.jpg',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        },
        /*{
            id: 2,
            user: {
                username: 'marlon_roy',
                profile_image: 'https://img.wonderhowto.com/img/15/71/63427020936322/0/change-your-facebook-profile-picture-your-favorite-cartoon-character.w1456.jpg'
            },
            replies: 0,
            likes: 2,
            dislikes: 0,
            text: 'Lets see what does other pages think about this',
            time_elapsed: '2h',
            timestamp: "59328472384734",
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        },
        {
            id: 3,
            user: {
                username: 'dNisesse',
                profile_image: 'http://images.all-free-download.com/images/graphiclarge/cute_little_dog_definition_picture_168910.jpg'
            },
            replies: 2,
            likes: 50,
            dislikes: 14,
            text: 'I had to spit everything i had.',
            media: {
                width: 475,
                height: 272,
                type: 2,
                url: 'https://media1.giphy.com/media/26FPnj46RYsIWgYLe/giphy.gif',
                thumbnail: 'patht-to-thumnail',
                extension: 'gif'
            },
            timestamp: "59328472384734",
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        }*/
    ]
};

export const replies = {
    "1": [
        {
            id: 10,
            user: {
                username: 'jPabloD',
                profile_image: 'http://profilepics.xyz/wp-content/uploads/2018/04/Profile-pic-9.jpg'
            },
            replies: 140,
            likes: 70,
            dislikes: 30,
            text: 'Learn how to spell champion',
            time_elapsed: '3h',
            timestamp: "59328472384734",
            media: {
                width: 331,
                height: 225,
                type: 2,
                url: 'https://media2.giphy.com/media/6IciAdVhhZ2G4/giphy.gif',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        },
        {
            id: 11,
            user: {
                username: 'restiCris',
                profile_image: 'https://ichef.bbci.co.uk/news/624/cpsprodpb/2CEE/production/_87020511_russia_putin2.jpg'
            },
            replies: 3,
            likes: 10,
            dislikes: 3,
            text: 'No he is not you moron',
            time_elapsed: '6s',
            timestamp: "59328472384734",
            media: {
                width: 658,
                height: 425,
                type: 1,
                url: 'https://static.timesofisrael.com/www/uploads/2016/05/%D7%90%D7%9D%D7%A6.jpg',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        }],
    "3": [
        {
            id: 20,
            user: {
                username: 'thrump_card',
                profile_image: 'http://stories.gettyimages.com/wp-content/uploads/2016/08/GettyImages-586904512.jpg'
            },
            replies: 3,
            likes: 10,
            dislikes: 3,
            text: 'They wont think sir',
            time_elapsed: '6s',
            timestamp: "59328472384734",
            media: {
                width: 800,
                height: 533,
                type: 2,
                url: 'https://media0.giphy.com/media/u0LxmF9QVeDoQ/giphy.gif',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        },
        {
            id: 21,
            user: {
                username: 'morgan_slaved_man',
                profile_image: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Gull_portrait_ca_usa.jpg'
            },
            replies: 3,
            likes: 10,
            dislikes: 3,
            text: 'No words for this nonsense',
            time_elapsed: '6s',
            timestamp: "59328472384734",
            media: {
                width: 658,
                height: 425,
                type: 1,
                url: 'https://static.timesofisrael.com/www/uploads/2016/05/%D7%90%D7%9D%D7%A6.jpg',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        }
    ],
    "10": [{
        id: 40,
        user: {
            username: 'morgan_slaved_man',
            profile_image: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Gull_portrait_ca_usa.jpg'
        },
        replies: 0,
        likes: 10,
        dislikes: 3,
        text: 'Responding to your comment',
        time_elapsed: '6s',
        timestamp: "59328472384734",
        media: {
            width: 658,
            height: 425,
            type: 1,
            url: 'https://static.timesofisrael.com/www/uploads/2016/05/%D7%90%D7%9D%D7%A6.jpg',
            thumbnail: 'patht-to-thumnail',
            extension: 'jpg'
        },
        upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
    },
        {
            id: 91,
            user: {
                username: 'dNisesse',
                profile_image: 'http://images.all-free-download.com/images/graphiclarge/cute_little_dog_definition_picture_168910.jpg'
            },
            replies: 0,
            likes: 20,
            dislikes: 0,
            text: 'I\'m fed up of writing comments',
            time_elapsed: '6s',
            timestamp: "59328472384734",
            media: {
                width: 658,
                height: 425,
                type: 1,
                url: 'https://static.timesofisrael.com/www/uploads/2016/05/%D7%90%D7%9D%D7%A6.jpg',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        }],
    "40": [{
        id: 50,
        user: {
            username: 'thrump_card',
            profile_image: 'http://stories.gettyimages.com/wp-content/uploads/2016/08/GettyImages-586904512.jpg'
        },
        replies: 0,
        likes: 10,
        dislikes: 3,
        text: 'Lets keep going till we explode',
        time_elapsed: '6s',
        timestamp: "59328472384734",
        media: {
            width: 658,
            height: 425,
            type: 1,
            url: 'https://static.timesofisrael.com/www/uploads/2016/05/%D7%90%D7%9D%D7%A6.jpg',
            thumbnail: 'patht-to-thumnail',
            extension: 'jpg'
        },
        upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
    },
        {
            id: 51,
            user: {
                username: 'jPabloD',
                profile_image: 'http://profilepics.xyz/wp-content/uploads/2018/04/Profile-pic-9.jpg'
            },
            replies: 140,
            likes: 0,
            dislikes: 0,
            text: 'Ok i got it, so stop...',
            time_elapsed: '3h',
            timestamp: "59328472384734",
            media: {
                width: 331,
                height: 225,
                type: 2,
                url: 'https://media2.giphy.com/media/6IciAdVhhZ2G4/giphy.gif',
                thumbnail: 'patht-to-thumnail',
                extension: 'jpg'
            },
            upVoted: 1, // 0 nothing done; 1 liked ; 2 disliked
        }]
};

const moreReplies = {

}
