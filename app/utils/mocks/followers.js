export const followers = {
    _metadata:
        {
            page: 1,
            per_page: 10,
            page_count: 5,
            total_count: 3,
        },
    items: [
        {
            id: 1,
            username: 'jdoe',
            profile_image: 'https://www.ienglishstatus.com/wp-content/uploads/2018/04/Anonymous-Whatsapp-profile-picture.jpg',
            name: 'Jhon Doe',
            following: true,
        },
        {
            id: 2,
            username: 'dNisesse',
            profile_image: 'http://images.all-free-download.com/images/graphiclarge/cute_little_dog_definition_picture_168910.jpg',
            name: 'Denisse graphy',
            following: false,
        },
        {
            id: 3,
            username: 'marlon_roy',
            profile_image: 'https://img.wonderhowto.com/img/15/71/63427020936322/0/change-your-facebook-profile-picture-your-favorite-cartoon-character.w1456.jpg',
            name: 'Marlon Roy',
            following: true,
        },
        {
            id: 4,
            username: 'uSPresident',
            profile_image: 'https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/09/21/104723838-GettyImages-850288240.1910x1000.jpg',
            name: 'Barack Obama',
            following: true,
        }
    ]
};

