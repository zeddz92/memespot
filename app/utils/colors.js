export const colorActions = "#9B999B";
// export const colorActions = "#807C84";

export const colorPrimary = "#3f51b5";
export const colorSecondary = "#C5092D";

export const colorDisable = "#c5cae8";

export const colorSnackBar = "#323232";
export const colorFilledHeart = "#FA0D1C"
export const colorLink = "#3E99ED";
export const colorLoader = "#585858";
export const white = "#FFF";
export const black = "#000";
export const lightBlue = "#48C8F4";
export const lightRed = "#F44336";
export const colorBorder = "#E8EAF6";

export const colorFacebook = '#3b5998';
export const colorGoogle = '#d34836';


export const colorSwitchThumb = "#2962FF";
export const colorSwitchOn = "#82B1FF";

// export const colorSettingsText = "#747474";
export const colorSettingsText = "#515151";
