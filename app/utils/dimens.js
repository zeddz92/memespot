import {Platform, Dimensions, DeviceInfo} from 'react-native';
export let {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');


// export const actionSmallSize = 20;

export const actionSize = 18;
export const actionSmallSize = 15;

export const blurRadius = Platform.OS === 'android' ? 1 : 3;



const keyboardVerticalOffsetIOS = DeviceInfo.isIPhoneX_deprecated ? 88 : 64;

export const profileImgSmallSize = 40;
export const keyboardVerticalOffset = Platform.OS === "android" ? 80: keyboardVerticalOffsetIOS;
export const loginKeyboardVerticalOffset = Platform.OS === "android" ? 80: keyboardVerticalOffsetIOS;

export const getRadius = (size) => {
    return Platform.OS === 'android' ? (size * 2) : (size / 2);
};

export const getMediaHeight = (originalWidth, originalHeight) => {
    return viewportWidth * originalHeight / originalWidth;
}