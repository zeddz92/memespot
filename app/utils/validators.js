export const validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

export const validateMinLength = (field) => {
    const text= field.text;
    console.log(`minlenght: ${text.length}, ${field.name}`);
    return (text.length >= field.minLength);
};

const validateField = field => {
    let isValidType = true;
    let isValidLength = true;

    if(field.hasOwnProperty('type')) {
        switch (field.type) {
            case 'email':
                isValidType = validateEmail(field.text);
                break;
            default:
                isValidType = true;
                break;
        }
    }

    if(field.hasOwnProperty('minLength')) {
        isValidLength = validateMinLength(field)
    }

    return isValidType && isValidLength;

};


export const validate = (fields) => {

    let isValid = true;
    fields.forEach((field) => {
        if(!validateField(field)) {
            isValid = false;
        }
    });

    return isValid;


}