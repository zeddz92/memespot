import React from 'react';
import {TabNavigator, TabView, TabBarTop, SafeAreaView} from 'react-navigation';
import Home from '../screens/Home';
import SvgUri from 'react-native-svg-uri';
import {Platform} from 'react-native';
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import PostList from '../screens/PostList';
import * as colors from "../utils/colors";


const iconSize = 20;


const ProfileTabs = TabNavigator({
        UserPost: {
            screen: (props) => <PostList {...props} category="OWN"/>,
            navigationOptions: {
                tabBarLabel: "Posts",
                tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name="view-grid" size={iconSize} color={tintColor}/>
            },
        },
        UserComments: {
            screen: (props) => <PostList {...props} category="COMMENTS"/>,
            navigationOptions: {
                tabBarLabel: "Comments",
                tabBarIcon: ({tintColor}) =>  <MaterialIcons name="mode-comment" size={iconSize} color={tintColor}/>

            },
        },

        UserFavoritePosts: {
            screen: (props) => <PostList {...props} category="FAVORITES"/>,
            navigationOptions: {
                tabBarLabel: "Favorites",
                tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name="heart" size={iconSize} color={tintColor}/>
            },
        },

    },
    {
        tabBarComponent: TabBarTop,
        tabBarPosition: 'top',
        lazyLoad: true,
        tabBarOptions: {
            showLabel: false,
            showIcon: true,
            activeBackgroundColor: 'white',
            activeTintColor: '#3897F0',
            inactiveTintColor: '#969796',
            inactiveBackgroundColor: 'white',
            style: {
                elevation: 0,
                backgroundColor: 'white',
                borderTopWidth: 0.4,
                borderBottomWidth: 0.4,
                // paddingTop: 10,
                borderColor: '#E2E3E2',
            },
            labelStyle: {
                fontWeight: 'bold',
                color: 'grey',
                fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
            },
            indicatorStyle: {
                // backgroundColor: 'none',
                backgroundColor: colors.lightBlue,
            },
        },
        navigationOptions: ({ navigation }) => ({
            headerTintColor: "#CBCBCB",
            headerStyle: {
                backgroundColor: "white",
            }
        }),
        swipeEnabled: false,
        animationEnabled: false,
        backBehavior: 'none',
    });


export default ProfileTabs;