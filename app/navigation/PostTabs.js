import React from 'react';
import Home from '../screens/Home';
import {TabNavigator, TabView, TabBarTop} from 'react-navigation';
import {Platform} from 'react-native';

const iconSize = 25;
import {MaterialIcons, MaterialCommunityIcons} from '@expo/vector-icons';
import PostList from '../screens/PostList';
import * as colors from "../utils/colors";
import {theme} from "../utils/styles";

import configureStore from '../store/configureStore';

let shouldRemoveFeed = false;
export const tabs = {

    Fresh: {
        screen: (props) => <PostList {...props} category="FRESH"/>,
        navigationOptions: {
            tabBarLabel: "Fresh",
            tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name="new-box" size={iconSize} color={tintColor}/>
        },
    },
    Feed: {
        screen: (props) => <PostList {...props} category="FEED"/>,
        navigationOptions: {
            tabBarLabel: "Feed",
            tabBarIcon: ({tintColor}) => <MaterialIcons name="people" size={iconSize} color={tintColor}/>
        },
    },
    Hot: {
        screen: (props) => <PostList {...props} category="HOT"/>,
        navigationOptions: {
            tabBarLabel: "Hot",
            tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name="fire" size={iconSize} color={tintColor}/>

        },
    },

    Trending: {
        screen: (props) => <PostList {...props} category="TRENDING"/>,
        navigationOptions: {
            tabBarLabel: "Trending",
            tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name="trending-up" size={iconSize} color={tintColor}/>
        },
    },

};

const options = {
    tabBarComponent: TabBarTop,
    tabBarPosition: 'top',
    lazyLoad: false,
    tabBarOptions: {
        showLabel: false,
        showIcon: true,
        activeBackgroundColor: 'white',
        activeTintColor: '#3897F0',
        inactiveTintColor: colors.black,
        inactiveBackgroundColor: 'white',
        style: {

            // elevation: 6,
            backgroundColor: 'white',
            borderTopWidth: 0,
            borderBottomWidth: 0,

            // borderBottomWidth: 1,
            // borderColor: '#E2E3E2',
        },
        labelStyle: {
            fontWeight: 'bold',
            fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
        },

        indicatorStyle: {
            backgroundColor: colors.lightBlue,
        },
    },
    navigationOptions: ({navigation}) => ({
        headerTintColor: "#CBCBCB",
        headerStyle: {
            backgroundColor: "white",

        },
    }),
    swipeEnabled: true,
    animationEnabled: true,
    backBehavior: 'none',
}


// delete tabs.Feed;

const tabWithoutFeed = {...tabs};
delete tabWithoutFeed.Feed;

export const PostTabsWithoutFeed = TabNavigator(tabWithoutFeed, options);



export const PostTabsWithFeed = TabNavigator(tabs, options);

