import React from 'react';
import {TabNavigator, TabView, TabBarBottom, NavigationActions} from 'react-navigation';
import {Image, Text, AsyncStorage} from 'react-native';

import * as colors from '../utils/colors';

import Home from '../screens/Home';
import Search from '../screens/Search';
import Profile from '../screens/Profile';
import Welcome from '../screens/ProfileTab';
import NewPost from '../screens/NewPost';
import Notifications from '../screens/Notifications';

import {MaterialCommunityIcons, MaterialIcons, FontAwesome, Feather} from '@expo/vector-icons';


import SvgUri from 'react-native-svg-uri';

const iconSize = 25;
//
// function Tabs (props) {
//
// }


const Tabs = TabNavigator({




    Home: {
        screen: Home,
        navigationOptions: {
            tabBarLabel: "Home",
            tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name="home" size={iconSize} color={tintColor}/>
        },
    },

    Search: {
        screen: Search,
        navigationOptions: {
            tabBarLabel: "Search",
            header: null,
            tabBarIcon: ({tintColor}) => <MaterialIcons name="search" size={iconSize} color={tintColor}/>
        },
    },


    NewPostBtn: {
        screen: NewPost,
        navigationOptions: {
            tabBarLabel: "New Post",
            tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name="pencil-circle" size={35} color={tintColor}/>
        },
    },
    Favorites: {
        screen: Notifications,
        navigationOptions: {
            tabBarLabel: "Activity",
            tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name="bell-ring" size={iconSize} color={tintColor}/>
        },
    },

    Profile: {
        screen: Welcome,
        navigationOptions: {
            tabBarLabel: "Profile",
            tabBarIcon: ({tintColor}) => <MaterialIcons name="person" size={iconSize} color={tintColor}/>
        },
    },

    /* Profile: {
         screen: Welcome,
         navigationOptions: {
             tabBarLabel:  "Profile",
             tabBarIcon: ({tintColor}) => <SvgUri fill={tintColor} width={iconSize} height={iconSize} source={require('../assets/icons/user.svg')}/>
         },
     },*/


}, {
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    lazyLoad: true,
    tabBarOptions: {
        showLabel: false,
        activeTintColor: "#000",
        style: {
            height: 56,
            shadowColor: 'rgba(0, 0, 0, 0.24)',
            shadowOffset: {
                width: 0,
                height: 3
            },
            shadowRadius: 6,
            shadowOpacity: 1
        }
    },
    navigationOptions: ({navigation}) => ({
        headerMode: 'screen',

        tabBarOnPress: ({previousScene, scene, jumpToIndex}) => {
            const {focused, index} = scene;
            if (!focused) {
                AsyncStorage.getItem('userId').then(userId => {
                    if (index === 2) {
                        if(userId) {
                            navigation.dispatch(
                                NavigationActions.navigate({routeName: "NewPost"})
                            );
                        } else {
                            navigation.dispatch(
                                NavigationActions.navigate({routeName: "Login"})
                            );
                        }



                    } else {
                        jumpToIndex(index);
                    }
                });

            }
        },
        headerTitle: (<Text style={{flex: 1, fontSize: 20, color: 'white', fontWeight: 'bold', textAlign: 'center'}}>APP
            NAME</Text>),
        headerTintColor: "#CBCBCB",
        headerStyle: {
            elevation: 0,
            backgroundColor: colors.colorPrimary,
        }
    }),
    swipeEnabled: false,
    animationEnabled: false,
    backBehavior: 'none',
});

export default Tabs;