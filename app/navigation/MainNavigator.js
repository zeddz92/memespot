import React from 'react';
import {StackNavigator} from 'react-navigation';
import Tabs from './Tabs';
import PostComments from '../screens/PostComments';
import Followers from '../screens/Followers';
import NewPost from '../screens/NewPost';
import SignUp from '../screens/SignUp';
import Login from '../screens/Login';
import Settings from '../screens/Settings';
import EditProfile from '../screens/Settings/EditProfile';
import ChangePassword from '../screens/Settings/ChangePassword';
import PushNotification from '../screens/Settings/PushNotification';

import MediaVisualizer from '../screens/MediaVisualizer';
import Profile from "../screens/Profile";
import UserProfile from "../screens/UserProfile";

import ProcessMedia from "../screens/ProcessMedia";

import AddTag from '../screens/AddTag';
import CommentDetail from '../screens/CommentDetail';


const MainNavigator = StackNavigator({

    Tabs: {
        screen: Tabs,
        navigationOptions: {
            headerLeft: null,
        }
    },

    CommentDetail: {
        screen: CommentDetail,
        navigationOptions: {

        }
    },
  
    NewPost: {
        screen: NewPost,
        navigationOptions: {
            title: 'New post',
        }
    },
    AddTag: {
        screen: AddTag,
        navigationOptions: {

        }
    },

   
    ProcessMedia: {
        screen: ProcessMedia,
        navigationOptions: {
            title: 'Edit',
            headerStyle: {
                backgroundColor: "#000",
                color: "#FFF"
            },
            headerTitleStyle: {
                color: "#FFF"
            }
        }
    },




    SignUp: {
        screen: SignUp,
        navigationOptions: {
            title: 'Join the fam',
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            title: 'Log in',
        }
    },

    EditProfile: {
        screen: EditProfile,
        navigationOptions: {
            title: 'Edit Profile',
        }
    },





    ChangePassword: {
        screen: ChangePassword,
        navigationOptions: {
            title: 'Change Password',
        }
    },




    UserProfile: {
        screen: UserProfile,
        navigationOptions: {
        }
    },

    Settings: {
        screen: Settings,
        navigationOptions: {
            title: 'Settings',
        }
    },

    PushNotification: {
        screen: PushNotification,
        navigationOptions: {
            title: 'Push Notifications',

        }
    },


    MediaVisualizer: {
        screen: MediaVisualizer,
        navigationOptions: {
            header: null,
        }
    },







    PostComments: {
        screen: PostComments,
        navigationOptions: {
            title: 'Comments',
            // headerLeft: null,
        }
    },





    Followers: {
        screen: Followers,
        navigationOptions: {
            title: 'Followers',
            // headerLeft: null,
        }
    },


});


export default MainNavigator;