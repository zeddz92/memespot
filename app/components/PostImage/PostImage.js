import React from 'react';
import styles from './styles';
import {Image, View, Text, TouchableOpacity} from 'react-native';

const PostImage = (props) => {
    return (
      <TouchableOpacity style={styles.container}>
          <Image
              source={{uri: 'https://afinde-production.s3.amazonaws.com/uploads/1dd616ad-f0d0-45d3-8909-24b578014654.jpg'}}
              style={styles.image}/>

      </TouchableOpacity>
    );
};

export default PostImage;
