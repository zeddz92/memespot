import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import * as dimens from '../../utils/dimens';
import * as colors from '../../utils/colors';

const padding = {
    paddingTop: 15,
    paddingStart: 10,
    paddingEnd: 10
};

export default StyleSheet.create({

    tag: {
        // backgroundColor: 'black',
        // borderRadius: 25,
        // margin: 5,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'


    },

    arrow: {
      // marginEnd: 10,
    },

    image: {
        width: 30,
        height: 30,
        marginEnd: 10,
        borderRadius: dimens.getRadius(30),
    },


    text: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#373C3F'
    },

    description: {
        color: 'gray',
        fontWeight: 'bold',
    },


    font: {
        color: 'white',
        fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
    },


});