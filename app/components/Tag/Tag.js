import React from 'react';
import styles from './styles';
import {Image, View, Text, TouchableOpacity} from 'react-native';
import {MaterialCommunityIcons, MaterialIcons, FontAwesome, Ionicons} from '@expo/vector-icons';

import * as colors from '../../utils/colors';
import * as dimens from '../../utils/dimens';
import {theme} from '../../utils/styles';


const Tag = (props) => {

    const {data, selectTag} = props;

    return (
        <TouchableOpacity
            onPress={() => selectTag(data)}
            style={styles.tag}>
            <View style={{flex: 1,flexDirection: 'row'}}>
                {/*<MaterialCommunityIcons style={{marginEnd: 10,}} name="at" size={30} color={colors.colorActions}/>*/}
                <Image
                    source={{uri: "https://vignette.wikia.nocookie.net/souleater/images/d/d5/Soul.png/revision/latest?cb=20120721172741"}}
                    style={styles.image}/>
                <View style={{flex: 1}}>
                    <Text style={[theme.font, styles.text]}>{data.name}</Text>
                    {/*<Text style={[styles.font, styles.description]}>{data.description}</Text>*/}
                </View>
            </View>

            <MaterialIcons style={styles.arrow}  color={colors.colorActions} name="keyboard-arrow-right" size={25}/>




        </TouchableOpacity>
    )

}

export default Tag;