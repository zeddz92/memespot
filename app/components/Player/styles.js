import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import {colorActions} from "../../utils/colors";
import * as colors from "../../utils/colors";
import * as dimens from "../../utils/dimens";


export default StyleSheet.create({
    mediaIcon: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
    player: {
        // backgroundColor: 'black',
        backgroundColor: 'rgba(0,0,0,0.8)',
        height: 200,
    },
    footer: {

        justifyContent: 'flex-end',
        // marginBottom: 15,
    },
    playButton: {
        backgroundColor: 'rgba(0, 0, 0, .5  )',
        width: 60,
        height: 60,
        borderRadius: dimens.getRadius(60),
        elevation: 12,
        /*  width: 40,
          height: 40,
          borderRadius: dimens.getRadius(40),
          backgroundColor: 'rgba(0, 0, 0, .8  )'*/
    },

    duration: {
        // fontWeight: 'bold',
        color: colors.white,
        fontSize: 12,

    },

    markerStyle: {
        width: 20,
        height: 30,
        // shadowOpacity: 0,
        borderRadius: 1,
        marginTop: 15,
        borderWidth: 2,
        borderColor: '#838486',
        backgroundColor: '#EAEAEA',
    },
    trackStyle: {
        height: 15,
        borderRadius: 1,
        backgroundColor: '#d5d8e8',
    },

    seekBarContainer: {
        position: 'absolute',
        bottom: 10,
        left: 0,
        right: 0,
        alignItems: 'center'
    },
    seekBar: {
        flex: 1,
        marginLeft: 10,
        marginRight:10


    },
});