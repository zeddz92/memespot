import React from 'react';
import styles from './styles';
import {
    Image,
    View,
    Text,
    TouchableWithoutFeedback,
    FlatList,
    TouchableOpacity,
    AppState,
    Slider,
    Animated
} from 'react-native';

import SvgUri from 'react-native-svg-uri';
import * as colors from '../../utils/colors';
import * as dimens from '../../utils/dimens';
import { theme } from '../../utils/styles';

import Media from '../Media';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';

import { withNavigation } from 'react-navigation';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

import { ImagePicker, DocumentPicker, Video, FileSystem, Audio } from 'expo';
import moment from "moment/moment";
import { getMediaHeight } from "../../utils/dimens";
import * as Animatable from 'react-native-animatable';

import PropTypes from 'prop-types';


import * as mime from 'react-native-mime-types';

class Player extends React.Component {
    constructor(props) {
        super(props);
        const { shouldPlay } = props;

        const mimeType = mime.lookup(props.source);
        let isAudio = true;
        if (mimeType) {
            isAudio = !mimeType ? true : mimeType.includes("audio");
        }


        this.state = {
            appState: AppState.currentState,
            source: props.source,

            isAudio,
            layoutVisible: false,
            isVideo: false,
            playBackEnded: false,
            playControlVisible: shouldPlay,
            fadeAnim: new Animated.Value(1),
            seekBarPosition: 0,
            seekStart: 0,
            currentPositionLabel: 0,
            isPlaying: shouldPlay,
            isSliding: false,
            seekEnd: 0,
        };


        this.shouldHidePlayBackBtn = true;
        this.fadeAnimDuration = 800;
        this.fadeAnimTimeOut = 2000;
        this.media = null;
    }


    prepare(data) {
        // this.setState({
        //     isAudio: !data.width,
        //     mediaHeight: data.height,
        //     mediaWidth: data.width
        // }, () => {
        //     this.media = this.refs.video
        // })


    }

    async componentDidMount() {
        const { mediaType, isAudio, source } = this.state;
        const { shouldPlay } = this.props;

        if (isAudio) {
            this.media = new Expo.Audio.Sound();
            await this.media.loadAsync({ uri: source },
                { shouldPlay: shouldPlay, rate: 1.0, isMuted: false },
                true).then((data) => {
                    this.setState({
                        seekEnd: Number(data.durationMillis)
                    })
                }).catch(err => {
                    alert(JSON.stringify(err))
                });

            this.media.setStatusAsync({ progressUpdateIntervalMillis: 1000 });
            this.media.setOnPlaybackStatusUpdate(this.onPlaybackStatusUpdate);


        } else {
            this.media = this.refs.video;
        }


        if (shouldPlay) {
            Animated.timing(this.state.fadeAnim, { toValue: 0, duration: this.fadeAnimDuration }).start(() => {
                this.setState({
                    playControlVisible: false
                })
            });
        }

        AppState.addEventListener('change', this._handleAppStateChange);


    }

    componentWillUnmount() {
        this.media.unloadAsync().then(_ => {
            Expo.FileSystem.deleteAsync(this.state.source);
        });
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            // this.media.playAsync();
        } else {
            this.media.pauseAsync();
        }
        this.setState({ appState: nextAppState });
    };


    // static getDerivedStateFromProps(props, state) {
    //     if(state.playControlVisible) {
    //
    //         setTimeout(() => {
    //             if (!state.isSliding) {
    //                 Animated.timing(state.fadeAnim, {
    //                     toValue: 0,
    //                     duration: 1000,
    //                 }).start(() => {
    //
    //                 });
    //                 return {
    //                     playControlVisible: false
    //                 }
    //             }
    //
    //         }, 2000);
    //
    //
    //     }
    //
    //     if (state.playBackEnded) {
    //         Animated.timing(state.fadeAnim, {
    //             toValue: 1,
    //             duration: 1000,
    //         }).start();
    //     }
    //
    //     return state;
    // }

    onPlaybackStatusUpdate = (status) => {

        const { seekEnd, playBackEnded, isVideo, isSliding } = this.state;

        if (!playBackEnded) {

            // console.log(status);
            if (status.isPlaying && !isSliding) {
                this.setState({
                    currentPositionLabel: status.positionMillis,
                    seekBarPosition: status.positionMillis,
                    // isPlaying: true,
                });
            }

            if (status.didJustFinish) {
                this.setState({
                    isPlaying: false,
                    playBackEnded: true,
                    playControlVisible: true
                }, () => {
                    Animated.timing(this.state.fadeAnim, {
                        toValue: 1,
                        duration: this.fadeAnimDuration,
                    }).start();
                });
            }
        }

    }

    onSeekBarValueChange = (value) => {
        this.setState({
            isSliding: true,
            currentPositionLabel: value,
            playControlVisible: false
            // seekBarPosition: value
        });
    };

    onSeekBarPositionChange = (value) => {
        const { isVideo, fadeAnim } = this.state;
        this.setState({
            seekBarPosition: value,
            currentPositionLabel: value,
            playBackEnded: false
        });

        if (!isVideo) {
            this.media.playFromPositionAsync(value).then(() => {
                this.setState({
                    isPlaying: true,
                    isSliding: false,
                }, () => {
                    setTimeout(() => {
                        if (this.state.isPlaying && !this.state.isSliding) {
                            this.setState({
                                playControlVisible: true
                            })
                        }
                    }, this.fadeAnimTimeOut);

                });

            });
        }
    };

    _handlePlayBackBtnPressed = () => {
        const { isPlaying, playBackEnded, seekBarPosition } = this.state;

        if (isPlaying) {

            this.setState({
                isPlaying: false,
                playBackEnded: false
            });
            this.media.pauseAsync().then(() => {

            });
        }

        if (!isPlaying) {

            const fromPosition = playBackEnded ? 0 : seekBarPosition;

            this.setState({ isPlaying: true, playBackEnded: false })

            this.media.playFromPositionAsync(fromPosition).then(() => {

            })


            setTimeout(() => {
                if (this.state.isPlaying && !this.state.isSliding) {
                    this.setState({
                        playControlVisible: true
                    })
                }
            }, this.fadeAnimTimeOut)
        }

    };

    _handleTogglePlayControlVisibility = () => {
        const { fadeAnim, playControlVisible, isPlaying } = this.state;



        this.setState(state => ({

            playControlVisible: !state.playControlVisible

        }))


        setTimeout(() => {
            if (this.state.playControlVisible && this.state.isPlaying && !this.state.isSliding) {
                this.setState((state) => ({
                    playControlVisible: true
                }));
            }
        }, 3000);


        // if (this.state.playControlVisible) {
        //     Animated.timing(fadeAnim, {
        //         toValue: 0,
        //         duration: this.fadeAnimDuration,
        //     }).start(() => {
        //         this.setState((state) => ({
        //             playControlVisible: false
        //         }));
        //     });
        // } else {
        //     this.setState((state) => ({
        //         playControlVisible: true
        //     }), () => {
        //         Animated.timing(fadeAnim, {
        //             toValue: 1,
        //             duration: this.fadeAnimDuration,
        //         }).start(() => {
        //             setTimeout(() => {
        //                 if (this.state.isPlaying && !this.state.isSliding) {
        //                     Animated.timing(fadeAnim, {
        //                         toValue: 0,
        //                         duration: this.fadeAnimDuration,
        //                     }).start(() => {
        //                         this.setState({
        //                             playControlVisible: false
        //                         })
        //                     });
        //                 }
        //             }, this.fadeAnimTimeOut)
        //         });
        //     })
        // }


    }

    onLoad = (data) => {
        this.setState({
            seekEnd: Number(data.durationMillis)
        })
    };


    render() {
        const { shouldPlay } = this.props;
        const { seekBarPosition, isAudio, mediaHeight, mediaWidth, seekEnd, playBackEnded, isPlaying, fadeAnim, currentPositionLabel, playControlVisible, mediaType } = this.state;

        let resizedHeight = 250;
        if (mediaHeight) {
            resizedHeight = getMediaHeight(mediaWidth, mediaHeight);
        }

        return (
            <TouchableWithoutFeedback
                onPress={this._handleTogglePlayControlVisibility}
                style={{ flex: 1 }}>
                <View style={styles.player}>

                    <View style={[styles.mediaIcon, theme.center]}>

                        {isAudio && (
                            <MaterialIcons name="audiotrack" size={50}
                                color={colors.white} />
                        )}

                        {!isAudio && (
                            <Video
                                ref="video"
                                onLoad={this.onLoad}
                                source={{ uri: this.props.source }}
                                progressUpdateIntervalMillis={500}
                                onPlaybackStatusUpdate={this.onPlaybackStatusUpdate}
                                rate={1.0}
                                volume={1.0}
                                shouldPlay={shouldPlay}
                                // isLooping={true}
                                isMuted={false}
                                resizeMode="contain"
                                style={{ flex: 1, width: dimens.viewportWidth, height: resizedHeight }} />
                        )}



                        <Animatable.View
                            animation={playControlVisible ? "fadeOut" : "fadeIn"} duration={800}
                            onAnimationBegin={() => {
                                this.setState({ layoutVisible: true })
                            }}
                            onAnimationEnd={() => {

                                this.setState({ layoutVisible: !playControlVisible })
                            }}
                            style={{
                                position: 'absolute',
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 0
                            }}>
                            {this.state.layoutVisible && (
                                <View style={{ flex: 1 }}>
                                    <View style={[styles.playAction, theme.center, {
                                        position: 'absolute',
                                        top: 0,
                                        bottom: 0,
                                        left: 0,
                                        right: 0
                                    }]}>
                                        <TouchableOpacity
                                            onPress={this._handlePlayBackBtnPressed}
                                            style={[styles.playButton, theme.center]}>
                                            <View style={styles.icon}>
                                                <MaterialIcons color={colors.white} size={50}
                                                    name={playBackEnded ? "refresh" : isPlaying ? "pause" : "play-arrow"} />
                                            </View>


                                        </TouchableOpacity>
                                    </View>

                                    <View style={[styles.seekBarContainer]}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginRight: 10 }}>
                                            <Text style={[theme.font, styles.duration]}>
                                                {moment(currentPositionLabel).format("mm:ss")}
                                            </Text>
                                            <Slider
                                                step={1}
                                                value={seekBarPosition}
                                                style={styles.seekBar}
                                                minimumValue={0}
                                                onValueChange={this.onSeekBarValueChange}
                                                onSlidingComplete={this.onSeekBarPositionChange}
                                                maximumValue={seekEnd}
                                            />
                                            <Text style={[theme.font, styles.duration]}>
                                                {moment(seekEnd).format("mm:ss")}
                                            </Text>

                                        </View>


                                    </View>
                                </View>
                            )}

                        </Animatable.View>



                    </View>




                </View>


            </TouchableWithoutFeedback>
        )

    }

}

Player.defaultProps = {
    shouldPlay: false
};


export default Player;
