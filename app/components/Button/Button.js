import React from 'react';
import {
    View,
    Platform,
    TouchableOpacity,
    TouchableHighlight,
    TouchableNativeFeedback,
} from 'react-native';


class Button extends React.PureComponent {

    render() {
        return (
            <View style={this.props.style}>
                {Platform.OS === 'android' ?
                    <TouchableNativeFeedback {...this.props}>
                        {this.props.children}
                    </TouchableNativeFeedback> :
                    <TouchableOpacity {...this.props}>
                        {this.props.children}
                    </TouchableOpacity>
                }
            </View>
        )
    }
}


export default Button;