import SnackBar, {SNACKBAR_DURATION} from './SnackBar';

export const DURATION = SNACKBAR_DURATION;
export default SnackBar;