import {StyleSheet, Dimensions} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import * as dimens from '../../utils/dimens';
import * as colors from "../../utils/colors";
const {width} = Dimensions.get('window');

export default StyleSheet.create({

    snackBar: {
        position: 'absolute',
        padding: 15,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: colors.colorSnackBar
    },

    text: {
        fontSize: 14,
        color: colors.white,
        fontWeight: 'bold'
    },

});