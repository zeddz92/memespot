import React from 'react';
import styles from './styles';
import {Image, View, Text, Dimensions, Animated, Keyboard} from 'react-native';
import * as colors from "../../utils/colors";
let {width, height} = Dimensions.get('window');


export const SNACKBAR_DURATION = {
    LENGTH_SHORT: 3000,
    LENGTH_LONG: 2000,
    LENGTH_INDEFINITE: 0,
};

export default class SnackBar extends React.Component {
    state = {
        text: '',
        isVisible: false,
        bottom: 0,
        animated: new Animated.Value(0)
    };

    componentDidMount() {
        // this.toggleBar();
    }

    componentWillMount() {
        Keyboard.addListener('keyboardDidShow', this.keyboardWillShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this.keyboardWillHide.bind(this));
    }

    componentWillUnmount () {
        //this.keyboardWillShow.remove();
        //this.keyboardWillHide.remove();
    }

    keyboardWillShow(e) {
        this.setState({bottom: e.endCoordinates.height})
    }

    keyboardWillHide(e) {
        this.setState({bottom: 0})
    }

    show(message, duration) {
        this.duration = typeof duration === 'number' ? duration : SNACKBAR_DURATION.LENGTH_SHORT;
        const {animated, isVisible} = this.state;
        this.setState({
            text: message,
            isVisible: true,
        }, ()=> {
            Animated.timing(animated, {
                toValue: 1,
                duration: 250,
            }).start(()=> {
                setTimeout(() => {
                    Animated.timing(animated, {
                        toValue: 0,
                        duration: 250,
                    }).start(()=> {
                        this.setState({isVisible: false})
                    })
                }, 3000);
            });
        });

        // this.toggleBar();

    }

    toggleBar() {

        const {animated, isVisible} = this.state;
        const newState = !isVisible;

        this.setState((state) => ({isVisible: !state.isVisible}));

        Animated.timing(animated, {
            toValue: newState ? 1 : 0,
            duration: 250,
        }).start(newState ? this.hideBar() : null);
    }


    hideBar() {

        setTimeout(() => {
            this.toggleBar()
        }, 4000)

    }

    setPosition = () => {
        const {position} = this.props;
        const {text, bottom, isVisible} = this.state;


        switch (position) {
            case "top":
                return {top: 0}
            case "bottom":
                return {bottom: bottom}
            default:
                return {bottom: bottom}
        }

    };


    render() {

        const {position} = this.props;

        const {text, bottom, isVisible} = this.state;
        const transformStyle = [{
            translateY: this.state.animated.interpolate({
                inputRange: [0, 1],
                outputRange: [100, 1]
            })
        }];

        if(isVisible) {
            return (
                <Animated.View style={{
                    flex: 1,
                    padding: 15,
                    position: 'absolute',
                    bottom: bottom,
                    left: 0,
                    right: 0,
                    backgroundColor: colors.colorSnackBar,
                    transform: transformStyle
                }}>
                    <Text numofLines={1} style={styles.text}>{text}</Text>
                </Animated.View>
            );
        }

        return null;



    }

};
