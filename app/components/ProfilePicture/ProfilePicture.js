import React from 'react';
import styles from './styles';
import {Image, View, Text, TouchableOpacity} from 'react-native';
import SvgUri from 'react-native-svg-uri';
import {theme} from "../../utils/styles";
import {SimpleLineIcons} from '@expo/vector-icons';
import * as colors from "../../utils/colors";
import {connect} from "react-redux";



const ProfilePicture = (props) => {

    const {user, imageStyle, containerStyle} = props;

    return (
        <View style={containerStyle}>
            <Image style={imageStyle}
                   source={user.data.profile_picture_url ? {uri: user.data.profile_picture_url} : require('../assets/img/profile.png')}/>
        </View>
    );
};

function mapStateToProps({user}) {
    return {
        user
    }
}

export default connect(mapStateToProps)(ProfilePicture);