import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import * as dimens from '../../utils/dimens';
import {theme} from '../../utils/styles';

const padding = {
    paddingTop: 15,
    paddingStart: 10,
    paddingEnd: 10
};

export default StyleSheet.create({

    font: {
        color: '#37393D',
        fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
    },

    timeElapsed: {
        fontWeight: 'normal',
        fontSize: 12
    },


    moreReplies: {
        padding: 5,
        marginStart: 15,
        marginEnd: 15,
        borderRadius: 5,
        marginTop: 10,
        backgroundColor: '#ECEEF1',
        alignItems: 'center',
    },

    container: {
        flex: 1
    },

    verticalDivider: {
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "#cbcbcb",
        height: '100%',
        margin: 5,
    },

    icon: {
        marginEnd: 5
    },

    actions: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingStart: 40,
        // paddingTop: 5,
        // paddingBottom: 5,
        flexDirection: 'row',

    },
    action: {
        flexDirection: 'row',
        ...padding,
        paddingTop: 10,
        paddingStart: 0,
        paddingEnd: 10,
        alignItems: 'center'
    },

    repliesProfileImgStyle: {
        width: 30,
        height: 30,
        resizeMode: 'cover',
        borderRadius: dimens.getRadius(30),
        marginRight: 10
    },


    comment: {
        padding: 15,
        borderBottomColor: '#cbcbcb',
        // borderBottomWidth: 1
    },
    profile_image: {
        width: 35,
        height: 35,
        resizeMode: 'cover',
        borderRadius: dimens.getRadius(35),
        marginRight: 10
    },

    text: {
        fontSize: Platform.OS === 'android' ? 14 : 16,
        fontWeight: '600',
        color: 'grey',
    },
    showImageBtn: {
        marginTop: 10,
        flex: 1,
        // marginLeft: 45
        marginLeft: 0

    },

    username: {
        fontSize: Platform.OS === 'android' ? 14 : 16,
        fontWeight: 'bold'
    },

    row: {
        flexDirection: 'row'
    }

});