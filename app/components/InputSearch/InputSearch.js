import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    ActivityIndicator,
    Keyboard,
    Animated,
    FlatList,
    Image,
    Alert,
    Platform,
} from 'react-native';
import styles from './styles';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import * as colors from "../../utils/colors";
import * as dimens from "../../utils/dimens";
import PropTypes from 'prop-types';
import Player from "../Player/Player";
import Button from "../Button/Button";
import { theme } from '../../utils/styles';
import { api } from '../../utils/api';
import * as Animatable from 'react-native-animatable';


// import debounce from 'lodash.debounce';

var debounce = require('lodash.debounce');


class InputSearch extends React.Component {

    constructor(props) {
        super(props);
        this.onChangeTextDelayed = debounce(this.handleSearch, 1000);
        this.state = {
            text: props.tag ? props.tag.name : "",
            tags: [],
            searchResultHeight: dimens.viewportHeight,
            selectedTag: props.tag,
            isLoading: true,
            searchResultVisible: false,
        }
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    keyboardDidShow(e) {

        this.setState({ searchResultHeight: e.endCoordinates.height })
    }

    keyboardDidHide(e) {
        this.setState({ searchResultHeight: '100%' })

        // this.setState({bottom: 0})
        // this.setState({replyingComment: null})
    }


    handleRequest = () => {
        const { text } = this.state;
    }

    handleSearch = () => {
        const { text } = this.state;

        this.setState({ tags: [] })
        api.tag.getAll(1, 15, text).then(response => {
            if (!response.error) {
                this.setState({
                    tags: response.data,
                    isLoading: false,
                    // searchResultVisible: true
                })
            }

        }).catch(error => {

        });
        console.log(text);
    };

    handleTextChanged = (text) => {
        const shouldSearch = text.length > 1;
        if (shouldSearch && !this.state.searchResultVisible) {
            // Animated.spring(this.state.animation, {
            //     toValue: 2,
            //   }).start();
        }


        this.setState({ selectedTag: null, text, searchResultVisible: shouldSearch, isLoading: shouldSearch }, () => this.onChangeTextDelayed())
    }

    handleTagSelected = (tag) => {
        this.setState({
            text: tag.name,
            searchResultVisible: false,
            selectedTag: tag
        });

    }

    renderResult = ({ item, index }) => {
        return (
            <Button onPress={() => this.handleTagSelected(item)}>
                <View style={[styles.result, styles.horizontal]}>
                    <View style={[styles.horizontal, { alignItems: 'center' }]}>
                        <MaterialIcons style={styles.icon} name="search" size={30}
                            color={colors.colorActions} />
                        <Text style={styles.searchResultText}>{item.name}</Text>
                    </View>
                    <Text style={styles.searchResultText}>{item.count} Posts</Text>
                </View>
            </Button>

        )
    }

    render() {

        const { text, searchResultVisible, isLoading, tags, selectedTag } = this.state;
        const { placeholder } = this.props;

        return (
            <View style={styles.inputSearch}>
                <View style={[styles.horizontal, { justifyContent: 'space-between' }]}>
                    {/* <MaterialIcons style={styles.icon} name="search" size={25}
                        color={colors.colorActions} /> */}
                    <TextInput
                        value={text}
                        onChangeText={this.handleTextChanged}
                        onBlur={() => { this.onChangeTextDelayed.flush() }}
                        underlineColorAndroid='transparent'
                        style={styles.input}
                        placeholder={placeholder} />
                    {selectedTag && (
                        <MaterialIcons name="check" size={25}
                            color={colors.colorActions} />
                    )}

                </View>

                {searchResultVisible && (
                    <Animatable.View animation="fadeIn" style={[styles.searchResultContainer, { height: this.state.searchResultHeight }]}>
                        {isLoading && (
                            <ActivityIndicator size="large" color="#0000ff" />
                        )}
                        {!isLoading && (
                            <FlatList
                                keyboardShouldPersistTaps={'handled'}
                                data={tags}
                                renderItem={this.renderResult}
                            />
                        )}
                    </Animatable.View>
                )}


            </View>
        )

    }
}

InputSearch.defaultProps = {
    style: false
};

export default InputSearch;