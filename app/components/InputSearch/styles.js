import { StyleSheet } from 'react-native';
import { Platform } from 'react-native';
import { Fonts } from '../../utils/Fonts';
import * as dimens from '../../utils/dimens';
import { theme } from '../../utils/styles';

const padding = {
    paddingTop: 15,
    paddingStart: 10,
    paddingEnd: 10
};

export default StyleSheet.create({
    inputSearch: {
        padding: 15,
    },

    icon: {
        marginEnd: 15,
    },

    searchResultText: {
        fontSize: 18,
    },

    searchResultContainer: {
        // flex: 1,
        marginTop: 15,
        // backgroundColor: 'red',
        height: '100%'

    },

    result: {
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 10,
    },

    input: {
        flex: 1,
        fontSize: 18,
    },

    horizontal: {
        flexDirection: 'row',
    }

});