import React from 'react';
import styles from './styles';
import {
    Image,
    View,
    Text,
    TouchableWithoutFeedback,
    ActivityIndicator,
    TouchableOpacity,
    AppState,
    Slider,
    Animated
} from 'react-native';

import * as colors from '../../utils/colors';
import * as dimens from '../../utils/dimens';
import { theme } from '../../utils/styles';


import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';

import Modal from 'expo/src/modal/Modal';


const DEFAULT_CONTENT = "Please wait...";

class ProgressDialog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            content: DEFAULT_CONTENT
        };
    }

    componentWillUnmount() {
        this.setState({
            modalVisible: false,
        });
    }

    show(message = DEFAULT_CONTENT) {
        this.setState({
            modalVisible: true,
            content: message

        });
    }

    dismiss() {
        return new Promise((resolve, reject) => {
            this.setState({
                modalVisible: false,
            }, resolve());
        });
    }


    render() {
        const { modalVisible, content } = this.state;
        return (
       
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={modalVisible}>

                    <View
                        style={[theme.center, styles.progressDialog]}>
                        <View style={[styles.contentContainer]}>
                            <ActivityIndicator style={styles.indicatorStyle} size="large" color="#0000ff" />
                            <Text style={[theme.font, styles.contentText]}>{content}</Text>
                        </View>
                    </View>
                </Modal>
     

        )

        // return (
        //     <View
        //       style={styles.container}>

        //         <View
        //             style={[theme.center, styles.progressDialog]}>
        //             <View style={[styles.contentContainer]}>
        //                 <ActivityIndicator style={styles.indicatorStyle} size="large" color="#0000ff"/>
        //                 <Text style={[theme.font, styles.contentText]}>{content}</Text>
        //             </View>
        //         </View>
        //     </View>
        // )


    }

}

export default ProgressDialog;
