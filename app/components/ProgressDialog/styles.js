import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import {colorActions} from "../../utils/colors";
import * as colors from "../../utils/colors";
import * as dimens from "../../utils/dimens";


export default StyleSheet.create({
   progressDialog: {
       flex: 1,
       flexDirection: 'column',
       backgroundColor: 'rgba(52, 52, 52, 0.8)',
   },

   container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
   },

    contentContainer: {
        width: '60%',
        borderRadius: 5,
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        padding: 15,
    },

    indicatorStyle: {
      marginEnd: 15
    },

    contentText: {
       fontSize: 16,

    }
});