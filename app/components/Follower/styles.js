import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import * as dimens from '../../utils/dimens';


export default StyleSheet.create({
    follower: {
        flex: 1,
        flexDirection: 'row',
        margin: 10,
        justifyContent: 'space-between',
        alignItems: 'center'

    },

    actions: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    profile_image: {
        width: 50,
        height: 50,
        marginEnd: 15,
        borderRadius: dimens.getRadius(50),
        borderWidth: 1,
        resizeMode: 'cover',
        borderColor: 'red'
    },

    icon: {
        padding: 5,

    },
    font: {
        color: '#37393D',
        fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
    },

    username: {
        fontWeight: 'bold'
    },


    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
})