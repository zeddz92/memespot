import React from 'react';
import styles from './styles';
import {Image, View, Text, TouchableOpacity} from 'react-native';
import SvgUri from 'react-native-svg-uri';
import {theme} from "../../utils/styles";
import {SimpleLineIcons} from '@expo/vector-icons';
import * as colors from "../../utils/colors";
import {connect} from "react-redux";



const Follower = (props) => {

    const {user, data, unFollow, follow} = props;

    console.log(user.following);

    const Actions = () => {
        return (
            <View style={styles.actions}>
                {user.following[data.id]  ?
                    <TouchableOpacity
                        onPress={()=> unFollow(data)}
                        style={{margin: 5, marginRight: 15,}}>
                        <SimpleLineIcons name="user-following" size={24} color={colors.colorLink}/>
                    </TouchableOpacity>:
                    <TouchableOpacity
                        onPress={()=> follow(data)}
                        style={{margin: 5, marginRight: 15}}>
                        <SimpleLineIcons name="user-follow" size={24} color={colors.colorActions}/>
                    </TouchableOpacity>
                }



            </View>
        );
    };


    return (
        <View style={styles.follower}>
            <View style={styles.row}>
                <Image
                    style={styles.profile_image}
                    source={{uri: data.profile_picture_url}}/>
                <View>
                    <Text style={[styles.font, styles.username]}>
                        {data.username}
                    </Text>

                    <Text style={[styles.font]}>{data.name}</Text>
                </View>
            </View>
            <Actions/>
        </View>
    )

};

function mapStateToProps({user}) {
    return {
        user
    }
}

export default connect(mapStateToProps)(Follower);