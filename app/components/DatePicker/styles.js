import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import * as dimens from '../../utils/dimens';
import {Constants} from "expo";


export default StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        paddingTop: Constants.statusBarHeight + 55,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',

        alignItems: 'center'
    },
    datePickerContainer : {
        width: '90%',
        borderRadius: 10,
        backgroundColor: 'white',
    }
})