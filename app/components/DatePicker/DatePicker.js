import React from 'react';
import styles from './styles';
import {Modal, View, Text, TouchableWithoutFeedback, DatePickerIOS, DatePickerAndroid, Platform} from 'react-native';
import SvgUri from 'react-native-svg-uri';
import {theme} from "../../utils/styles";
import {SimpleLineIcons} from '@expo/vector-icons';
import * as colors from "../../utils/colors";
import {connect} from "react-redux";
import {Constants} from 'expo';
import moment from 'moment';


class DatePicker extends React.PureComponent {


    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            chosenDate: new Date()
        }

    }

    open() {
        const {changeDate} = this.props;

        if (Platform.OS === 'android') {
            DatePickerAndroid.open({date: new Date(2020, 4, 25)}).then(chosenDate => {
                if (chosenDate.action !== DatePickerAndroid.dismissedAction) {
                    changeDate(chosenDate);
                }
            })
        } else {
            this.setState({modalVisible: true})
        }
    }

    handleDateChanged = (date) => {
        const {changeDate} = this.props;

        this.setState({
            chosenDate: date
        }, () => {
            changeDate(date);
        })
    };


    render() {
        const {changeDate} = this.props;
        const {chosenDate} = this.state;

        return (
            <View>
                <Modal
                    transparent={true}
                    animationType="slide"
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                    <TouchableWithoutFeedback
                        onPress={() => this.setState({modalVisible: false})}>
                        <View
                            style={styles.container}>
                            <View style={styles.datePickerContainer}>
                                {Platform.OS === 'ios' && (
                                    <DatePickerIOS
                                        onDateChange={this.handleDateChanged}
                                        mode="date"
                                        date={chosenDate}/>
                                )}
                            </View>
                        </View>

                    </TouchableWithoutFeedback>

                </Modal>


            </View>
        )
    }


};

export default DatePicker;