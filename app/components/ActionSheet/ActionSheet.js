import React from 'react';
import styles from './styles';
import {
    TouchableWithoutFeedback,
    View,
    Text,
    Modal
} from 'react-native';

import * as colors from '../../utils/colors';

import {theme} from "../../utils/styles";
import Button from "../Button";
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';

class ActionSheet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            options: {}
        };
    }

    show(options) {
        this.setState({
            options,
            modalVisible: true
        });
    }

    dismiss() {
        return new Promise((resolve, reject) => {
            this.setState({
                options: {},
                modalVisible: false
            }, resolve());

            resolve()
        });

    }

    render() {
        const {modalVisible, options} = this.state;



        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}>
                <TouchableWithoutFeedback
                onPress={()=> this.setState({modalVisible: false})}>
                    <View
                        style={[theme.center, styles.actionList]}>
                        <View style={[styles.contentContainer]}>

                            {options.title && (
                                <Text style={[theme.font, styles.title]}>{options.title}</Text>
                            )}



                            {options.buttons && options.buttons.map((action, index) => (
                                <Button
                                    key={index}
                                    onPress={(comment)=> this.dismiss().then(_ => action.onPress(comment))}>
                                    <View style={styles.action}>
                                        {action.icon && (
                                            <MaterialCommunityIcons style={styles.actionIcon} name={action.icon} size={25}
                                                                    color={colors.colorActions}/>
                                        )}

                                        <Text style={[theme.font, styles.actionText]}>{action.text}</Text>

                                    </View>
                                </Button>
                            ))}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

export default ActionSheet;