import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import * as dimens from '../../utils/dimens';
import * as colors from '../../utils/colors';

const padding = {
    paddingTop: 15,
    paddingStart: 10,
    paddingEnd: 10
};

export default StyleSheet.create({

    actionList: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
    },

    actionIcon: {
        marginEnd: 15
    },

    actionText: {
        fontSize: 18,
        color: colors.black,
        fontWeight: 'bold',
    },

    title: {
        padding: 15,
        fontSize: 16,
        fontWeight: 'bold',

    },

    action: {
        padding: 15,
        // paddingStart: 30,
        // paddingEnd: 30,
        alignItems: 'center',
        flexDirection: 'row',
    },

    contentContainer: {
        width: '90%',
        borderRadius: 5,
        backgroundColor: 'white',
        padding: 15,
        paddingLeft: 0,
        paddingRight: 0,
    },

    image: {
        width: 30,
        height: 30,
        marginEnd: 10,
        borderRadius: dimens.getRadius(30),
    },


    text: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#373C3F'
    },

    description: {
        color: 'gray',
        fontWeight: 'bold',
    },


    font: {
        color: 'white',
        fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
    },


});