import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import {colorActions} from "../../utils/colors";
import * as colors from "../../utils/colors";


const size = 100;
const avatarSize = 40;
const avatarRadius = Platform.OS === 'android' ? (avatarSize * 2) : (avatarSize / 2);
const padding = {
    paddingTop: 15,
    paddingStart: 15,
    paddingEnd: 15
};

export default StyleSheet.create({

    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    post: {
        // margin: 5,
        paddingBottom: 15,
        marginBottom: 15,
        borderColor: '#cbcbcb',
        borderTopWidth: .5,
        borderBottomWidth: .5
    },


    header: {
        ...padding,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
    },

    link: {
        fontSize: 12,
        color: 'blue'
    },

    font: {
        color: '#37393D',
        fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
    },

    image: {
        marginTop: 0,
        height: 370,
        resizeMode: 'cover',
        // marginBottom: 5
    },

    bodyMessage: {
        ...padding,
        paddingTop: 0,
        marginTop: 5,
        marginBottom: 10,
        fontSize: 18,
        textAlign: 'justify',
        fontWeight: 'bold',
        // color: colors.black,
        color: colors.colorSettingsText

        // letterSpacing: 1.1,
        // fontFamily: 'roboto-mono-medium',
    },

    icon: {
        marginEnd: 10
    },

    actionIcon: {
        marginEnd: 5
    },

    actionText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.colorActions
    },

    userName: {
        fontWeight: 'bold'
    },

    divider: {
        marginTop: 10,
        marginBottom: 10,
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "#cbcbcb"

    },

    createdTimeLabel: {
        fontSize: 12,

    },


    verticalDivider: {
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "#cbcbcb",
        height: '100%',
        margin: 5,
        // marginTop: 15,
    },

    avatar: {
        width: avatarSize,
        height: avatarSize,
        resizeMode: 'cover',
        borderRadius: avatarRadius,
        marginRight: 15
    },

    actions: {

        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
        // paddingTop: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    action: {
        flex: 1,
        marginEnd: 20,
        flexDirection: 'row',
        alignItems: 'center',
    }

});