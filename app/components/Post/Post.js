import React from 'react';
import styles from './styles';
import { Image, View, Text, TouchableWithoutFeedback, FlatList, TouchableOpacity, AppState } from 'react-native';

import * as colors from '../../utils/colors';
import * as dimens from '../../utils/dimens';
import { theme } from '../../utils/styles';

import Media from '../Media';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';

import Button from "../Button";

class Post extends React.Component {

    state = {
        appState: AppState.currentState,
        shouldPlay: false,
        isItemHidden: true,

    }

    togglePlayVideo = () => {
        this.setState((state) => ({
            shouldPlay: !state.shouldPlay
        }));
    }

    toggleItemHidden = (isViewable) => {
        this.setState((state) => ({
            isItemHidden: isViewable
        }));
    }

    stopVideos = () => {
        this.setState({
            shouldPlay: false,
        });
    };

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {

        }
        this.setState({ appState: nextAppState });
    };

    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
    }

    renderTags = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={()=> this.props.navigation.navigate("Search")}
                style={{ margin: 15, marginBottom: 0 }}>
                <Text style={[theme.link, { fontSize: 16, fontWeight: 'bold' }]}>{item.name}</Text>
            </TouchableOpacity>
        )

    };


    render() {


        const { item, doFeedBack } = this.props;
        const { shouldPlay, isItemHidden } = this.state;
        const media = item.medias[0];

        const tags = item.tags.map(function (tag) {
            return tag.name;
        }).join(",");

        const feedback = item.user_feedback ? item.user_feedback.action : 0;

        const Actions = () => {
            return (
                <View style={styles.actions}>
                    <View style={{ flexDirection: 'row' }}>
                        <Button
                            onPress={() => doFeedBack(item, feedback === 1 ? 0 : 1)}>
                            <View style={styles.action}>
                                <View style={styles.actionIcon}>
                                    <MaterialIcons name="thumb-up" size={dimens.actionSize}
                                        color={feedback === 1 ? colors.colorLink : colors.colorActions} />
                                </View>

                                <Text style={[theme.font, theme.bold, styles.actionText]}>{item.likes}</Text>
                            </View>

                        </Button>

                        {/*<View style={styles.verticalDivider}/>*/}

                        <Button
                            onPress={() => doFeedBack(item, feedback === 2 ? 0 : 2)}>
                            <View style={styles.action}>
                                <View style={styles.actionIcon}>
                                    <MaterialIcons name="thumb-down" size={dimens.actionSize}
                                        color={feedback === 2 ? colors.colorLink : colors.colorActions} />
                                </View>

                                <Text style={[theme.font, styles.actionText]}>{item.dislikes}</Text>
                            </View>

                        </Button>

                        {/*<View style={styles.verticalDivider}/>*/}

                        <Button
                            onPress={() => this.props.goToComments(item)}>

                            <View style={[styles.action]}>
                                <View style={styles.actionIcon}>
                                    <MaterialIcons name="mode-comment" size={dimens.actionSize}
                                        color={colors.colorActions} />
                                </View>
                                <Text style={[theme.font, styles.actionText]}>{item.comments}</Text>


                            </View>
                        </Button>
                    </View>


                    {/*<View style={styles.verticalDivider}/>*/}

                    <Button
                        onPress={() => this.props.saveToFavorite(item)}>
                        <View style={[styles.action, { marginEnd: 0 }]}>
                            <View style={styles.icon}>
                                <MaterialCommunityIcons
                                    name="heart"
                                    size={20}
                                    color={item.favorite ? colors.colorFilledHeart : colors.colorActions} />
                            </View>

                            <Text style={[theme.font, styles.actionText, { fontSize: 16, color: colors.colorLink }]}>Save</Text>
                        </View>

                    </Button>
                </View>
            )
        };

        return (
            <View style={[styles.post, { backgroundColor: colors.white }]}>

                <View style={styles.header}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.rootNavigation.navigate("UserProfile", { user: item.user })}>
                        <View style={[styles.horizontal]}>
                            <Image
                                source={{ uri: item.user.profile_picture_url || "" }}
                                style={styles.avatar} />
                            <View>
                                <Text style={[theme.font, styles.userName]}>{item.user.username}</Text>
                                <Text style={[theme.font, styles.createdTimeLabel]}>{item.time}</Text>
                                {/*<Text style={styles.link}>#{tags}</Text>*/}

                            </View>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableOpacity
                        onPress={() => this.props.viewMore(item)}>
                        <MaterialIcons style={styles.icon} name="more-horiz" size={25} color={colors.colorActions} />
                    </TouchableOpacity>

                </View>
                <Text style={[theme.font, styles.bodyMessage]}>{item.title}</Text>

                {media && (
                    <Media isItemHidden={isItemHidden} shouldPlay={shouldPlay} togglePlayVideo={this.togglePlayVideo}
                        padding={0} {...this.props} />
                )}


                <FlatList
                    data={item.tags}
                    renderItem={this.renderTags}
                    keyExtractor={(item) => item.id.toString()}
                    horizontal={true}
                />

                <Actions />


            </View>
        )

    }


};


export default Post;