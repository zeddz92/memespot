import React from 'react';
import styles from './styles';
import {Image, View, Text, TouchableOpacity, FlatList} from 'react-native';
import {withNavigation} from 'react-navigation';

import SvgUri from 'react-native-svg-uri';
import * as colors from '../../utils/colors';
import * as dimens from '../../utils/dimens';
import {replies} from '../../utils/mocks/commnets';

import Media from '../Media';
import {theme} from "../../utils/styles";
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';


const Actions = (props) => {
    const {item} = props;
    return (
        <View style={styles.actions}>
            <TouchableOpacity style={styles.action}>
                <MaterialIcons style={styles.icon} name="thumb-up" size={dimens.actionSmallSize}
                               color={colors.colorActions}/>
                <Text style={styles.font}>{item.likes}</Text>
            </TouchableOpacity>
            {/*<View style={styles.verticalDivider}/>*/}
            <TouchableOpacity style={styles.action}>
                <MaterialIcons style={styles.icon} name="thumb-down" size={dimens.actionSmallSize}
                               color={colors.colorActions}/>
                <Text style={styles.font}>{item.dislikes}</Text>
            </TouchableOpacity>

            {/*<View style={styles.verticalDivider}/>*/}
            <TouchableOpacity onPress={()=>props.onReply(item)} style={styles.action}>
                <MaterialIcons style={styles.icon} name="reply" size={20}
                               color={colors.colorActions}/>
                {/*<Text style={[styles.font, theme.link]}>REPLY</Text>*/}

            </TouchableOpacity>
        </View>
    )
};

const Detail = (props) => {
    const {item, renderList, renderDetail} = props;
    const {showMoreReplies} = props.state;

    const repliesStyle = {marginLeft: 15, paddingTop: 15, paddingStart: 5, borderLeftWidth: 1, borderColor: colors.colorBorder};
    return (
      <View style={renderList ? repliesStyle: {marginTop: 0}}>
          <View style={styles.row}>
              <Image style={ renderList? styles.repliesProfileImgStyle : styles.profile_image}
                     source={{uri: item.user.profile_image}}/>
              <View style={styles.container}>
                  <Text style={[styles.font, styles.username]}>{item.user.username} <Text
                      style={styles.timeElapsed}>• {item.time_elapsed}</Text></Text>
                  <Text style={[styles.font]}>{item.text} </Text>
              </View>
          </View>
         {/* {renderList && item.media && (
              <Media padding={135} item={item}/>
          )}*/}
          <Actions {...props} />

           {item.media && (
               <TouchableOpacity
                   onPress={()=>props.navigation.navigate("MediaVisualizer", {data: props.item})}
                   style={styles.showImageBtn}>
                   <Text style={[theme.font, theme.link, {fontSize: 10,}]}>SHOW IMAGE</Text>
               </TouchableOpacity>
          )}

          {renderList && (
              <FlatList
                  data={replies[item.id]}
                  renderItem={renderDetail}
                  keyExtractor={(item, index) => index}
              />
          )}


          {/* {item.media && (
              <View>
                  {(!showImage) ? (
                      <TouchableOpacity
                          onPress={this._handleShowImage}
                          style={styles.showImageBtn}>
                          <Text style={[theme.font, theme.link, {fontSize: 12,}]}>SHOW IMAGE</Text>
                      </TouchableOpacity>
                  ) : <Media padding={45} item={item}/>}
              </View>
          )}*/}
      </View>
    );

};

class Comment extends React.Component {

    state = {
        showImage: false,
        showMoreReplies: false,
    };

    _handleShowImage = () => {
        this.setState({
            showImage: true,
        })
    };

    renderDetail = ({item, index}) => {
        return (
            <Detail state={this.state} renderDetail={this.renderDetail} renderList={true} {...this.props} item={item}/>
        )
    }


    render() {
        const {item, onReply} = this.props;
        const {showImage, showMoreReplies} = this.state;

        return (
            <View style={styles.comment}>

                <Detail state={this.state} renderDetail={this.renderDetail} {...this.props}/>

                 {(item.replies > 0 && !showMoreReplies) && (
                    <TouchableOpacity
                        onPress={()=> this.setState({showMoreReplies: true})}
                        style={styles.moreReplies}>
                        <Text style={styles.font}>{item.replies} more replies</Text>
                    </TouchableOpacity>
                )}

                {showMoreReplies && (
                    <FlatList
                        data={replies[item.id]}
                        renderItem={this.renderDetail}
                        keyExtractor={(item, index) => index}
                    />
                )}



            </View>
        );
    }
}


export default withNavigation(Comment);