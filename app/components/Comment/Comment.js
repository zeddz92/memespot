import React from 'react';
import styles from './styles';
import {Image, View, Text, TouchableOpacity, TouchableWithoutFeedback, FlatList, ActivityIndicator} from 'react-native';
import {withNavigation} from 'react-navigation';

import * as colors from '../../utils/colors';
import * as dimens from '../../utils/dimens';
import {replies} from '../../utils/mocks/commnets';

import Media from '../Media';
import {theme} from "../../utils/styles";
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import {api} from "../../utils/api";
import Button from "../Button/Button";


const Actions = (props) => {
    const {item, doFeedBack, handleNewReply, handleRemoveReply, handleCommentFeedback} = props;
    const feedback = item.user_feedback ? item.user_feedback.action : 0;
    const {comment} = props.state;

    return (
        <View style={styles.actions}>
            <TouchableOpacity
                onPress={() => doFeedBack(item, feedback === 1 ? 0 : 1, handleCommentFeedback)}
                style={styles.action}>
                <MaterialIcons style={styles.icon} name="thumb-up" size={dimens.actionSmallSize}
                               color={feedback === 1 ? colors.colorLink : colors.colorActions}/>
                <Text style={styles.font}>{item.likes}</Text>
            </TouchableOpacity>
            {/*<View style={styles.verticalDivider}/>*/}
            <TouchableOpacity
                onPress={() => doFeedBack(item, feedback === 2 ? 0 : 2, handleCommentFeedback)}
                style={styles.action}>
                <MaterialIcons style={styles.icon} name="thumb-down" size={dimens.actionSmallSize}
                               color={feedback === 2 ? colors.colorLink : colors.colorActions}/>
                <Text style={styles.font}>{item.dislikes}</Text>
            </TouchableOpacity>


            <TouchableOpacity onPress={() => props.onReply(item, comment, handleNewReply)} style={styles.action}>
                <MaterialIcons style={styles.icon} name="reply" size={20}
                               color={colors.colorActions}/>
            </TouchableOpacity>

            {item.medias && item.medias.length > 0 && (
                <TouchableOpacity
                    onPress={() => props.navigation.navigate("MediaVisualizer", {data: props.item})}
                    style={styles.showImageBtn}>
                    <MaterialCommunityIcons style={styles.icon} name="folder-image" size={20}
                                            color={colors.colorLink}/>
                </TouchableOpacity>
            )}

            <TouchableOpacity onPress={() => props.viewMore(item, comment, handleNewReply, handleRemoveReply)}
                              style={[styles.action, {flex: 1, justifyContent: 'flex-end'}]}>
                <MaterialIcons style={styles.icon} name="more-vert" size={20}
                               color={colors.colorActions}/>
            </TouchableOpacity>


        </View>
    )
};

const Detail = (props) => {
    const {item, renderList, renderDetail, getMoreReplies} = props;
    const {showMoreReplies, loaders, replies, showFirst} = props.state;


    const repliesStyle = {

        paddingTop: 15,
        paddingStart: 20,
        borderLeftWidth: 1,
        borderColor: colors.colorBorder
    };

    const repliesArray = replies[item.id] ? replies[item.id].data : [];

    const repliesCount = item.replies_count;

    return (
        <View
            style={[renderList ? repliesStyle : {marginTop: 0}, {backgroundColor: item.isPosting ? "#F5FBFF" : colors.white}]}>
            <Button
                onPress={() => props.navigation.navigate("UserProfile", {user: item.user})}>
                <View style={styles.row}>
                    <Image style={renderList ? styles.repliesProfileImgStyle : styles.profile_image}
                           source={{uri: item.user.profile_picture_url || ""}}/>
                    <View style={styles.container}>
                        <Text style={[styles.font, styles.username]}>{item.user.username} <Text
                            style={[theme.font, styles.timeElapsed,]}>• {item.time}</Text></Text>
                        <Text style={[styles.font, styles.text]}>{item.text} </Text>
                    </View>
                </View>
            </Button>


            {!item.isPosting && (
                <Actions {...props} />
            )}


            {renderList && (
                <View>

                    <FlatList
                        data={repliesArray}
                        renderItem={renderDetail}
                        keyExtractor={(item, index) => index}
                        extraData={props.state}
                    />


                    {(item.replies_count > 1 && repliesArray.length < item.replies_count) && (
                        <TouchableWithoutFeedback
                            onPress={() => getMoreReplies(item.id, item.parent_id)}>
                            <View style={styles.moreReplies}>
                                {loaders[item.id] ?
                                    <ActivityIndicator color="#0000ff" size="small" animating/> :
                                    <Text style={styles.font}>{repliesCount - repliesArray.length} more replies</Text>
                                }
                            </View>
                        </TouchableWithoutFeedback>
                    )}
                </View>

            )}


        </View>
    );

};

class Comment extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showImage: false,
            showMoreReplies: true,
            replies: {},
            loaders: {},
            exclude: {},
            showFirst: {},
            comment: props.item
        };

        this.exclude = {};
    }

    componentDidMount() {
        const {loadReplies, item} = this.props;
        if (loadReplies) {
            this.getMoreReplies(item.id, item.parent_id);
        }
    }


    _handleShowImage = () => {
        this.setState({
            showImage: true,
        })
    };

    handleRemoveReply = (comment, parent) => {
        const id = comment.parent_id || comment.id;
        this.setState((state) => ({
            replies: {
                ...state.replies,
                [id]: {
                    ...state.replies[id],
                    data: state.replies[id].data.filter(c => (c.id !== comment.id))
                }
            }
        }))
    };

    handleCommentFeedback = (comment) => {
        const id = comment.parent_id || comment.id;
        const replies = this.state.replies[id] ? this.state.replies[id].data : [];

        // alert(JSON.stringify(comment));

        this.setState((state) => ({
            replies: {
                ...state.replies,
                [id]: {
                    ...state.replies[id],
                    data: replies.map(c => (
                        c.id === comment.id ? comment : c
                    ))
                }
            }
        }))
    };


    handleNewReply = (newComment, isNew = 0) => {


        const id = newComment.parent_id || newComment.id;
        const replies = this.state.replies[id] ? this.state.replies[id].data : [];
        this.setState((state) => ({
            replies: {
                ...state.replies,
                [id]: {
                    ...state.replies[id],
                    data: newComment.id === 0 ? [newComment, ...replies] : replies.map(comment => (
                        comment.id === isNew ? newComment : comment
                    ))
                }
            }
        }))
    };

    renderDetail = ({item, index}) => {
        return (
            <Detail state={this.state} renderDetail={this.renderDetail}
                    getMoreReplies={this.getMoreReplies}
                    renderList={true}
                    handleNewReply={this.handleNewReply}
                    handleCommentFeedback={this.handleCommentFeedback}
                    handleRemoveReply={this.handleRemoveReply}
                    {...this.props} item={item}/>
        )
    }

    getMoreReplies = (commentId, parentId, parent = false) => {

        this.setState({
            loaders: {
                [commentId]: true,
            }
        });

        api.comment.getReplies(commentId).then(response => {

            this.setState((state) => ({
                replies: {
                    ...state.replies,
                    [commentId]: response,
                },
                loaders: {
                    ...state.loaders,
                    [commentId]: false,
                },
                showFirst: commentId,
                showMoreReplies: false,
                exclude: {
                    ...state.exclude,
                }
            }));

            response.data.map((comment, index) => {
                const reply = comment.first_reply;

                if (reply && !this.state.exclude[comment.parent_id]) {
                    const replies = this.state.replies[reply.parent_id] ? this.state.replies[reply.parent_id].data : [];

                    this.setState((state) => ({
                        replies: {
                            ...state.replies,
                            [reply.parent_id]: {
                                ...state.replies[reply.parent_id],
                                data: [...replies, ...[reply]]

                            },
                        },
                        exclude: {
                            ...state.exclude,
                            [comment.id]: true,
                        }
                    }));
                    // this.exclude[comment.id] = true;
                } else if (index > 0 && reply) {
                    this.setState((state) => ({
                        replies: {
                            ...state.replies,
                            [reply.parent_id]: {
                                ...state.replies[reply.parent_id],
                                data: [...replies, ...[reply]]

                            },
                            exclude: {
                                ...state.exclude,
                            }
                        },
                    }));
                }
            });

        }).catch(error => {
            this.setState((state) => ({
                error,
                refreshing: false,
                showLoading: false,
                showMoreReplies: false
            }));
        });
    };


    render() {
        const {item, onReply, loadReplies} = this.props;
        const {showImage, showMoreReplies, comment, replies, loaders} = this.state;

        const repliesArray = replies[item.id] ? replies[item.id].data : [];
        const repliesCount = item.replies_count;


        console.log(this.state.replies);


        return (
            <View style={[styles.comment, {backgroundColor: item.isPosting ? "#F5FBFF" : colors.white}]}>

                <Detail
                    handleNewReply={this.handleNewReply}
                    handleRemoveReply={this.handleRemoveReply}
                    handleCommentFeedback={this.handleCommentFeedback}
                    state={this.state}
                    renderDetail={this.renderDetail} {...this.props} />

                {loadReplies && (
                    <View style={theme.divider}/>
                )}

                <FlatList
                    data={this.state.replies[item.id] ? this.state.replies[item.id].data : []}
                    renderItem={this.renderDetail}
                    keyExtractor={(item, index) => index}
                    extraData={this.state}
                />

                {loadReplies && (
                    <View>

                        {/* <View style={theme.divider} /> */}
                        {loaders[item.id] && (
                            <ActivityIndicator color="#0000ff" size="small" animating/>
                        )}
                        <FlatList
                            data={repliesArray}
                            renderItem={this.renderDetail}
                            keyExtractor={(item, index) => index}
                            extraData={this.state}
                        />


                        {(item.replies_count > 1 && repliesArray.length < item.replies_count) && (
                            <TouchableWithoutFeedback
                                onPress={() => getMoreReplies(item.id, item.parent_id)}>
                                <View style={styles.moreReplies}>
                                    {loaders[item.id] ?
                                        <ActivityIndicator color="#0000ff" size="small" animating/> :
                                        <Text style={styles.font}>{repliesCount - repliesArray.length} more
                                            replies</Text>
                                    }
                                </View>
                            </TouchableWithoutFeedback>
                        )}
                    </View>

                )}

                {(item.replies > 0 && showMoreReplies) && (
                    <TouchableWithoutFeedback
                        onPress={() => this.getMoreReplies(item.id, item.parent_id, true)}>
                        <View style={styles.moreReplies}>
                            {this.state.loaders[item.id] ?
                                <ActivityIndicator color="#0000ff" size="small" animating/> :
                                <Text style={styles.font}>{item.replies} more replies</Text>
                            }
                        </View>

                    </TouchableWithoutFeedback>
                )}


            </View>
        );
    }
}


export default withNavigation(Comment);