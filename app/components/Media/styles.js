import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
import {Fonts} from '../../utils/Fonts';
import * as dimens from '../../utils/dimens';
import * as colors from '../../utils/colors';


export default StyleSheet.create({
    media: {
        marginTop: 0,
        // height: 370,
        resizeMode: 'contain',
    },

    video: {
        marginTop: 0,
        height: 300,
    },
    container: {
    },
    messageContainer: {
        position:'absolute',
        bottom: 0,
        left: 0,
        right: 0,
      backgroundColor: colors.lightBlue
    },
    message: {
      padding: 15,
      color: colors.white
    },
    playAction: {},
    playButton: {
        backgroundColor: 'rgba(0, 0, 0, .5  )',
        width: 60,
        height: 60,
        borderRadius: dimens.getRadius(60),
        elevation: 12,
      /*  width: 40,
        height: 40,
        borderRadius: dimens.getRadius(40),
        backgroundColor: 'rgba(0, 0, 0, .8  )'*/
    },
    icon: {
      // padding: 15,
    },

    loader: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      right: 0,
      left: 0,
    },
    backdrop: {
        bottom: 0,
        top: 0,
        left: 0,
        right: 0,
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, .1)'
    },
})