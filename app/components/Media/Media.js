import React from 'react';
import styles from './styles';
import {

    Text,
    View,
    ActivityIndicator,
    TouchableWithoutFeedback,
    Dimensions,
    AppState,
    Platform
} from 'react-native';
import {Video} from 'expo';
import {FileSystem} from "expo";
import SvgUri from 'react-native-svg-uri';
import * as colors from '../../utils/colors';
import * as dimens from '../../utils/dimens';
import {theme} from "../../utils/styles";

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');
import {withNavigation} from 'react-navigation';
import {MaterialCommunityIcons, MaterialIcons, Entypo} from '@expo/vector-icons';

import {connect} from 'react-redux';
import {media} from "../../reducers/media";
import CacheManager from '../../utils/CacheManager';
import {Image} from "react-native-expo-image-cache";
import Player from "../Player/Player";


class Media extends React.PureComponent {

    constructor(props) {
        super(props);

        const {navigation, rootNavigation, item} = this.props;
        const metadata = item.medias[0];

        // const imageIsCached = Expo.FileSystem.readAsStringAsync(metadata.media_url) !== null;

        this.mounted = true;

        this.state = {
            appState: AppState.currentState,
            shouldPlay: false,
            showLoading: false,
            uri: metadata.thumbnail_url,
            width: viewportWidth,
            height: 0,
            // mediaHeight: 370,
            showMessage: false,
            fullImage: false,
        };
        const path = CacheManager.get(metadata.media_url).getPath();


        // CacheManager.get(metadata.media_url).getPath().then(resp => {
        //     this.setState((state) => ({showLoading: false, uri: resp}))
        // });
        //


    }

    // shouldComponentUpdate(nextProps, nextState) {}


    /* shouldComponentUpdate(nextProps, nextState) {
         const {media, item} = nextProps;
         if(item.media.type === 3 && media.isViewable) {
             return true;
         }
         return false;
     }*/

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {media} = nextProps;
        if (media && !media.isViewable && this.state.shouldPlay === true) {
            if (!media.isViewable) {
                this.setState({shouldPlay: false});
            }
        }


    }

    _handlePlayVideo = () => {
        const {isItemHidden} = this.props;
        this.setState((state) => ({
            shouldPlay: !isItemHidden ? false : !state.shouldPlay,
        }));
    };


    _handleImageSize = () => {
        const {item} = this.props;
        const metadata = item.media;
        if (metadata.type !== 2) {
            Image.getSize(metadata.url, (width, height) => {
                this.setState({
                    showMessage: height > 370,
                    height: this.getHeight(width, height),
                    width,
                    showLoading: false
                })
            });
        }
    };

    getHeight = (width, height) => {
        return viewportWidth * height / width;
    };

    _handleVideoSize = (event) => {
        const height = event.naturalSize.height;
        const width = event.naturalSize.width;


        this.setState({
            height: this.getHeight(width, height),
            width: event.naturalSize.width,
            showLoading: false
        });

    };

    _handleShowFullImage = () => {
        this.setState((state) => ({
            fullImage: !state.fullImage,
        }));

    };

    componentDidMount() {


        const {navigation, rootNavigation, item} = this.props;
        const {showLoading, uri} = this.state;

        const metadata = item.medias[0];

        console.log(metadata);

        // if (rootNavigation) {
        //     rootNavigation.addListener('willBlur', () => {
        //         this.setState({shouldPlay: false})
        //     });
        // }
        // navigation.addListener('willBlur', () => {
        //     this.setState({shouldPlay: false})
        // });
        // AppState.addEventListener('change', this._handleAppStateChange);

        // const imageIsCached = Expo.FileSystem.readAsStringAsync(metadata.media_url) !== null;

        // if(uri !== metadata.thumbnail_url) {
        //     CacheManager.get(metadata.media_url).getPath().then(resp => {
        //         setTimeout(() => {
        //             this.setState((state) => ({showLoading: false, uri: resp}))
        //         }, 1000)
        //     });
        // }


        // Expo.FileSystem.getInfoAsync(metadata.media_url).then(response => {
        //     this.setState((state) => ({showLoading: false, uri: response}))
        // });

        // if (this.mounted) {
        //     if (metadata.type !== 3 && showLoading) {
        //         Image.prefetch(metadata.media_url).then(response => {
        //             setTimeout(() => {
        //                 this.setState((state) => ({showLoading: false}))
        //             }, 500)
        //         }).catch(err => {
        //             alert("error");
        //         })
        //     }
        // }


        // this._handleImageSize();
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.setState({shouldPlay: false});

        }
        this.setState({appState: nextAppState});
    };

    _handleToggleLoading = (event) => {
        this.setState((state) => ({showLoading: false}))
    };

    _handleOpenMediaVisualizer = () => {
        const {navigation, rootNavigation, item} = this.props;
        if (rootNavigation) {
            rootNavigation.navigate("MediaVisualizer", {data: item})
        }

    };


    render() {
        const {item, padding, togglePlayVideo, isItemHidden, media} = this.props;
        const {showMessage, showLoading, height, width, shouldPlay} = this.state;

        const metadata = item.medias[0];


        if (metadata.type === 2) {
        }
        /* if (!isItemHidden && shouldPlay && metadata.type === 3) {
             this._handlePlayVideo();
         }*/

        /*  if(metadata.type === 3 && shouldPlay && !media.isViewable) {
              this.setState({shouldPlay: false});
          }
  */

        const mediaHeight = this.getHeight(metadata.width, metadata.height);
        const viewHeight = showLoading ? mediaHeight : mediaHeight;
        const mediaWidth = viewportWidth - padding;

        const thumbsImg = metadata.thumbnail_url ? metadata.thumbnail_url : ""

        const uri = metadata.media_url;
        const preview = {uri: metadata.thumbnail_url};


        return (
            <TouchableWithoutFeedback onPress={this._handleOpenMediaVisualizer}>
                <View>
                    {metadata.media_type_id === 2 && (
                        <TouchableWithoutFeedback style={[styles.container, theme.center, {height: viewHeight}]}
                                                  onPress={this._handlePlayVideo}>
                            <View>
                                <Video
                                    source={{uri: metadata.media_url}}
                                    onLoad={this._handleToggleLoading}
                                    rate={1.0}
                                    volume={1.0}
                                    isMuted={false}
                                    resizeMode="contain"
                                    shouldPlay={shouldPlay}
                                    isLooping={true}
                                    style={[{
                                        height: mediaHeight,
                                        width: mediaWidth
                                    }]}
                                />
                                {(!shouldPlay && !showLoading) && (
                                    <View style={[styles.playAction, theme.center, {
                                        position: 'absolute',
                                        top: 0,
                                        bottom: 0,
                                        left: 0,
                                        right: 0
                                    }]}>
                                        <View style={[styles.playButton, theme.center]}>
                                            <View style={styles.icon}>
                                                <MaterialIcons color={colors.white} size={60}
                                                               name="play-circle-filled"/>
                                            </View>


                                        </View>
                                    </View>
                                )}
                            </View>

                        </TouchableWithoutFeedback>)}

                    {metadata.media_type_id === 3 && (
                        <Player shouldPlay={false} source={metadata.media_url} duration={metadata.duration}/>
                    )}

                    {metadata.media_type_id === 1 && (
                        <View style={[theme.center, {height: viewHeight}]}>

                            {/*<Image*/}
                            {/*source={{uri: this.state.uri}}*/}
                            {/*resizeMethod="scale"*/}
                            {/*blurRadius={showLoading ? dimens.blurRadius : 0}*/}
                            {/*// onLoadStart={this._handleToggleLoading}*/}
                            {/*// onLoadEnd={this._handleToggleLoading}*/}
                            {/*style={{height: mediaHeight, width: mediaWidth,}}*/}
                            {/*/>*/}
                            <Image
                                resizeMethod="scale"
                                style={{height: mediaHeight, width: mediaWidth,}}
                                {...{preview, uri}} />
                            {showLoading && (
                                <View style={[theme.center, styles.loader]}>
                                    <ActivityIndicator size="large" color={colors.colorLoader}/>
                                </View>
                            )}

                            {/*{showMessage && (
                            <TouchableOpacity onPress={this._handleShowFullImage} style={styles.messageContainer}>
                                <Text style={styles.message}>VIEW FULL IMAGE</Text>
                            </TouchableOpacity>
                        )}*/}


                        </View>
                    )}


                </View>
            </TouchableWithoutFeedback>

        );
    }
}

function mapStateToProps({media}, ownProps) {
    const {item} = ownProps;
    return {
        media: media[item.id],
    }
}

export default connect(mapStateToProps)(withNavigation(Media));