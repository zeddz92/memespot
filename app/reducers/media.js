import * as actionTypes from '../actions/types';

export function media(state = {}, action) {
    const payload = action.payload;

    switch (action.type) {
        case actionTypes.REGISTER_MEDIA:
            return {
                ...state,
                [payload.id]: {
                    isViewable: true,
                }
            };
        case actionTypes.MEDIA_PLAY_CONTROL:
            return {
                ...state,
                [payload.id]: {
                    isViewable: payload.isViewable,
                }
            };
        default:
            return state;
    }

}