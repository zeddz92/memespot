import * as actionTypes from '../actions/types';

const initialState = {
    "FEED": [],
    "HOT": [],
    "TRENDING": [],
    "FRESH": [],
    "OWN": [],
    "COMMENTS": [],
    "FAVORITES": []
}

export function post(state = initialState, action) {
    const payload = action.payload;
    // const data = payload.response.data;

    switch (action.type) {
        case actionTypes.RECEIVE_POSTS:
            return {
                ...state,
                [payload.category]: payload.response.current_page === 1 ? payload.response.data : [
                    ...state[payload.category],
                    ...payload.response.data
                ]
            };

        case actionTypes.RECEIVE_POST_FEEDBACK:
            return Object.keys(state).map(key => key).reduce((accumulator, category) => {

                accumulator[category] = state[category].map((post) => (
                    post.id === payload.data.id
                        ? payload.data
                        : post
                ));

                return accumulator;

            }, {});

        case actionTypes.REMOVE_POST:
            return Object.keys(state).map(key => key).reduce((accumulator, category) => {

                accumulator[category] = state[category].filter(post => (post.id !== payload.postId));

                return accumulator;

            }, {});

        case actionTypes.HANDLE_FAVORITE_POST:
            return Object.keys(state).map(key => key).reduce((accumulator, category) => {

                accumulator[category] = state[category].map((post) => (
                    post.id === payload.postId
                        ? { ...post, favorite: payload.action }
                        : post
                ));

                return accumulator;

            }, {});

        default:
            return state;

    }
}