import * as actionTypes from '../actions/types';


export function comment(state = {}, action) {
    const payload = action.payload;

    switch (action.type) {
        case actionTypes.RECEIVE_COMMENTS:
            return {
                ...state,
                [payload.postId]: payload.response.current_page === 1 ? payload.response.data : [
                    ...state[payload.postId],
                    ...payload.response.data
                ]
            };

        case actionTypes.RECEIVE_COMMENT_FEEDBACK:
            return {
                ...state,
                [payload.postId]: state[payload.postId].map(comment => (
                    comment.id === payload.data.id
                        ? payload.data
                        : comment
                ))
            };

        case actionTypes.ADD_COMMENT:
            return !payload.error ? {
                ...state,
                [payload.postId]: [payload.data, ...state[payload.postId]]
            } : state;

        default:
            return state;
    }
}