import {media} from './media';
import {user} from './user';
import {post} from './post';
import {comment} from './comment';

import {navigation} from './navigation';
import {combineReducers} from 'redux';

export default combineReducers({
    user,
    media,
    navigation,
    post,
    comment
})