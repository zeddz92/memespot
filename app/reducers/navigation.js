import * as actionTypes from '../actions/types';

export function navigation(state = {}, action) {
    const payload = action.payload;

    switch (action.type) {
        case actionTypes.SET_NAVIGATION:
            return {
                ...state,
                [payload.id]: payload.navigation
            };
        default:
            return state;
    }

}