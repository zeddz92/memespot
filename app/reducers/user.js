import * as actionTypes from '../actions/types';

const initialState = {
    data: {
        id: 0,
    },
    summary: {},
    following: {},
    settings: {}
}

export function user(state = initialState, action) {
    const payload = action.payload;

    switch (action.type) {
        case actionTypes.RECEIVE_USER_DATA:

            return {
                ...state,
                data: payload.data ? payload.data.user : null,
                summary: payload.data ? payload.data.summary : null,
                following: payload.data ? payload.data.following : null,
                settings: payload.data ? payload.data.settings : null
            };

        case actionTypes.RECEIVE_UPDATED_USER:
            return payload.error?  state : {
                ...state,
                data: {
                    ...state.data,
                    name: payload.data.name || state.data.name,
                    username: payload.data.username || state.data.username,
                    dob: payload.data.dob || state.data.dob,
                    short_bio: payload.data.short_bio || state.data.short_bio,
                    profile_picture_url: payload.data.profile_picture_url || state.data.profile_picture_url,
                }
            };

        case actionTypes.RECEIVE_USER_SETTING :
            return payload.error? state: {
                ...state,
                settings: {
                    ...state.settings,
                    ...payload.data
                }
            };


        case actionTypes.RECEIVE_FOLLOW_USER :

            return payload.error? state: {
                ...state,
                summary: {
                    ...state.summary,
                    following: state.summary.following + 1
                },
                following: {
                    ...state.following,
                    [payload.data.user_id]: true
                }
            };


        case actionTypes.RECEIVE_UNFOLLOW_USER :

            return payload.error? state: {
                ...state,
                summary: {
                    ...state.summary,
                    following: state.summary.following - 1
                },
                following: {
                    ...state.following,
                    [payload.data.user_id]: false
                }
            };

        case actionTypes.LOGOUT:
            return initialState;

        default:
            return state;

    }
}