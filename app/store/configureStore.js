import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {api} from '../utils/api';
import thunk from 'redux-thunk';

import reducer from '../reducers/index';

const logger = store => next => action => {
    // console.group(action.type)
    // console.info('dispatching', action)
    let result = next(action)
    // console.log('next state', store.getState())
    // console.groupEnd(action.type)
    return result
};

const rootReducer = (state, action) => {
    return reducer(state, action)
};

const persistConfig = {
    key: 'root',
    storage: storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose


export default () => {

    let store = createStore(persistedReducer,   composeEnhancers(
        applyMiddleware(logger),
        applyMiddleware(thunk.withExtraArgument(api)),
    ));

    let persistor = persistStore(store);

    return { store, persistor }
}