import * as actionTypes from './types';
import {api} from "../utils/api";


export const authenticateUser = (email, password, token) => (dispatch, getState, api) => (
    api
        .auth
        .login(email, password, token)
        .then((response) => dispatch(receiveUserData(
            response,
            response.status && response.status.error ? {message: response.status.message, code: response.status.code} : false)))
        .catch(error => receiveUserData(null, error))
);

export const updateUserProfile = (userId,profilePicture, name, username, birthDate, shortBio) => (dispatch, getState, api) => (
    api
        .user
        .update(userId,profilePicture, name, username, birthDate, shortBio)
        .then((response) => dispatch(receiveUpdatedUser(
            response,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receiveUpdatedUser(null, error))
);

export function receiveUpdatedUser(response, error) {
    return {
        type: actionTypes.RECEIVE_UPDATED_USER,
        payload: {
            ...response,
        },
        error: error

    }
}


export const updateSetting = (userId, setting, value) => (dispatch, getState, api) => (
    api
        .user
        .updateSetting(userId, setting, value)
        .then((response) => dispatch(receiveUserSettings(
            response,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receiveUpdatedUser(null, error))
);

export function receiveUserSettings(response, error) {
    return {
        type: actionTypes.RECEIVE_USER_SETTING,
        payload: {
            ...response,
        },
        error: error
    }
}


export const register = (name, username, email, password, token) => (dispatch, getState, api) => (
    api
        .auth
        .register(name, username, email, password, token)
        .then((response) => dispatch(receiveUserData(
            response,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receiveUserData(null, error))
);

export const follow = (userId, followerId) => (dispatch, getState, api) => (
    api
        .follow
        .followUser(userId, followerId)
        .then((response) => dispatch(receiveFollowUser(
            response,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receiveFollowUser(null, error))
);

export const unFollow = (userId, followerId) => (dispatch, getState, api) => (
    api
        .follow
        .unFollowUser(userId, followerId)
        .then((response) => dispatch(receiveUnFollowUser(
            response,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receiveUnFollowUser(null, error))
);

export function receiveFollowUser(response, error) {
    return {
        type: actionTypes.RECEIVE_FOLLOW_USER,
        payload: {
            ...response,
        },
        error: error
    }
}


export function receiveUnFollowUser(response, error) {
    return {
        type: actionTypes.RECEIVE_UNFOLLOW_USER,
        payload: {
            ...response,
        },
        error: error
    }
}


export function logOut() {
    return {
        type: actionTypes.LOGOUT,
        payload: null,
        error: false

    }
}



export function receiveUserData(response, error) {

    return {
        type: actionTypes.RECEIVE_USER_DATA,
        payload: {
            ...response,
        },
        error: error

    }
}