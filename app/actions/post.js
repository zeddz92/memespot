import * as actionTypes from './types';


export const getPosts = (userId,criteria, page, category) => (dispatch, getState, api) => (
    api
        .post
        .getPosts(userId,criteria, page, category)
        .then((response) => dispatch(receivePosts(
            response,
            category,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receivePosts(null, null, error))
);


export const makeFeedback = (userId, postId, feedback) => (dispatch, getState, api) => (
    api
        .post
        .feedback(userId, postId, feedback)
        .then((response) => dispatch(receiveFeedback(
            response,
            feedback,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receiveFeedback(null, null, error))
);

export function receiveFeedback(response, feedback, error) {

    return {
        type: actionTypes.RECEIVE_POST_FEEDBACK,
        payload: {
            data: response.data,
            feedback
        },
        error: error

    }
}

export function removePost(postId, error) {

    return {
        type: actionTypes.REMOVE_POST,
        payload: {
            postId,
        },
        error: error

    }
}

export function handleFavorite(postId, action, error) {

    return {
        type: actionTypes.HANDLE_FAVORITE_POST,
        payload: {
            postId,
            action
        },
        error: error

    }
}

export function receivePosts(response, category, error) {

    return {
        type: actionTypes.RECEIVE_POSTS,
        payload: {
            response,
            category
        },
        error: error

    }
}