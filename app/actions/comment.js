import * as actionTypes from './types';

export const getComments = (userId, postId, page) => (dispatch, getState, api) => (
    api
        .comment
        .getAll(userId, postId, page)
        .then((response) => dispatch(receiveComments(
            response,
            postId,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receiveComments(null, null, error))
);

export const makeFeedback = (userId, postId, commentId, feedback) => (dispatch, getState, api) => (
    api
        .comment
        .feedback(userId, commentId, feedback)
        .then((response) => dispatch(receiveFeedback(
            response,
            postId,
            feedback,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => receiveFeedback(null, null, null, error))
);

export const createComment = (postId, userId, text, media, parentId) => (dispatch, getState, api) => (
    api
        .comment
        .create(postId, userId, text, media, parentId)
        .then((response) => dispatch(addComment(
            response,
            postId,
            response.error ? {message: response.error, code: response.code} : false)))
        .catch(error => addComment(null, null, error))
);

export function addComment(response, postId, error) {
    return {
        type: actionTypes.ADD_COMMENT,
        payload: {
            data: response.data,
            postId
        },
        error: error
    }
}


export function receiveFeedback(response, postId, feedback, error) {

    return {
        type: actionTypes.RECEIVE_COMMENT_FEEDBACK,
        payload: {
            data: response.data,
            postId,
            feedback
        },
        error: error

    }
}

export function receiveComments(response, postId, error) {

    return {
        type: actionTypes.RECEIVE_COMMENTS,
        payload: {
            response,
            postId
        },
        error: error

    }
}