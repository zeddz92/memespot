import * as actionTypes from './types';

export const setNavigation = (id, navigation) => {
    return {
        type: actionTypes.SET_NAVIGATION,
        payload: {id, navigation},
        error: false
    }
};