import * as actionTypes from './types';

export const registerMedia = (id) => {
    return {
        type: actionTypes.REGISTER_MEDIA,
        payload: {id, shouldPlay: true},
        error: false
    }
};

export const playControlsMedia = (id, isViewable) => {
    return {
        type: actionTypes.MEDIA_PLAY_CONTROL,
        payload: {id, isViewable},
        error: false
    }
};