import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    KeyboardAvoidingView,
    ScrollView,
    ActivityIndicator,
    TextInput,
    DatePickerIOS,
    DatePickerAndroid,
    FlatList,
    Platform,
} from 'react-native';

import * as dimens from '../../utils/dimens';
import * as colors from '../../utils/colors';
import {theme} from '../../utils/styles';
import DatePicker from '../../components/DatePicker';
import {connect} from "react-redux";
import {validate} from "../../utils/validators";
import {ImagePicker} from 'expo';
import moment from 'moment';
import * as userActions from "../../actions/user";
import SnackBar, {DURATION} from '../../components/SnackBar';


function getCurrentParams(state) {
    if (state.routes) {
        return getCurrentParams(state.routes[state.index]);
    }
    return state.params || {};
}


class EditProfile extends React.Component {

    constructor(props) {
        super(props);

        const defaultProfilePicture = Expo.Asset.fromModule(require('../../assets/img/profile.png')).localUri;
        this.state = {
            textInputName: props.user.data.name || "",
            profilePicture: props.user.data.profile_picture_url || defaultProfilePicture,
            textInputUserName: props.user.data.username || "",
            textBirthDate: props.user.data.dob || null,
            textInputShortBio: props.user.data.short_bio || ""
        }

        this.shouldEnableSaveBtn = false;
        // this.scrollOffsetY = 15;
        this.focusNextField = this.focusNextField.bind(this);


    }

    static navigationOptions = ({navigation}) => {

        const request = navigation.state.params ? navigation.state.params.actions.request : null;
        const showLoading = navigation.state.params ? navigation.state.params.showLoading : null;

        return {
            headerRight:
                <TouchableOpacity
                    onPress={request}
                    style={{marginEnd: 15}}>
                    {showLoading ?
                        <ActivityIndicator color={colors.colorLoader}/> :
                        <Text style={{color: colors.colorLink, fontWeight: 'bold'}}>SAVE</Text>
                    }
                </TouchableOpacity>
        }
    };


    componentDidMount() {
        const params = {
            actions: {
                request: this.updateUser,
                showLoading: false,
            }
        };
        this.checkPermissions();
        this.props.navigation.setParams(params);

    }

    UNSAFE_componentWillUpdate(nextProps, nextState) {

        const {textInputName, textInputUserName} = nextState;

        this.shouldEnableSaveBtn = validate([
            {name: 'name', text: textInputName, type: 'text', minLength: 3},
            {name: 'username', text: textInputUserName, type: 'text', minLength: 3},
        ]);
    }

    _pickImage = async () => {

        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            base64: true,
            exif: true,
        });


        const base64Image = `data:image/jpeg;base64,${result.base64}`;
        if (result.base64) {
            this.setState({profilePicture: base64Image});

        }
    };

    checkPermissions = async () => {
        const {Permissions} = Expo;
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('In order to upload photos you should enable camera roll permissions');
        }
    }

    onDateChanged = (date) => {

        const dob = moment(date).format("YYYY-MM-DD");
        this.setState({textBirthDate: dob})
    };

    updateUser = () => {
        const {user} = this.props;
        const {textInputName, textInputUserName, textInputShortBio, textBirthDate, profilePicture} = this.state;

        // if (this.shouldEnableSaveBtn) {

        this.setState({showLoading: true});
        this.props.navigation.setParams({showLoading: true});


        this.props.updateProfile(user.data.id, profilePicture, textInputName, textInputUserName, textBirthDate, textInputShortBio).then(response => {
            this.props.navigation.setParams({showLoading: false});
            if (response.error) {
                this.refs.snackBar.show(this.getError(response.error), DURATION.LENGTH_LONG)

            } else {
                this.props.navigation.goBack();
            }
        })
            .catch(err => {
                // alert(JSON.stringify(err))
            })


        // }

    };

    getError = (response) => {
        const {message} = response;
        return message.username || message.name || message;
    };

    focusNextField(key) {
        this.refs[key].focus();
    }


    render() {
        const scrollToNextField = Platform.OS === 'android' ? 25 : this.scrollOffsetY;
        const {textInputName, textInputUserName, textBirthDate, textInputShortBio, profilePicture} = this.state;
        return (
            <KeyboardAvoidingView
                behavior='padding'
                keyboardVerticalOffset={dimens.keyboardVerticalOffset}
                style={[theme.container]}>

                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    onScroll={(event) => {
                        this.scrollOffsetY = event.nativeEvent.contentOffset.y
                    }}
                    ref="scrollView">
                    <TouchableOpacity
                        onPress={this._pickImage}
                        style={[styles.imageContainer, theme.center]}>
                        <Image style={[styles.profileImg]}
                               source={{uri: profilePicture}}/>

                        <Text style={[theme.font, theme.link, {marginTop: 10, fontWeight: 'bold', fontSize: 18}]}>Change
                            Picture</Text>
                        {/*<View style={styles.addPhotoButton}>
                        <SvgUri fill={colors.lightBlue} width={25} height={25}
                                source={require('../assets/icons/camera.svg')}/>
                    </View>*/}
                    </TouchableOpacity>


                    <View style={styles.form}>
                        <View style={[theme.row, styles.inputContainer]}>
                            <Text style={[theme.font, styles.itemText]}>Name</Text>
                            <TextInput
                                ref="textInputName"
                                style={{flex: 1}}
                                value={textInputName}
                                blurOnSubmit={false}
                                onChangeText={(text) => this.setState({textInputName: text})}
                                returnKeyType={"next"}
                                autoCorrect={false}
                                underlineColorAndroid='transparent' style={[theme.font, styles.input]}
                                placeholder="Name"
                                onSubmitEditing={() => {
                                    this.refs.scrollView.scrollTo({
                                        x: 0, y: scrollToNextField, animated: true
                                    });
                                    this.focusNextField('textInputUserName');
                                }}
                            />
                        </View>

                        <View style={[theme.row, styles.inputContainer]}>
                            <Text style={[theme.font, styles.itemText]}>Username</Text>
                            <TextInput
                                ref="textInputUserName"
                                value={textInputUserName}
                                onChangeText={(text) => this.setState({textInputUserName: text})}
                                blurOnSubmit={false}
                                autoCapitalize="none"
                                autoCorrect={false}
                                returnKeyType={"next"}
                                underlineColorAndroid='transparent' style={[theme.font, styles.input]}
                                placeholder="Username"
                                onSubmitEditing={() => {
                                    this.refs.scrollView.scrollTo({
                                        x: 0,
                                        y: scrollToNextField,
                                        animated: true
                                    });
                                    this.focusNextField('textInputShortBio');
                                }}/>
                        </View>


                        <View style={[theme.row, styles.inputContainer]}>
                            <Text style={[theme.font, styles.itemText]}>Short Bio</Text>
                            <TextInput
                                ref="textInputShortBio"
                                value={textInputShortBio}
                                onChangeText={(text) => this.setState({textInputShortBio: text})}

                                autoCorrect={false}
                                blurOnSubmit={true}
                                underlineColorAndroid='transparent' style={[theme.font, styles.input]}
                                placeholder="Short Bio"/>
                            {/*<T style={[theme.font]}>Trying to conquer the world</T>*/}
                        </View>

                        <TouchableOpacity
                            onPress={() => this.refs.datePicker.open()}
                            style={[theme.row, styles.inputContainer]}>
                            <Text style={[theme.font, styles.itemText]}>Birthday</Text>
                            <Text style={[theme.font]}>{textBirthDate}</Text>
                        </TouchableOpacity>


                        {/*<TextInput underlineColorAndroid='transparent' style={[theme.font, theme.input]} placeholder="Username"/>
                    <TextInput underlineColorAndroid='transparent' style={[theme.font, theme.input]} placeholder="Birthday"/>
                    <TextInput underlineColorAndroid='transparent' style={[theme.font, theme.input]} placeholder="Gender"/>*/}
                    </View>

                </ScrollView>


                <DatePicker ref="datePicker" changeDate={this.onDateChanged}/>
                <SnackBar ref="snackBar"/>

            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    form: {
        flex: 1,
    },
    input: {
        flex: 1,
        textAlign: 'right'
    },
    inputContainer: {
        padding: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: colors.colorBorder,
        height: 50,
        marginBottom: 10,
    },
    imageContainer: {
        margin: 15,
    },
    itemText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#3D3F41',
    },
    addPhotoButton: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        bottom: -14,
        width: 40,
        height: 40,
        borderRadius: dimens.getRadius(40),
        backgroundColor: 'red',
    },


    profileImg: {
        width: 110,
        height: 110,
        borderWidth: 1,
        resizeMode: 'cover',
        borderRadius: dimens.getRadius(110),
        borderColor: colors.lightBlue
    },
})

function mapStateToProps({user}) {
    return {
        user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateProfile: (userId, profilePicture, name, username, birthDate, shortBio) => dispatch(userActions.updateUserProfile(userId, profilePicture, name, username, birthDate, shortBio)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
