import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert,
    TouchableOpacity,
    ActivityIndicator,
    TextInput,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
} from 'react-native';

import * as dimens from '../../utils/dimens';
import * as colors from '../../utils/colors';
import {theme} from '../../utils/styles';
import SvgUri from 'react-native-svg-uri';
import {validate} from "../../utils/validators";
import {api} from '../../utils/api';
import SnackBar, {DURATION} from '../../components/SnackBar';
import {connect} from "react-redux";
import * as userActions from "../../actions/user";


class ChangePassword extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentPassword: '',
            password: '',
            passwordConfirmation: ''
        };

        this.shouldEnableSaveBtn = false;
        // this.scrollOffsetY = 15;
        this.focusNextField = this.focusNextField.bind(this);

    }

    static navigationOptions = ({navigation}) => {

        const request = navigation.state.params ? navigation.state.params.actions.request : null;
        const showLoading = navigation.state.params ? navigation.state.params.showLoading : null;

        return {
            headerRight:
                <TouchableOpacity
                    onPress={request}
                    style={{marginEnd: 15}}>
                    {showLoading ?
                        <ActivityIndicator color={colors.colorLoader}/> :
                        <Text style={{color: colors.colorLink, fontWeight: 'bold'}}>DONE</Text>
                    }
                </TouchableOpacity>
        }
    };

    componentDidMount() {
        const params = {
            actions: {
                request: this.attemptChangePassword,
                showLoading: false,
            }
        };
        this.props.navigation.setParams(params);
    }

    UNSAFE_componentWillUpdate(nextProps, nextState) {

        const {currentPassword, password, passwordConfirmation} = nextState;

        this.shouldEnableSaveBtn = validate([
            {name: 'currentPassword', text: currentPassword, type: 'text', minLength: 6},
            {name: 'password', text: password, type: 'text', minLength: 6},
            {name: 'passwordConfirmation', text: password, type: 'text', minLength: 6},
        ]);

        this.shouldEnableSaveBtn = password === passwordConfirmation ? this.shouldEnableSaveBtn : false;
    }

    attemptChangePassword = () => {
        const {currentPassword, password, passwordConfirmation} = this.state;
        const {user} = this.props;

        if (this.shouldEnableSaveBtn) {
            this.props.navigation.setParams({showLoading: true});

            api.auth.changePassword(user.data.id, currentPassword, password, passwordConfirmation)
                .then(response => {
                    this.props.navigation.setParams({showLoading: false});
                    // alert(JSON.stringify(response));
                    if (response.error) {
                        this.refs.snackBar.show(this.getError(response.error), DURATION.LENGTH_LONG)
                    } else {
                        this.props.navigation.goBack();
                    }

                })
                .catch(err => {
                    alert("error");
                    this.props.navigation.setParams({showLoading: false});
                })
        }
    };

    attemptSendResetPassword = () => {

        const {user} = this.props;
        const email = user.data.email;

        this.setState({showLoading: true});

        setTimeout(()=> {
            this.setState({showLoading: false});
            Alert.alert(
                'Password reset',
                'Password reset sent successfully',
                [
                    {text: 'OK', onPress: () => console.log("send")},
                ],
                {cancelable: false}
            )
        }, 3000);



    };

    promptSendResetPassword = () => {
        Alert.alert(
            'Password reset',
            'You are about to send a password reset link, are you sure?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () =>this.attemptSendResetPassword()},
            ],
            {cancelable: false}
        )

    };

    getError = (error) => {
        return error.current_password || error.password || error.password_confirmation || error;
    };


    focusNextField(key) {
        this.refs[key].focus();
    }

    render() {

        const scrollToNextField = Platform.OS === 'android' ? 25 : this.scrollOffsetY;
        const {currentPassword, password, passwordConfirmation, showLoading} = this.state;

        return (
            <KeyboardAvoidingView
                behavior='padding'
                keyboardVerticalOffset={dimens.keyboardVerticalOffset}
                style={theme.container}>
                <ScrollView
                    ref="scrollView"
                    keyboardShouldPersistTaps={'handled'}
                    onScroll={(event) => {
                        this.scrollOffsetY = event.nativeEvent.contentOffset.y
                    }}>
                    <Text style={[theme.font, styles.title]}>Account Information</Text>
                    <View style={styles.form}>
                        <TextInput
                            ref="currentPassword"
                            value={currentPassword}
                            onChangeText={(text) => this.setState({currentPassword: text})}
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            onSubmitEditing={() => {
                                this.refs.scrollView.scrollTo({x: 0, y: scrollToNextField, animated: true});
                                this.focusNextField('password');
                            }}
                            underlineColorAndroid='transparent'
                            style={[theme.font, theme.input, styles.inputPadding]}
                            placeholder="Current Password"/>
                        <TextInput
                            ref="password"
                            value={password}
                            onChangeText={(text) => this.setState({password: text})}
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            onSubmitEditing={() => {
                                this.refs.scrollView.scrollTo({x: 0, y: scrollToNextField, animated: true});
                                this.focusNextField('passwordConfirmation');
                            }}
                            underlineColorAndroid='transparent'
                            style={[theme.font, theme.input, styles.inputPadding]}
                            placeholder="New Password"/>
                        <TextInput
                            ref="passwordConfirmation"
                            value={passwordConfirmation}
                            onChangeText={(text) => this.setState({passwordConfirmation: text})}
                            returnKeyType={"done"}
                            blurOnSubmit={true}
                            onSubmitEditing={this.attemptChangePassword}
                            underlineColorAndroid='transparent'
                            style={[theme.font, theme.input, styles.inputPadding]}
                            placeholder="Confirm New Password"/>
                    </View>

                    <TouchableOpacity
                        onPress={this.promptSendResetPassword}
                        style={[styles.forgotPasswordButton]}>
                        <Text style={[theme.font, theme.link, styles.forgotPasswordText]}>FORGOT PASSWORD?</Text>
                    </TouchableOpacity>


                    {showLoading && (
                        <ActivityIndicator style={{marginTop: 25,}} size="large" />
                    )}
                </ScrollView>


                <SnackBar ref="snackBar"/>

            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    forgotPasswordText: {
        fontWeight: 'bold'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#3D3F41',
        marginTop: 15,
        marginBottom: 15,
        paddingStart: 15,
        paddingEnd: 15,
    },
    forgotPasswordButton: {
        marginTop: 15,
        paddingStart: 15,
    },
    form: {
        // marginTop: 15,
    },
    inputPadding: {
        paddingStart: 15,
        paddingEnd: 15,
        fontSize: 16
    }
})

function mapStateToProps({user}) {
    return {
        user
    }
}


export default connect(mapStateToProps)(ChangePassword);

