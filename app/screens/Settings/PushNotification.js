import React from 'react';
import {
    StyleSheet,
    View,
    Switch,
    Text,
    TouchableOpacity,
    Image,
    TextInput,
    FlatList,
    Platform,
} from 'react-native';

import * as dimens from '../../utils/dimens';
import * as colors from '../../utils/colors';
import {theme} from '../../utils/styles';
import SvgUri from 'react-native-svg-uri';
import {connect} from "react-redux";
import * as userActions from "../../actions/user";


const NotificationSwitch = (props) => {

    const {enabled, onValueChange} = props;

    const thumbTintColor = enabled ? colors.colorSwitchThumb : "";
    const value = typeof enabled === 'number'? enabled === 1 : enabled;

    return (
        <Switch
            thumbTintColor={thumbTintColor}
            onTintColor={colors.colorSwitchOn}
            value={value}
            onValueChange={onValueChange}
        />
    )

};


class PushNotification extends React.PureComponent {

    constructor(props) {
        super(props);

        const {user} = props;
        this.state = {

            notification_post_feedback: user.settings.notification_post_feedback,
            notification_post_reply: user.settings.notification_post_reply,
            notification_comment_feedback: user.settings.notification_comment_feedback,
            notification_comment_reply: user.settings.notification_comment_reply,
            notification_follow: user.settings.notification_follow,

        }
    }

    updateNotification = (setting) => {


        const {user} = this.props;
        const value = !user.settings[setting];

        this.setState({[setting]: value});

        this.props.updateSetting(user.data.id, setting, value).then(response => {

            const payload = response.payload;
            if(!payload.error) {
                // this.setState(
                //     payload.data
                // )
            }
        })
    };

    render() {
        const {user} = this.props;
        const {settings} = user;
        const {notification_post_feedback, notification_post_reply,
            notification_comment_feedback, notification_comment_reply, notification_follow} = this.state;


        return (
            <View style={theme.container}>

                <View style={[theme.row, styles.section]}>
                    <Text style={[theme.font, styles.text]}>Likes to your posts</Text>
                    <NotificationSwitch
                        onValueChange={() => this.updateNotification("notification_post_feedback")}
                        enabled={notification_post_feedback}/>
                </View>

                <View style={[theme.row, styles.section]}>
                    <Text style={[theme.font, styles.text]}>Replies to your posts</Text>
                    <NotificationSwitch
                        onValueChange={() => this.updateNotification("notification_post_reply")}
                        enabled={notification_post_reply}/>
                </View>

                <View style={[theme.row, styles.section]}>
                    <Text style={[theme.font, styles.text]}>Likes on your comment</Text>
                    <NotificationSwitch
                        onValueChange={() => this.updateNotification("notification_comment_feedback")}
                        enabled={notification_comment_feedback}/>
                </View>

                <View style={[theme.row, styles.section]}>
                    <Text style={[theme.font, styles.text]}>Replies to your comment</Text>
                    <NotificationSwitch
                        onValueChange={() => this.updateNotification("notification_comment_reply")}
                        enabled={notification_comment_reply}/>
                </View>

                <View style={[theme.row, styles.section]}>
                    <Text style={[theme.font, styles.text]}>Follow request</Text>
                    <NotificationSwitch
                        onValueChange={() => this.updateNotification("notification_follow")}
                        enabled={notification_follow}/>
                </View>

            </View>
        )
    }

}


const styles = StyleSheet.create({
    section: {
        padding: 15,
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    text: {
        // fontWeight: 'bold',
        // color: colors.colorSettingsText,
        fontSize: 18,
    }


})


function mapStateToProps({user}) {
    return {
        user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateSetting: (userId, setting, value) => dispatch(userActions.updateSetting(userId, setting, value)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(PushNotification);


