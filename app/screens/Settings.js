import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Alert,
    AsyncStorage,
    Image,
    FlatList,
    Platform,
} from 'react-native';
import {MaterialCommunityIcons, MaterialIcons, Entypo} from '@expo/vector-icons';

import * as dimens from '../utils/dimens';
import * as colors from '../utils/colors';
import {theme} from '../utils/styles';
import SvgUri from 'react-native-svg-uri';
import {connect} from "react-redux";
import * as userActions from "../actions/user";

const attemptLogOut = (props) => {

    Alert.alert(
        'Logout',
        'Are you sure you want to logout?',
        [
            {text: 'Cancel', onPress: () => console.log("cancel")},
            {
                text: 'Ok', onPress: () => {
                    props.logOut();
                    AsyncStorage.removeItem("userId");
                    props.navigation.goBack();
                }
            },
        ],
        {cancelable: false}
    )
};


const Settings = (props) => {
    return (
        <View style={theme.container}>
            <View style={styles.section}>
                <View style={styles.sectionTitleContainer}>
                    <Text style={[theme.font, styles.title]}>ACCOUNT</Text>
                </View>

                <TouchableOpacity
                    onPress={() => props.navigation.navigate("EditProfile")}
                    style={styles.sectionItems}>
                    <Text style={[theme.font, styles.itemText]}>Edit profile</Text>
                    <MaterialIcons style={styles.icon} name="navigate-next" size={dimens.actionSize}
                                   color={colors.colorActions}/>

                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => props.navigation.navigate("ChangePassword")}
                    style={styles.sectionItems}>
                    <Text style={[theme.font, styles.itemText]}>Change Password</Text>
                    <MaterialIcons style={styles.icon} name="navigate-next" size={dimens.actionSize}
                                   color={colors.colorActions}/>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => props.navigation.navigate("PushNotification")}
                    style={styles.sectionItems}>
                    <Text style={[theme.font, styles.itemText]}>Notifications</Text>
                    <MaterialIcons style={styles.icon} name="navigate-next" size={dimens.actionSize}
                                   color={colors.colorActions}/>
                </TouchableOpacity>
            </View>

            <View style={styles.section}>
                <View style={styles.sectionTitleContainer}>
                    <Text style={[theme.font, styles.title]}>ABOUT</Text>
                </View>

                <TouchableOpacity style={styles.sectionItems}>
                    <Text style={[theme.font, styles.itemText]}>Content policy</Text>
                    <MaterialIcons style={styles.icon} name="navigate-next" size={dimens.actionSize}
                                   color={colors.colorActions}/>
                </TouchableOpacity>

                <TouchableOpacity style={styles.sectionItems}>
                    <Text style={[theme.font, styles.itemText]}>Privacy Policy</Text>
                    <MaterialIcons style={styles.icon} name="navigate-next" size={dimens.actionSize}
                                   color={colors.colorActions}/>
                </TouchableOpacity>
            </View>

            <View style={styles.section}>
                <View style={styles.sectionTitleContainer}>
                    <Text style={[theme.font, styles.title]}>APP VERSION</Text>
                </View>

                <View style={[styles.sectionItems, styles.versionContainer, {marginTop: 0}]}>
                    <Text style={[theme.font, styles.itemText, styles.versionText]}>1.0</Text>
                </View>


            </View>

            <TouchableOpacity
                onPress={() => attemptLogOut(props)}
                style={styles.logOUtButton}>
                <Text style={[styles.font, theme.link, styles.floatRight, styles.logOut,]}>LOG OUT</Text>
            </TouchableOpacity>


        </View>
    )
};


const styles = StyleSheet.create({
    item: {
        // marginBottom: 10,
    },
    logOut: {
        fontWeight: 'bold'
    },
    versionContainer: {
        padding: 10,
        borderBottomWidth: .2,
        borderColor: colors.colorBorder
    },
    versionText: {
        color: '#C3C4C5',
        fontWeight: 'bold'
    },
    logOUtButton: {
        marginTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: 'flex-end'
    },
    section: {
        marginBottom: 15,
    },
    title: {
        fontSize: 12,
        color: colors.colorActions,
        fontWeight: 'bold'
    },
    itemText: {
        fontSize: 16,
        fontWeight: 'bold',

        color: colors.colorSettingsText,
    },
    sectionItems: {
        marginTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    sectionTitleContainer: {
        padding: 15,
        backgroundColor: "#F9F7F9"

    }

});


function mapDispatchToProps(dispatch) {
    return {
        logOut: () => dispatch(userActions.logOut()),
    }
}


export default connect(null, mapDispatchToProps)(Settings);

// export default Settings;
