import React from 'react';



import {connect} from 'react-redux';

import Profile from './Profile';
import GuestProfile from './GuestProfile';


const ProfileTab = (props) => {

    const {user} = props;
    if (user.data && user.data.id !== 0) {
        return (<Profile {...props}/>)
    }
    return (<GuestProfile {...props}/>)
};

function mapStateToProps({user}) {
    return {
        user
    }
}


export default connect(mapStateToProps)(ProfileTab);