import React from 'react';
import {
    StyleSheet,
    View,
    FlatList,
    Alert,
    ActivityIndicator,
    Platform,
} from 'react-native';

import Follower from '../components/Follower';
import {api} from "../utils/api";
import {theme} from "../utils/styles";
import * as userActions from "../actions/user";
import {connect} from "react-redux";
import SnackBar, {DURATION} from '../components/SnackBar';

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

class Followers extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            follows: [],
            refreshing: false,
            showLoading: true,
            error: null,
            page: 1,
            currentPage: 0,
            lastPage: 0,
        }
    }

    static navigationOptions = ({navigation}) => {

        const title = navigation.state.params ? navigation.state.params.criteria : "Follow";


        return {
            headerTitle: capitalize(title)
        }
    };

    componentDidMount() {
        this.makeFollowsRequest();
    }

    renderFollower = ({item, index}) => {
        return (
            <Follower follow={this.handleFollow} unFollow={this.handleUnFollow} data={item}/>
        );
    };


    handleFollow = (follow) => {
        const {user} = this.props;
        this.props.follow(user.data.id, follow.id).then(response => {


            if (response.error) {
                this.refs.snackBar.show(this.getError(response.error), DURATION.LENGTH_LONG)
            }


        }).catch(err => {
            alert("error following")
        });

    };

    handleUnFollow = (follow) => {
        Alert.alert(
            'MemePot',
            `Do you want to stop following ${follow.username}?`,
            [
                {text: 'Nop', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {
                    text: 'Stop Following', onPress: () => {
                        const {user} = this.props;
                        this.props.unFollow(user.data.id, follow.id).then(response => {

                            if (response.error) {
                                this.refs.snackBar.show(this.getError(response.error), DURATION.LENGTH_LONG)
                            }

                        }).catch(err => {
                            alert("error unfollowing")
                        });
                    }
                },
            ],
            {cancelable: false}
        )

    };

    makeFollowsRequest = () => {
        const {page} = this.state;
        const {criteria, userId} = this.props.navigation.state.params;
        api.follow.getFollows(criteria, userId, page).then(response => {
            this.setState((state) => ({
                follows: response.data ? page === 1 ? response.data : [...state.follows, ...response.data] : [],
                refreshing: false,
                showLoading: false,
                currentPage: response.current_page,
                lastPage: response.last_page,
                error: response.code !== 200 ? {message: response.error, code: response.code} : null
            }));

        }).catch(error => {
            this.setState((state) => ({
                error,
                refreshing: false,
                showLoading: false,
                currentPage: 0,
                lastPage: 0,
                showFooter: false,
            }));
        });
    };

    _handleRefresh = () => {
        this.setState({
            page: 1,
            refreshing: true,
        }, () => {
            this.makeFollowsRequest();
        })
    };

    _handleLoadMore = () => {
        const {currentPage, lastPage} = this.state;
        if (currentPage < lastPage) {
            this.setState({
                page: this.state.page + 1
            }, () => {
                this.makeFollowsRequest();
            })
        }
    };

    renderFooter = () => {
        if (!this.state.showLoading) return null;
        return (
            <View style={styles.footerLoader}>
                <ActivityIndicator animating size="large"/>
            </View>
        )
    }


    render() {

        const {follows, refreshing, showLoading} = this.state;
        return (
            <View style={styles.container}>
                {showLoading ?
                    <View style={{flex: 1}}>
                        <ActivityIndicator style={theme.loader} size="large" color="#0000ff"/>
                    </View> :
                    <FlatList
                        data={follows}
                        refreshing={refreshing}
                        renderItem={this.renderFollower}
                        onRefresh={this._handleRefresh}
                        onEndReached={this._handleLoadMore}
                        onEndReachedThreshol={10}
                        ListFooterComponent={this.renderFooter}
                    />}

                <SnackBar ref="snackBar"/>
            </View>
        );
    }


};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps({user}) {
    return {
        user
    }
}


function mapDispatchToProps(dispatch) {
    return {
        follow: (userId, followerId) => dispatch(userActions.follow(userId, followerId)),
        unFollow: (userId, followerId) => dispatch(userActions.unFollow(userId, followerId)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Followers);
