import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    Platform,
} from 'react-native';
import {connect} from "react-redux";
import * as colors from "../utils/colors";
import {theme} from "../utils/styles";

const GuestProfile = (props) => {
    return (
        <View style={[theme.container, theme.center]}>
            <Image style={styles.logo}
                   source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo_TV_2015.png'}}/>
            <View style={styles.welcomeMessage}>
                <Text style={[theme.font, styles.primaryText]}>Welcome! time to have some fun</Text>
                <Text style={[theme.font, styles.secondaryText]}>Have some fun and provide others with that fun.
                    Happiness is for everybody</Text>
            </View>

            <View style={styles.actions}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate("Login")}
                    style={[theme.button, styles.button, {marginLeft: 15}]}>
                    <Text style={[theme.font,  theme.buttonText]}>LOGIN</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate("SignUp")}
                    style={[theme.button, styles.button, {marginRight: 15, backgroundColor: colors.colorGoogle}]}>
                    <Text style={[theme.font,  theme.buttonText]}>SIGN UP</Text>
                </TouchableOpacity>

            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    welcomeMessage: {
        alignItems: 'center',
        padding: 15,
        marginBottom: 15,
    },
    primaryText: {
        fontWeight: 'bold',
        marginBottom: 5,
        fontSize: 18,
    },

    button: {
        flex: 1,
        width: '50%',
        margin: 5,
    },

    secondaryText: {
        fontSize: 16,
        color: 'grey',
        textAlign: 'center'
    },

    logo: {
        resizeMode: 'contain',
        marginBottom: 35,
        width: '100%',
        height: 60
    },

    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },

    actions: {
        flexDirection: 'row',

    },
});


export default GuestProfile;