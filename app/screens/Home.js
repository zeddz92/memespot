import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    ScrollView,
    FlatList,
    Animated,
    InteractionManager,
    VirtualizedList,
    SectionList
} from 'react-native';
import Post from '../components/Post';
import {PostTabsWithoutFeed, PostTabsWithFeed} from '../navigation/PostTabs';


import {posts} from '../utils/mocks/posts';
import SvgUri from 'react-native-svg-uri';
import * as colors from '../utils/colors';
import * as dimens from '../utils/dimens';
import * as mediaActions from '../actions/media';
import * as postActions from '../actions/post';

import * as navigationActions from '../actions/navigation';

import {Constants} from 'expo';
import {connect} from 'react-redux';
import {withNavigation, Header} from 'react-navigation';


class Home extends React.Component {


    constructor(props) {
        super(props);


        this.state = {
            refreshing: false,
            page: 1,
            _animatedValue: new Animated.Value(0),
            _animatedValueView: new Animated.Value(80)
        };

        this.feedRefs = {};
        this._animatedValue = new Animated.Value(0);
        this._animatedValueView = new Animated.Value(80);

    }

    // static navigationOptions = ({navigation, screenProps}) => ({
    //     // header:null
    // })

    // static navigationOptions = ({navigation, screenProps}) => ({
    //     // headerTitle: "activo",
    //     headerStyle: {
    //
    //     },
    // })

    // static navigationOptions = ({navigation}) => ({
    //     headerStyle: {
    //         // position: 'absolute',
    //         backgroundColor: 'red',
    //         bottom: (!navigation.state.params ? 0 : navigation.state.params.animatedValue),
    //         left: 0,
    //         right: 0,
    //         overflow: 'hidden',
    //     },
    //     // header: navigation.state.params ? navigation.state.params.header : undefined,
    //
    // });

    // static navigationOptions = {
    //     title: moment().format('dd, Do of MMMM'),
    //     header: ({state}) => {
    //         return {
    //             style: {
    //                 position: 'absolute',
    //                 top: (!state.params ? 0 : state.params.animatedValue),
    //                 left: 0,
    //                 right: 0,
    //                 overflow: 'hidden',
    //             }
    //         };
    //     }

    // componentDidUpdate(prevProps) {
    //     this.setState({
    //         _animatedValueView: nextState._animatedValue
    //     })
    // }

    componentDidMount() {
        const {user} = this.props;
        // this.props.setNavigation("root", this.props.navigation);
        // this.props.getPosts("").then(response => {
        //     alert(JSON.stringify(response))
        // }).catch(err => alert(JSON.stringify(err)))

        const HEADER_MAX_HEIGHT = 60
        const HEADER_MIN_HEIGHT = 60
        const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT

        // this.props.navigation.setParams({animatedValue: this._animatedValue.interpolate({
        //         inputRange: [0, HEADER_SCROLL_DISTANCE],
        //         outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
        //         extrapolate: 'clamp',
        //     })
        // });
        //
        this.props.navigation.setParams({
            animatedValue: this.state._animatedValue.interpolate({
                inputRange: [0, 80],
                outputRange: [0, -80],
                extrapolate: 'clamp'
            })
        });

        this.state._animatedValueView.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp'
        })

    }

    componentWillMount() {

    }

    handleOnScroll = (event) => {
        // console.log("scrolling");
        const value = event.nativeEvent.contentOffset.y;
        // Animated.timing(                  // Animate over time
        //     this._animatedValue,            // The animated value to drive
        //     {
        //         toValue: value,                   // Animate to opacity: 1 (opaque)
        //
        //     }
        // ).start();
        // Animated.event([{nativeEvent: {contentOffset: {y: this._animatedValue}}}])
    };

    handleShowHeader = (shouldShow, dif, offset, thisof) => {

        // this.props.navigation.setParams({
        //     display: 'none'
        // });


        if (shouldShow) {
            this.props.navigation.setParams({
                header: undefined,
            });
        } else {
            this.props.navigation.setParams({
                header: null
            });
        }
        // alert("val");


        //Animated.event([{nativeEvent: {contentOffset: {y: offset}}}])

    };

    render() {
        const {refreshing} = this.state;
        const {user} = this.props;
        return (
            <View style={[styles.container]}>
                {!(user.data && user.data.id) ?
                    <PostTabsWithoutFeed screenProps={{
                        navigation: this.props.navigation,
                        handleShowHeader: this.handleShowHeader,
                        onScroll: Animated.event([{
                            nativeEvent: {contentOffset: {y: this.state._animatedValue}},
                            useNativeDriver: true
                        }])
                    }}/> :
                    <PostTabsWithFeed screenProps={{
                        navigation: this.props.navigation,
                        handleShowHeader: this.handleShowHeader,
                        onScroll: Animated.event([{
                            nativeEvent: {contentOffset: {y: this.state._animatedValue}},
                            useNativeDriver: true
                        }])
                    }}/>}

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Header.HEIGHT,
        /*  alignItems: 'center',
          justifyContent: 'center',*/
    },
});

function mapStateToProps({post, user}) {
    return {
        user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerMedia: (id) => dispatch(mediaActions.registerMedia(id)),
        setNavigation: (id, navigation) => dispatch(navigationActions.setNavigation(id, navigation)),
        playControlsMedia: (id, isViewable) => dispatch(mediaActions.playControlsMedia(id, isViewable)),
        getPosts: (sort) => dispatch(postActions.getPosts(sort))

    }
}


export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Home));