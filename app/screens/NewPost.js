import React from 'react';
import {
    StyleSheet,
    View,
    TextInput,
    TouchableOpacity,
    Text,
    Alert,
    Modal,
    FlatList,
    ActivityIndicator,
    TouchableHighlight,
    Image,
    Platform, AppState,
} from 'react-native';
import { MaterialCommunityIcons, MaterialIcons, Entypo } from '@expo/vector-icons';
import { ImagePicker, DocumentPicker, Video, FileSystem, Audio, Constants } from 'expo';

import { theme } from '../utils/styles';

import PostImage from '../components/PostImage';
import ProgressDialog from '../components/ProgressDialog';

import Player from '../components/Player';


import * as dimens from '../utils/dimens';
import * as colors from '../utils/colors';
import { getMediaHeight } from "../utils/dimens";
import { api } from "../utils/api";
import { validate } from '../utils/validators';
import { connect } from 'react-redux';


class NewPost extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            showLoading: false,
            title: "",
            tags: [],
            media: null,
            mediaType: null,
            mediaWidth: null,
            mediaHeight: null,
            duration: null
        }
    }

    static navigationOptions = ({ navigation }) => {

        const request = navigation.state.params ? navigation.state.params.actions.request : null;
        const showLoading = navigation.state.params ? navigation.state.params.showLoading : null;

        return {
            headerRight:
                <TouchableOpacity
                    onPress={request}
                    style={{ marginEnd: 15 }}>
                    {showLoading ?
                        <ActivityIndicator color={colors.colorLoader} /> :
                        <Text style={{ color: colors.colorLink, fontWeight: 'bold' }}>SAVE</Text>
                    }
                </TouchableOpacity>
        }
    };

    async componentDidMount() {

        const params = {
            actions: {
                request: this.createPost,
                showLoading: false,
            }
        };

        this.props.navigation.setParams(params);


        this.checkPermissions();

        // const soundObject = new Expo.Audio.Sound();
        // // soundObject.setOnPlaybackStatusUpdate(onPlaybackStatusUpdate);
        // await soundObject.loadAsync({uri: "https://s3.amazonaws.com/exp-us-standard/audio/playlist-example/Comfort_Fit_-_03_-_Sorry.mp3"},
        //     {shouldPlay: true,rate: 1.0, isMuted: false},
        //     false).then(()=> {
        //     alert("loaded")
        // }).catch(err=> {
        //     alert(JSON.stringify(err))
        // });
    }


    checkPermissions = async () => {
        const { Permissions } = Expo;
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('In order to upload photos you should enable camera roll permissions');
        }

        // await Permissions.askAsync(Permissions.CAMERA);
    }

    _openCamera = async () => {

        this.refs.progressDialog.show("Processing file");


        let result = await ImagePicker.launchCameraAsync({
            base64: true,
            exif: true,
        });


        // const base64Image = `data:image/jpeg;base64,${result.base64}`;
     
            this.refs.progressDialog.dismiss();

            this.setState({
                media: result.uri,
                mediaType: 1,
                mediaWidth: result.width,
                mediaHeight: result.height,
            });
       
    }

    _pickDocument = () => {
      
        if(Platform.OS === "android") {
            this.refs.progressDialog.show("Processing file");
        }

        DocumentPicker.getDocumentAsync({
            type: 'audio/*'
        }).then(result => {
            if(!result.cancelled) {
                const audio = new Expo.Audio.Sound();
                audio.loadAsync({ uri: result.uri },
                    { shouldPlay: false, rate: 1.0, isMuted: true }, false)
                    .then((data) => {
                        this.refs.progressDialog.dismiss().then(_ => {
                            if (data.durationMillis >= 180000) {
                                Alert.alert('Error', 'Audio must be 3 min lenght or less', [{ text: 'Ok' }]);
                            } else {
                                this.setState({
                                    media: result.uri,
                                    mediaType: 4,
                                    duration: result.durationMillis
                                });
                            }
        
                        });
    
                    }).catch(err => {
                        this.refs.progressDialog.dismiss().then(_ => {
                            Alert.alert('Error', 'There was a problem with this audio', [{ text: 'Ok' }]);
                        });
                    })
            } else {
                this.refs.progressDialog.dismiss();
            }
        });


    };

    _pickVideo = () => {

        this.refs.progressDialog.show("Processing file");

        ImagePicker.launchImageLibraryAsync({
            mediaTypes: "Videos",
            exif: true,
        }).then(result => {
            this.refs.progressDialog.dismiss().then(_ => {
                if (!result.cancelled) {
                    if (result.duration >= 600000) {
                        Alert.alert("", "The video cannot exceed 10 minutes", [{ text: "Ok" }])
                        return;
                    }

                    this.props.navigation.navigate("ProcessMedia", {
                        media: result,
                        mediaType: 3,
                        onMediaTrimmed: this.setTrimmedMedia
                    })
                }
            });

        }).catch(_ => {
            this.refs.progressDialog.dismiss();
        })


    };

    _pickImage = async () => {

        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: "Images",
            base64: true,
            exif: true,
        });

        this.setState({
            media: result.uri,
            mediaType: 1,
            mediaWidth: result.width,
            mediaHeight: result.height,
        });

    };

    setTrimmedMedia = (source, media) => {
        this.setState({
            media: source,
            mediaType: 3,
            mediaWidth: media.width,
            mediaHeight: media.height
        })
    };


    renderImage = ({ item, index }) => {
        return (
            <PostImage key={index} />
        );
    };

    promptDiscard = () => {
        Alert.alert(
            '',
            'Discard this media?',
            [
                { text: 'Cancel', onPress: () => console.log("cancel") },
                {
                    text: 'Discard', onPress: () => {
                        this.setState({
                            media: null,
                            mediaType: null,
                            mediaWidth: null,
                            mediaHeight: null,
                            duration: null
                        })
                    },
                },
            ],
            { cancelable: false }
        )

    };

    createPost = () => {
        const { media, mediaWidth, mediaHeight, title, tags } = this.state;
        const { user } = this.props;
        const fieldsValid = validate([
            { name: 'title', text: title, type: 'text', minLength: 2 },
        ]);

        let tagsSeparatedByComma = null;
        if (tags) {
            tagsSeparatedByComma = tags.map(tag => tag.id).join(",");
        }

        if (media && fieldsValid) {
            this.refs.progressDialog.show("Submitting...");
            api.post.create(user.data.id, media, title, tagsSeparatedByComma).then(response => {
                this.refs.progressDialog.dismiss().then(_ => {
                    if (!response.error) {
                        this.props.navigation.goBack();
                    }
                });

            }).catch(error => {
                this.refs.progressDialog.dismiss().then(_ => {
                    alert(JSON.stringify(error))
                })
            })
        }

    };


    render() {
        const { media, mediaType, mediaHeight, mediaWidth, duration, title, tags } = this.state;

        const resizedHeight = getMediaHeight(mediaWidth, mediaHeight);

        let tagsLabel = "Tags";

        if (tags.length > 0) {
            tagsLabel = "";
            tagsLabel = tags.map(tag => tag.name).join(",");
        }



        return (
            <View style={styles.container}>

                <View style={styles.section}>

                    <TextInput
                        value={title}
                        onChangeText={text => this.setState({ title: text })}
                        underlineColorAndroid='transparent'
                        style={[styles.input, theme.font]} placeholder="Post awesome title" />
                </View>

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("AddTag", { tags, callback: (tags) => { this.setState({ tags }) } })}
                    style={[styles.section, styles.buttonArrow]}>

                    <Text style={theme.font}>{tagsLabel}</Text>

                </TouchableOpacity>


                {!media && (
                    <Actions _pickDocument={this._pickDocument} _openCamera={this._openCamera}
                        _pickImage={this._pickImage} _pickVideo={this._pickVideo} />
                )}

                {media && (
                    <View style={{ marginTop: 20, }}>

                        {mediaType <= 2 && (
                            <Image style={{ height: resizedHeight }} source={{ uri: media }} />
                        )}



                        {(mediaType === 3) && (
                            <Player ref="player" source={media} duration={duration} />
                        )}

                        {(mediaType === 4) && (
                            <Player ref="player" source={media} duration={duration} />
                        )}

                        <TouchableOpacity
                            onPress={this.promptDiscard}
                            style={{
                                position: 'absolute',
                                top: -15,
                                // left: 0,
                                right: 10,
                                width: 30,
                                height: 30,
                                borderRadius: 30,
                                backgroundColor: 'white',
                                // backgroundColor: 'rgba(52, 52, 52, 0.8)',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                            <MaterialCommunityIcons name="close-circle" color={"black"} size={30} />
                        </TouchableOpacity>

                    </View>
                )}


                <ProgressDialog ref="progressDialog" />

            </View>
        );
    }
}

const Actions = (props) => {
    const { _pickImage, _pickDocument, _openCamera, _pickVideo } = props;
    return (
        <View style={{ marginTop: 15, }}>
            <View style={styles.actions}>
                <TouchableOpacity
                    onPress={_openCamera}
                    style={styles.center}>
                    <View style={styles.circularAction}>
                        <Entypo name="camera" size={50}
                            color={colors.colorActions} />
                    </View>
                    <Text style={[theme.font, styles.label]}>Camera</Text>
                </TouchableOpacity>


                <TouchableOpacity
                    onPress={_pickImage}
                    style={styles.center}>
                    <View style={styles.circularAction}>
                        <MaterialCommunityIcons name="folder-image" size={50} color={colors.colorActions} />
                    </View>
                    <Text style={[theme.font, styles.label]}>Image</Text>
                </TouchableOpacity>


            </View>

            <View style={styles.actions}>
                <TouchableOpacity
                    onPress={_pickDocument}
                    style={styles.center}>
                    <View style={styles.circularAction}>
                        <MaterialIcons name="audiotrack" size={50}
                            color={colors.colorActions} />
                    </View>
                    <Text style={[theme.font, styles.label]}>Audio</Text>
                </TouchableOpacity>


                <TouchableOpacity
                    onPress={_pickVideo}
                    style={styles.center}>
                    <View style={styles.circularAction}>
                        <MaterialCommunityIcons name="video" size={50} color={colors.colorActions} />
                    </View>
                    <Text style={[theme.font, styles.label]}>Video</Text>
                </TouchableOpacity>


            </View>
        </View>

    );
};

const ImageGallery = (props) => {
    const { context } = props;
    return (
        <View style={styles.gallery}>

            <FlatList
                style={{ flex: 1 }}
                horizontal={true}
                data={[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]}
                renderItem={context.renderImage}
            />


        </View>
    );
};

const styles = StyleSheet.create({


    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
    },

    label: {
        fontSize: 16,
        // fontWeight: 'bold'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonArrow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    font: {
        color: '#37393D',
        fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
    },


    actions: {
        padding: 40,
        paddingTop: 0,
        paddingBottom: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },

    circularAction: {
        width: 80,
        height: 80,
        borderRadius: dimens.getRadius(80),
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'red'

    },
    actionLink: {
        color: colors.colorLink
    },
    section: {
        borderBottomWidth: .5,
        borderColor: '#CBCBCB',
        padding: 15,
        alignItems: 'center',
        flexDirection: 'row',

    },

    categories: {
        padding: 5,
        // borderBottomWidth: .5,
        borderColor: '#CBCBCB',

    },
    icon: {
        marginEnd: 15,
    },

    input: {
        fontSize: 18,
        width: '100%',
    },

    gallery: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 5,

    },
});

function mapStateToProps({ user }, ownProps) {
    return {
        user
    }
}

export default connect(mapStateToProps)(NewPost);