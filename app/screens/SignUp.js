import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    FlatList,
    Dimensions,
    Image,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    AsyncStorage,
    KeyboardAvoidingView
} from 'react-native';
import * as colors from '../utils/colors';
import {theme} from "../utils/styles";
import SnackBar, {DURATION} from '../components/SnackBar';
import * as dimens from "../utils/dimens";
import {validate} from "../utils/validators";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as userActions from "../actions/user";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";

class SignUp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'Edward',
            username: 'zuko',
            email: 'lopezredd@gmail.com',
            password: '123456',
            deviceToken: null,
            showLoading: false
        };

        this.focusNextField = this.focusNextField.bind(this);
        this.scrollOffsetY = 30;
        this.shouldEnableLoginBtn = true;
    }

    componentDidMount() {
        AsyncStorage.getItem("deviceToken").then(deviceToken => {
            this.setState({deviceToken});
        })
    }

    UNSAFE_componentWillUpdate(nextProps, nextState) {

        const {name, username, email, password} = nextState;

        // this.shouldEnableLoginBtn = validate([
        //     {name: 'name', text: name, type: 'text'},
        //     {name: 'username', text: username, type: 'text', minLength: 6},
        //     {name: 'email', text: email, type: 'email', minLength: 6},
        //     {name: 'password', text: password, type: 'text', minLength: 6},
        // ]);
    }

    focusNextField(key, multiplier) {
        // const substract = multiplier > 1 ? 0 : 2;
        // const androidScroll =((25) * multiplier) +
        //     ((multiplier - 1) * (30 + multiplier) + (multiplier - substract));
        // const scrollToNextField = Platform.OS === 'android' ? androidScroll  : this.scrollOffsetY;
        // const scrollObject = {x: 0, y: scrollToNextField, animated: true};
        //
        const nextField = this.refs[key];
        nextField.focus();


        // if(Platform.OS === )
        // this.refs.scrollView.scrollTo(scrollObject);

        // alert(this.scrollOffsetY);
        // this.refs.keyboardAvoidingView.scrollToFocusedInput(nextField);
        // nextField.measure((x,y,q,fieldHeight, e,r) => {
        //     let {width, height} = Dimensions.get('window');
        //     const scrollObject = {x: 0, y:  (r - this.scrollOffsetY - fieldHeight - y - 200), animated: true};
        //     this.refs.scrollView.scrollTo(scrollObject);
        // })



    }

    attemptRegister = () => {

        const {name, username, email, password, deviceToken, showLoading} = this.state;

        if (this.shouldEnableLoginBtn && !showLoading) {
            this.setState({showLoading: true});
            this.props.register(name, username, email, password, deviceToken).then(response => {
                this.setState({showLoading: false});
                if (response.error) {
                    this.refs.snackBar.show(this.getError(response.error), DURATION.LENGTH_LONG)
                } else {
                    // alert(JSON.stringify(response.payload.data.user));
                    AsyncStorage.setItem("userId", String(response.payload.data.user.id));
                    this.props.navigation.dispatch(NavigationActions.reset({
                        index: 0,
                        key: null,
                        actions: [NavigationActions.navigate({ routeName: 'Tabs' })]
                    }))
                    // this.props.navigation.navigate("Home");
                }
            }).catch(error => {
                // alert(JSON.stringify(error))
            });
        }



    };

    getError = (response) => {
        const {message} = response;
        return message.username || message.email || message.name || message.password || message ;
    };

    render() {


        const {name, username, email, password, showLoading} = this.state;
        return (
            <KeyboardAvoidingView
                ref="keyboardAvoidingView"
                behavior='padding'
                style={theme.container}
                scrollEventThrottle={16}
                keyboardVerticalOffset={dimens.loginKeyboardVerticalOffset}>

                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    scrollEventThrottle={16}
                    onScroll={(event) => {
                        this.scrollOffsetY = event.nativeEvent.contentOffset.y
                    }}
                    ref="scrollView"
                    style={styles.body}>
                    <View style={{flex: 1}}>
                        <Image style={theme.logo}
                               source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo_TV_2015.png'}}/>

                        <Text style={[theme.font, styles.title]}>Create Account</Text>

                        <View style={styles.form}>
                            <TextInput
                                ref="name"
                                value={name}
                                onChangeText={(text) => this.setState({name: text})}
                                autoCorrect={false}
                                blurOnSubmit={false}
                                returnKeyType={"next"}
                                onSubmitEditing={() => this.focusNextField('username', 1)}
                                underlineColorAndroid='transparent' style={[theme.font, theme.inputUnderLined]}
                                placeholder="Full Name"/>
                            <TextInput
                                ref="username"
                                value={username}
                                onChangeText={(text) => this.setState({username: text})}
                                autoCapitalize="none"
                                autoCorrect={false}
                                blurOnSubmit={false}
                                returnKeyType={"next"}
                                onSubmitEditing={() => this.focusNextField('email', 2)}
                                underlineColorAndroid='transparent' style={[theme.font, theme.inputUnderLined]}
                                placeholder="Username"/>
                            <TextInput
                                ref="email"
                                value={email}
                                onChangeText={(text) => this.setState({email: text})}
                                autoCapitalize="none"
                                autoCorrect={false}
                                blurOnSubmit={false}
                                returnKeyType={"next"}
                                onSubmitEditing={() => this.focusNextField('password', 3)}
                                underlineColorAndroid='transparent' style={[theme.font, theme.inputUnderLined]}
                                placeholder="Email"/>
                            <TextInput
                                ref="password"
                                value={password}
                                onChangeText={(text) => this.setState({password: text})}
                                autoCapitalize="none"
                                secureTextEntry={true}
                                autoCorrect={false}
                                blurOnSubmit={true}
                                returnKeyType={"done"}
                                onSubmitEditing={()=> this.attemptRegister()}
                                underlineColorAndroid='transparent' style={[theme.font, theme.inputUnderLined]}
                                placeholder="Password"/>
                        </View>

                        <View>

                            <Text style={styles.terms}>By signing up you agree with our Terms and that you have read our
                                privacy
                                policy.</Text>
                        </View>
                    </View>


                </ScrollView>


                <View style={theme.footer}>
                    <TouchableOpacity
                        onPress={this.attemptRegister}
                        disabled={!this.shouldEnableLoginBtn || showLoading}
                        style={[theme.buttonLoadingRight, {backgroundColor: this.shouldEnableLoginBtn && !showLoading ? colors.colorPrimary : colors.colorDisable}]}>
                        <Text style={[theme.font, styles.buttonText]}>Sign Up</Text>
                        {showLoading && (
                            <ActivityIndicator style={styles.indicator} size="small" color={colors.colorSecondary}/>)}
                    </TouchableOpacity>
                </View>

                <SnackBar ref="snackBar"/>
            </KeyboardAvoidingView>
        )

    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        padding: 15
    },
    indicator: {
        position: 'absolute',
        right: 15,
        alignItems: 'center',
    },


    title: {
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 10,
    },


    terms: {
        marginTop: 15,
        color: 'grey',
        // textDecorationLine: 'underline'
    },
    button: {
        padding: 15,
        backgroundColor: colors.colorPrimary,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'

    },
    buttonText: {
        fontSize: 18,
        color: colors.white,
        fontWeight: 'bold'
    },

    form: {
        flex: 1,
    }
});

function mapDispatchToProps(dispatch) {
    return {
        register: (name, username, email, password, token) => dispatch(userActions.register(name, username, email, password, token)),
    }
}

export default connect(null, mapDispatchToProps)(SignUp);


