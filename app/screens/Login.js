import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    ActivityIndicator,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    Platform,
    KeyboardAvoidingView,
    Alert,
    AsyncStorage
} from 'react-native';
import * as colors from '../utils/colors';
import {api} from '../utils/api';


import {validate} from '../utils/validators';
import {theme} from "../utils/styles";
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import SnackBar, {DURATION} from '../components/SnackBar';
import * as dimens from "../utils/dimens";
import * as userActions from '../actions/user';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
import Expo from 'expo';


class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            textInputEmail: 'djviera92@gmail.com',
            textInputPassword: '123456',
            deviceToken: null,
            showLoading: false,
            snackBarVisible: true,

            isPasswordHidden: true,
        };

        this.focusNextField = this.focusNextField.bind(this);
        this.inputs = {};
        this.scrollOffsetY = 30;
        this.shouldEnableLoginBtn = false;
    }

    focusNextField(key) {
        this.inputs[key].focus();
    }

    UNSAFE_componentWillUpdate(nextProps, nextState) {

        const {textInputEmail, textInputPassword, shouldEnableLoginBtn, showLoading} = nextState;

        this.shouldEnableLoginBtn = validate([
            {name: 'email', text: textInputEmail, type: 'email'},
            {name: 'password', text: textInputPassword, type: 'text', minLength: 6},
        ]);
    }

    generateUserNameWith = (name) => {

        const parts = name.split(" ");
        const firstName = parts[0];
        return `${firstName[0]} ${parts[1]}`

    };

    signInWithGoogleAsync = () => {

        Expo.Google.logInAsync({
            androidClientId: "AIzaSyAO0x-Vq4ubT4h70QX17RUUbnCoucRkTYk",
            iosClientId: "101445533680-vpikbmoj3guinjq49ukqg6kmru2o4r8d.apps.googleusercontent.com",
            iosStandaloneAppClientId: "101445533680-vpikbmoj3guinjq49ukqg6kmru2o4r8d.apps.googleusercontent.com",
            scopes: ['profile', 'email'],
        }).then(result => {

            if (result.type === 'success') {
                const user = result.user;

                const obj = {
                    provider: 'GOOGLE',
                    provider_id: user.id,
                    provider_token: result.accessToken,
                    name: user.name,
                    email: user.email,
                };

                this.attemptLoginWithSocialMedia(obj);
            }

        }).catch(e => {
            alert(JSON.stringify(e))
        });

    };

    signInWithFacebookAsync = () => {
        Expo.Facebook.logInWithReadPermissionsAsync('231849547534700', {
            permissions: ['public_profile', 'email'],
        }).then(result => {
            const {type, token} = result;
            if (type === 'success') {
                // Get the user's name using Facebook's Graph API
                fetch(`https://graph.facebook.com/me?access_token=${token}`)
                    .then(res => res.json())
                    .then(response => {


                        const user = response;

                        const obj = {
                            name: user.name,
                            email: user.email,
                            provider: 'FACEBOOK',
                            provider_id: user.id,
                            provider_token: token,
                            profile_picture: null,
                        };
                        // alert(JSON.stringify(obj));

                        this.attemptLoginWithSocialMedia(obj);
                    });


            }

        })
    }

    attemptLoginWithSocialMedia = (params) => {
        api.auth.loginWithSocialMedia(params)
            .then(response => {
                // alert(JSON.stringify(response))
                if (!response.error) {
                    this.props.receiveUserData(response, false);

                    AsyncStorage.setItem("userId", String(response.data.user.id)).then(_=> {
                        this.props.navigation.dispatch(NavigationActions.reset({
                            index: 0,
                            key: null,
                            actions: [NavigationActions.navigate({routeName: 'Tabs'})]
                        }))
                    });

                }
            }).catch(error => {
            alert(JSON.stringify(error));
        });

    };

    attemptLogin = () => {
        const {showLoading, textInputPassword, textInputEmail, deviceToken} = this.state;
        if (this.shouldEnableLoginBtn && !showLoading) {
            this.setState({showLoading: true});
            this.props.authenticate(textInputEmail, textInputPassword, deviceToken).then(response => {
                // alert(JSON.stringify(response))
                this.setState({showLoading: false});
                if (response.error) {
                    this.refs.snackBar.show(response.error.message, DURATION.LENGTH_LONG)
                } else {
                    AsyncStorage.setItem("userId", String(response.payload.data.user.id)).then(_=> {
                        this.props.navigation.dispatch(NavigationActions.reset({
                            index: 0,
                            key: null,
                            actions: [NavigationActions.navigate({routeName: 'Tabs'})]
                        }))
                    });

                    // this.props.navigation.navigate("Home");
                }
            }).catch(error => {
                // alert(JSON.stringify(error))
            });
        }

    };

    componentDidMount() {
        AsyncStorage.getItem("deviceToken").then(deviceToken => {
            this.setState({deviceToken});
        })
    }


    render() {

        const scrollToNextField = Platform.OS === 'android' ? 25 : this.scrollOffsetY;

        const {textInputEmail, textInputPassword, isPasswordHidden, showLoading} = this.state;
        return (
            <KeyboardAvoidingView
                behavior='padding'
                style={theme.container}
                keyboardVerticalOffset={dimens.loginKeyboardVerticalOffset}>

                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    onScroll={(event) => {
                        this.scrollOffsetY = event.nativeEvent.contentOffset.y
                    }}
                    ref={ref => {
                        this.scrollView = ref
                    }}
                    style={styles.body}>
                    <Image style={theme.logo}
                           source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo_TV_2015.png'}}/>

                    <Text style={[theme.font, styles.title]}>Log in</Text>

                    <View>
                        <TextInput
                            ref={input => {
                                this.inputs['email'] = input
                            }}
                            clearTextOnFocus={false}
                            blurOnSubmit={false}
                            onSubmitEditing={() => {
                                this.scrollView.scrollTo({
                                    x: 0,
                                    y: scrollToNextField,
                                    animated: true
                                });
                                this.focusNextField('password');
                            }}
                            value={textInputEmail}
                            keyboardType="email-address"
                            onChangeText={(text) => this.setState({textInputEmail: text})}
                            autoCapitalize="none"
                            returnKeyType={"next"}
                            autoCorrect={false}
                            underlineColorAndroid='transparent'
                            style={[theme.font, theme.inputUnderLined]}
                            placeholder="Email"/>
                    </View>


                    <View>
                        <TextInput
                            ref={input => {
                                this.inputs['password'] = input;
                            }}
                            value={textInputPassword}
                            onChangeText={(text) => this.setState({textInputPassword: text})}
                            secureTextEntry={isPasswordHidden}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            blurOnSubmit={false}
                            onSubmitEditing={() => {
                                this.attemptLogin()
                            }}
                            autoCorrect={false}
                            underlineColorAndroid='transparent'
                            style={[theme.font, theme.inputUnderLined]}
                            placeholder="Password"/>

                        <TouchableWithoutFeedback
                            onPress={() => this.setState((state) => ({
                                isPasswordHidden: !state.isPasswordHidden
                            }))}>
                            <MaterialCommunityIcons style={{position: 'absolute', right: 15, bottom: 20,}}
                                                    name="eye" size={25}
                                                    color={isPasswordHidden ? colors.colorActions : colors.colorLink}/>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={styles.rowSpaced}>

                        <TouchableOpacity style={styles.forgotPasswordButton}>
                            <Text style={[theme.font, styles.link]}>SIGN UP</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.forgotPasswordButton}>
                            <Text style={[theme.font, styles.link]}>FORGOT PASSWORD</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.orTagContainer}>
                        <Text style={[theme.font, styles.orTag]}>OR</Text>
                    </View>


                    <View style={styles.socialContainerRow}>
                        <TouchableOpacity
                            onPress={this.signInWithFacebookAsync}
                            style={[styles.socialBtnRow, {marginLeft: 0, backgroundColor: colors.colorFacebook}]}>
                            <View style={[theme.center, {flexDirection: 'row'}]}>
                                <MaterialCommunityIcons name="facebook" size={20} color={colors.white}/>
                                <Text style={[styles.socialBtnTextRow]}>{"Login with\nfacebook"}</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={this.signInWithGoogleAsync}
                            style={[styles.socialBtnRow, {marginRight: 0, backgroundColor: colors.colorGoogle}]}>
                            <View style={[theme.center, {flexDirection: 'row'}]}>
                                <MaterialCommunityIcons name="google" size={20} color={colors.white}/>
                                <Text style={[styles.socialBtnTextRow]}>{"Login with\ngoogle"}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>


                <View style={theme.footer}>
                    <TouchableOpacity
                        onPress={this.attemptLogin}
                        disabled={!this.shouldEnableLoginBtn || showLoading}
                        style={[theme.buttonLoadingRight, {backgroundColor: this.shouldEnableLoginBtn && !showLoading ? colors.colorPrimary : colors.colorDisable}]}>
                        <Text style={[theme.font, styles.buttonText]}>LOG IN</Text>
                        {showLoading && (
                            <ActivityIndicator style={styles.indicator} size="small" color={colors.colorSecondary}/>)}
                    </TouchableOpacity>
                </View>

                <SnackBar ref="snackBar"/>

            </KeyboardAvoidingView>
        );
    }

}

const styles = StyleSheet.create({

    socialContainerRow: {
        flex: 1,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    socialBtnRow: {
        borderRadius: 4,
        flex: 1,
        padding: Platform.OS === 'android' ? 8 : 8,
        margin: 5,
        justifyContent: 'center',
        // elevation: 3,
    },

    socialBtnTextRow: {
        fontSize: 12,
        color: colors.white,
        marginLeft: 15,
        fontWeight: 'bold'
    },


    snackBar: {
        padding: 15,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: colors.colorSnackBar
    },
    snackBarText: {
        fontSize: 16,
        color: colors.white,
        fontWeight: 'bold'
    },

    body: {
        flex: 1,
        padding: 15
    },

    rowSpaced: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },


    orTagContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 15,
        // paddingLeft: 30,
        // paddingRight: 30,
    },

    divider: {
        flex: 1,
        height: 1,
        borderColor: '#E3E3E5',
        borderBottomWidth: .5,
    },

    forgotPasswordButton: {
        marginTop: 10,

    },

    floatRight: {
        textAlign: 'right',
    },
    textCenter: {
        textAlign: 'center'
    },

    orTag: {
        textAlign: 'center',
        color: colors.colorActions,
        paddingRight: 10,
        paddingLeft: 10,
    },

    signUpButton: {},

    link: {
        color: colors.colorLink,
        fontSize: 14,
        fontWeight: 'bold'

    },

    forgotPasswordText: {},

    title: {
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 5,
    },

    signUpContainer: {
        // marginTop: 25,
    },
    buttonLoading: {
        padding: 13,
        flexDirection: 'row',
        backgroundColor: colors.colorPrimary,
        // borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'

    },

    indicator: {
        position: 'absolute',
        right: 15,
        alignItems: 'center',
    },
    buttonText: {
        fontSize: 16,
        color: colors.white,
        fontWeight: 'bold'
    },


    form: {}
});

function mapDispatchToProps(dispatch) {
    return {
        authenticate: (email, password, deviceToken) => dispatch(userActions.authenticateUser(email, password, deviceToken)),
        receiveUserData: (response, error) => dispatch(userActions.receiveUserData(response, error))
    }
}

export default connect(null, mapDispatchToProps)(Login);

