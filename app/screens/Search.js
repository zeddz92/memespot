import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    ActivityIndicator,
    FlatList,
    Image,
    TouchableOpacity,
    Platform,
    TextInput,
    Dimensions
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import Tag from '../components/Tag';
import PostImage from '../components/PostImage';
import {MaterialCommunityIcons, MaterialIcons, FontAwesome, Ionicons} from '@expo/vector-icons';
import * as colors from '../utils/colors';
import PostList from './PostList';
import {Constants} from 'expo';
import Post from '../components/Post';
import {api} from '../utils/api';


class Search extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            tags: [],
            textInputSearch: '',
            searchText: '',
            showLoading: true,
            showSearchResults: false
        };
    }


    componentDidMount() {
        this.makeTagsRequest();
    }

    _handleOnSearchBarClear = () => {

    };

    _handleOnSearchBarChangeText = (text) => {

    };

    makeTagsRequest = () => {
        const {page} = this.state;

        api.tag.getAll(1, 10).then(response => {
            this.setState((state) => ({
                tags: response.error ? [] : page === 1 ? response.data : [...state.tags, ...response.data],
                currentPage: response.current_page,
                lastPage: response.last_page,
                showLoading: false,
                error: response.code !== 200 ? {message: response.error, code: response.code} : null
            }));
        }).catch(error => {
            this.setState((state) => ({
                error,
                showLoading: false,
                currentPage: 0,
                lastPage: 0,
            }));
        });
    };


    renderTag = ({item, index}) => {
        return (
            <Tag selectTag={this.onTagSelected} data={item} key={index}/>
        );
    };

    onTagSelected = (tag) => {
        this.setState((state) => ({showSearchResults: true, searchText: tag.name, textInputSearch: tag.name}))
    };

    renderImage = ({item, index}) => {
        return (
            <PostImage key={index}/>
        );
    };

    handleClear = () => {
        this.setState({
            textInputSearch: '',
            searchText: '',
            showSearchResults: false
        })
    };


    render() {
        const {textInputSearch, showSearchResults, searchText, tags, showLoading} = this.state;
        return (
            <View style={[styles.container, {marginTop: Constants.statusBarHeight}]}>
                <View style={styles.searchBar}>
                    {!showSearchResults ?
                        <FontAwesome style={styles.icon} name="search" size={20} color={colors.colorActions}/> :
                        <TouchableOpacity
                        onPress={this.handleClear}>
                            {Platform.OS === 'android' ? (
                                <MaterialIcons style={styles.icon} color={colors.colorActions} name="arrow-back" size={25}/>
                            ) : (
                                <Ionicons style={styles.icon} color={colors.colorActions} name="ios-arrow-back" size={30}/>)
                            }
                        </TouchableOpacity>}
                    <TextInput
                        value={textInputSearch}
                        onChangeText={(text) => this.setState({textInputSearch: text})}
                        underlineColorAndroid='transparent'
                        style={styles.input} placeholder="Search..."
                        returnKeyType={"search"}
                        onSubmitEditing={() => {
                            this.setState((state) => ({showSearchResults: true, searchText: state.textInputSearch}))
                            // this.setState({showSearchResults: true, searchText: textInputSearch})
                            if (this.refs.postList) {
                                this.setState((state) => ({searchText: state.textInputSearch}))
                            }

                        }}
                    />
                </View>


                {showSearchResults ?
                    <PostList ref="postList" screenProps={{userId: 1}} category="FRESH" search={searchText} {...this.props}/> :
                    <View style={{paddingLeft: 5, paddingRight: 5}}>
                        {showLoading ?
                            <ActivityIndicator style={styles.loader} size="large" color="#0000ff"/> :
                            <FlatList
                                data={tags}
                                renderItem={this.renderTag}
                                keyExtractor={(item, index) => index}
                            />}
                    </View>


                }


                {/*  <View style={{flex: 1, paddingEnd:1}}>
                    <FlatList
                        style={{flex: 1}}
                        numColumns={4}
                        data={[0, 1, 2,3,4,5,6,7,8,9]}
                        renderItem={this.renderImage}
                        keyExtractor={(item, index) => index}
                    />

                </View>*/}


            </View>
        )
    }

}

const styles = StyleSheet.create({
    searchBar: {
        // elevation: 3,
        borderBottomWidth: .5,
        borderColor: '#CBCBCB',
        padding: 15,
        alignItems: 'center',
        flexDirection: 'row',



    },

    loader: {
        marginTop: 15,
    },

    categories: {
        padding: 5,
        // borderBottomWidth: .5,
        borderColor: '#CBCBCB',

    },
    icon: {
        marginEnd: 15,
    },

    input: {
        fontSize: 18,
        width: '100%',
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        /*  alignItems: 'center',
          justifyContent: 'center',*/
    }
});


export default Search;