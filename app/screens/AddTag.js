import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    Alert,
    ActivityIndicator,
    Platform,
} from 'react-native';
import { InstantSearch } from 'react-instantsearch/native';
import InputSearch from '../components/InputSearch';
import Button from '../components/Button';

import { theme } from '../utils/styles';
import * as colors from '../utils/colors';

class AddTag extends React.Component {

    static navigationOptions = ({ navigation }) => {

        const request = navigation.state.params.actions ? navigation.state.params.actions.request : null;

        return {
            headerRight:
                <Button
                    onPress={request}
                    style={{ marginEnd: 15 }}>
                    <Text style={{ color: colors.colorLink, fontWeight: 'bold' }}>DONE</Text>

                </Button>
        }
    };

    componentDidMount() {
        const params = {
            actions: {
                request: this.saveTags,
            }
        };
        this.props.navigation.setParams(params);
    }

    saveTags = () => {
        const tags = [
            this.refs.tag1.state.selectedTag,
            this.refs.tag2.state.selectedTag,
            this.refs.tag3.state.selectedTag
        ];

        let result = [];

        const reduce = tags.reduce((accumulator, tag) => {
            if(tag) {
                if (!accumulator.hasOwnProperty(tag.id)) {
                    accumulator[tag.id] = tag;
                    result.push(tag);
                }
            }
            return accumulator;
        }, {});

       
        this.props.navigation.state.params.callback(result);
        this.props.navigation.goBack();


        // const sanitized = tags.filter((t, index) => t !== null && tags[index].name === t.name);
      
    }


    render() {

        const {tags} = this.props.navigation.state.params;
  
        return (
            <View style={styles.container}>
                <InputSearch tag={tags[0]} ref="tag1" placeholder="Tag 1" />
                <InputSearch tag={tags[1]} ref="tag2" placeholder="Tag 2" />
                <InputSearch tag={tags[2]} ref="tag3" placeholder="Tag 3" />


                <View style={theme.divider} />
                <Text style={{ padding: 15, paddingTop: 5 }}>Be sure to select a tag from the results in order to be recognized</Text>
            </View>
        )

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});


export default AddTag;