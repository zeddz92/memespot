import React from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    FlatList,
    Alert,
    ActivityIndicator,
    Platform,
} from 'react-native';
import { api } from "../utils/api";
import Comment from '../components/Comment';
import { theme } from "../utils/styles";


class CommentDetail extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            comment: { user: {} },
            refreshing: false,
            showLoading: true,
            error: null,
            page: 1,
            currentPage: 0,
            lastPage: 0,
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        const { commentId } = navigation.state.params;

        api.comment.get(commentId).then(response => {
            if (!response.error) {
                this.setState({ showLoading: false, comment: response.data })
            }
        }).catch(error => {

        });

    }


    render() {
        const { showLoading } = this.state;
        return (
            <ScrollView style={{ flex: 1 }}>

                {this.state.comment.id ?
                    <View style={{ flex: 1 }}>
                        <Comment
                            loadReplies={true}
                            removeClippedSubviews={true}
                            replies={this.state.replies}
                            viewMore={this.handleViewMorePressed}
                            doFeedBack={this.doFeedBack}
                            onReply={this._handleReplyBtnPressed} item={this.state.comment} />
                    </View>

                    : <ActivityIndicator size="large" color="#0000ff" />}

            </ScrollView>
        )
    }
}


export default CommentDetail;