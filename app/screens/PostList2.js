import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    FlatList,
    InteractionManager,
    VirtualizedList,
    ActivityIndicator
} from 'react-native';
import Post from '../components/Post';
import {posts} from '../utils/mocks/posts';
import SvgUri from 'react-native-svg-uri';
import * as colors from '../utils/colors';
import * as dimens from '../utils/dimens';
import * as mediaActions from '../actions/media';
import Home from './Home';
import {connect} from 'react-redux';
import * as postActions from "../actions/post";
import {api} from '../utils/api';


class PostList2 extends React.Component {


    // static navigationOptions = {
    //     header: null
    // }


    constructor(props) {
        super(props);


        this.state = {
            posts: [],
            refreshing: true,
            showLoading: true,
            page: 1,
            error: null,
        };

        this.feedRefs = {};
        this.offset = 0;
    }


    goToComments = (post) => {
        this.props.navigation.navigate("PostComments", {post});

    };

    renderPost = ({item, index}) => {
        return (
            <Post
                ref={(ref) => {
                    if (item.medias[0].type === 3) {
                        this.props.registerMedia(item.id);
                        this.feedRefs[`REF_FEED_${item.id}`] = ref
                    }
                }}
                rootNavigation={this.props.navigation}
                item={item}
                goToComments={this.goToComments}/>
        )
    };
    _handleRefresh = () => {
        alert("refresh");
        this.setState({
            page: 1,
            refreshing: true,
        }, ()=> {
            this.makePostsRequest();
        })
    };

    _handleLoadMore = () => {

        this.setState = ((state) => ({
            page: state.page++
        }))

    };

    makePostsRequest = () => {
        const {page} = this.state;

        api.post.getAll("", page).then(response => {
            this.setState((state) => ({
                posts: [...state.posts, ...response.data],
                refreshing: false,
                showLoading: false,
                error: response.code !== 200 ? {message: response.error, code: response.code} : null
            }));
        }).catch(error => {
            this.setState((state) => ({
                error,
                refreshing: false,
                showLoading: false,
            }));
        });
    }

    componentDidMount() {

        this.makePostsRequest();

        // this.props.setNavigation("root", this.props.navigation);
        //  this.props.getPosts("").then(response => {
        //      // alert(JSON.stringify(response.error))
        //  }).catch(err => alert(JSON.stringify(err)))

        // this.props.navigation.navigate("PostComments", {post: posts.items[0]})
        this.props.navigation.addListener('didBlur', () => console.log('didBlur'));
    }

    onViewableItemsChanged = ({viewableItems, changed}) => {

        changed.forEach(item => {
            const {isViewable, key} = item;
            // const ref = this.feedRefs[`REF_FEED_${key}`];
            // this.props.playControlsMedia(key, isViewable);
            /* if(ref)
             ref.toggleItemHidden(isViewable);
 */

        });
        /*viewableItems.forEach((item) => {
            if(isViewable) {
                console.log(key);
                const ref = this[`swiperRef_${key}`];
                if(ref) {
                    ref.togglePlayVideo;

                }
            }
        });*/
    }

    _onScroll = event => {
        const currentOffset = event.nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);

        let showHeader = 0;


        if (Math.abs(dif) < 3) {
            // showHeader = true;
            // console.log('unclear');
        } else if (dif < 0) {
            showHeader = true;
            // console.log('up', dif);
        } else {
            showHeader = false;
            // console.log('down', dif);
        }

        const {handleShowHeader} = this.props.screenProps;

        if (showHeader !== 0)
        // handleShowHeader(showHeader, dif, currentOffset, this.offset);

            this.offset = currentOffset;
    };

    render() {
        const {refreshing, posts, showLoading} = this.state;
        // const {post} = this.props;
        // alert(JSON.stringify(post));
        return (
            <View

                style={styles.container}>
                {showLoading ?
                    <ActivityIndicator style={styles.loader} size="large" color="#0000ff"/> :
                    <FlatList
                        data={posts}
                        renderItem={this.renderPost}
                        scrollToOffset={this._onScroll}
                        scrollEventThrottle={0}

                        keyExtractor={(item) => item.id}
                        onViewableItemsChanged={this.onViewableItemsChanged}
                        onScrollEndDrag={console.log('end')}
                        onScrollBeginDrag={console.log('start')}
                        refreshing={refreshing}
                        onRefresh={this._handleRefresh}
                        onEndReached={this._handleLoadMore}
                        onEndReachedThreshol={100}
                    />
                }


            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e8eaf6',
        /*  alignItems: 'center',
          justifyContent: 'center',*/
    },
    loader: {
        flex: 1,
        marginTop: 15
    },
});

function mapStateToProps({post}) {
    return {
        post
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerMedia: (id) => dispatch(mediaActions.registerMedia(id)),
        playControlsMedia: (id, isViewable) => dispatch(mediaActions.playControlsMedia(id, isViewable)),
        getPosts: (sort) => dispatch(postActions.getPosts(sort))

    }
}


export default PostList2;