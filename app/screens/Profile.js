import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Dimensions, Animated} from 'react-native';

import ProfileTabs from '../navigation/ProfileTabs';
import SvgUri from 'react-native-svg-uri';

import * as dimens from '../utils/dimens';
import {theme} from '../utils/styles';
import * as colors from '../utils/colors';
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import {connect} from "react-redux";
import ScrollableTabView from 'react-native-scrollable-tab-view';
import {getMediaHeight} from "../utils/dimens";
import * as Animatable from 'react-native-animatable';
import ParallaxScrollView from 'react-native-parallax-scroll-view';

// import ProfilePicture from '../components/ProfilePicture';


const ProfileImgCamera = (props) => {
    const {navigation, user} = props;

    return (
        <TouchableOpacity style={styles.profileImgContainer}>
            <Image style={styles.profileImg}
                   source={user.data.profile_picture_url ? {uri: user.data.profile_picture_url} : require('../assets/img/profile.png')}/>

            {/*<View style={styles.addPhotoButton}>
                    <SvgUri fill={"#FFF"} width={13} height={13}
                            source={require('../assets/icons/camera.svg')}/>
                </View>*/}
        </TouchableOpacity>
    );
};

const UserSummary = (props) => {
    const {handleShowFollowers, user} = props;
    return (
        <View style={styles.summary}>

            <View style={[styles.center, styles.col, {marginStart: 0}]}>
                <Text style={[theme.font, styles.number]}>{user.summary.posts}</Text>
                <Text style={[styles.label]}>Posts</Text>
            </View>

            <TouchableOpacity onPress={() => handleShowFollowers("FOLLOWERS")}>
                <View style={[styles.center, styles.col]}>
                    <Text style={[theme.font, styles.number]}>{user.summary.followers}</Text>
                    <Text style={[theme.font, styles.label]}>Followers</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => handleShowFollowers("FOLLOWING")}>
                <View style={[styles.center, styles.col]}>
                    <Text style={[theme.font, styles.number]}>{user.summary.following}</Text>
                    <Text style={[theme.font, styles.label]}>Following</Text>
                </View>
            </TouchableOpacity>


        </View>
    );
};


class Profile extends React.Component {

    state = {
        headerHeight: null,
        showHeader: true
    };

    // const {data} = props.navigation.state.params;

    // const data = props.navigation.state.params ? props.navigation.state.params : null;
    // const onCommentPressed = () => {
    //     this.props.navigation.navigate("PostComments");
    // };

    _handleShowFollowers = (criteria) => {
        const {user} = this.props;

        this.props.navigation.navigate("Followers", {criteria, userId: user.data.id});
    };

    handleShowHeader = (showHeader, dif, currentOffset, offset, event) => {
        const {headerHeight} = this.state;
        const newHeight = showHeader ? headerHeight + 2 : headerHeight - 2;

        this.setState((state) => ({
            headerHeight: newHeight < 0 || newHeight > 20 ? state.headerHeight : newHeight,
            showHeader
        }));
        if (this.state.showHeader !== showHeader) {
            this.setState((state) => ({
                headerHeight: newHeight < 0 || newHeight > 20 ? state.headerHeight : newHeight,
                showHeader
            }));
        }

        // console.log(dif)

    };

    headerLayout = (event) => {
        const height = event.nativeEvent.layout.height;

        this.setState({
            headerHeight: height
        })
    };

    render() {
        const {user} = this.props;
        const {showHeader} = this.state;

        return (
            <ParallaxScrollView
                backgroundColor="white"
                contentBackgroundColor="white"
                onChangeHeaderVisibility={(value)=>{console.log(value)}}
                parallaxHeaderHeight={this.state.headerHeight}
                // renderScrollComponent={() => <Animated.View />}
                // renderScrollComponent={() => <AnimatedCustomScrollView/>}
                renderForeground={() => (
                    <View style={{ flex: 1}}>
                        <View
                            onLayout={this.headerLayout}

                        >
                            <View style={[styles.header]}>
                                <ProfileImgCamera {...this.props} />
                                <View style={{flex: 1,}}>
                                    <View style={{
                                        flexDirection: 'row',

                                        justifyContent: 'space-between',
                                        marginBottom: 5
                                    }}>
                                        <View>
                                            <Text style={[theme.font, styles.name]}>{user.data.name}</Text>
                                            <Text style={[theme.font, styles.userName]}>@{user.data.username}</Text>
                                        </View>

                                        <View style={[styles.headerActions]}>
                                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Settings")}
                                                              style={styles.settingsIcon}>
                                                <MaterialIcons name="settings" size={24} color={colors.colorActions}/>
                                            </TouchableOpacity>

                                        </View>
                                    </View>

                                    <UserSummary {...this.props} handleShowFollowers={this._handleShowFollowers}/>


                                </View>
                            </View>
                            <Text style={[theme.font, styles.shortBio]}>{user.data.short_bio}</Text>
                        </View>
                    </View>
                )}>
                <View>
                    <ProfileTabs onNavigationStateChange={(prevState, currentState) => {

                    }} screenProps={{navigation: this.props.navigation, handleShowHeader: this.handleShowHeader}}/>
                </View>
            </ParallaxScrollView>
        )


        // return (
        //     <View style={[styles.container, {flex: 1}]}>
        //         <View
        //             scrollEnabled={true} contentContainerStyle={{flex: 1}}
        //             automaticallyAdjustContentInsets={true}
        //             style={[styles.container, {flex: 1}]}>
        //
        //             <View
        //                 onLayout={this.headerLayout}
        //                 style={{height: `${this.state.headerHeight}%`}}
        //                 >
        //                 <View style={[styles.header]}>
        //                     <ProfileImgCamera {...this.props} />
        //                     <View style={{flex: 1,}}>
        //                         <View style={{
        //                             flexDirection: 'row',
        //
        //                             justifyContent: 'space-between',
        //                             marginBottom: 5
        //                         }}>
        //                             <View>
        //                                 <Text style={[theme.font, styles.name]}>{user.data.name}</Text>
        //                                 <Text style={[theme.font, styles.userName]}>@{user.data.username}</Text>
        //                             </View>
        //
        //                             <View style={[styles.headerActions]}>
        //                                 <TouchableOpacity onPress={() => this.props.navigation.navigate("Settings")}
        //                                                   style={styles.settingsIcon}>
        //                                     <MaterialIcons name="settings" size={24} color={colors.colorActions}/>
        //                                 </TouchableOpacity>
        //
        //                             </View>
        //                         </View>
        //
        //                         <UserSummary {...this.props} handleShowFollowers={this._handleShowFollowers}/>
        //
        //
        //                     </View>
        //                 </View>
        //                 <Text style={[theme.font, styles.shortBio]}>{user.data.short_bio}</Text>
        //             </View>
        //
        //
        //
        //             <ProfileTabs onNavigationStateChange={(prevState, currentState) => {
        //
        //             }} screenProps={{navigation: this.props.navigation, handleShowHeader: this.handleShowHeader}}/>
        //         </View>
        //     </View>
        //
        // );
    }


}


const avatarSize = 70;
var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        /*  alignItems: 'center',
          justifyContent: 'center',*/
    },

    shortBio: {
        padding: 15,
        paddingTop: 0,
        // fontWeight: 'bold',
        color: '#373C3F'
    },
    font: {},

    profileTabs: {
        borderBottomWidth: 5,

    },

    settingsIcon: {
        marginRight: 15,
    },

    headerActions: {},
    number: {
        // color: "white",
        fontSize: 18,
        fontWeight: 'bold'
    },

    name: {
        fontSize: 16,
        fontWeight: 'bold',
        // color: "white"
    },

    backgroundImg: {
        bottom: 0,
        top: 0,
        left: 0,
        right: 0,
        position: 'absolute',
        resizeMode: 'cover'
    },

    userName: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.black,

    },

    label: {
        fontWeight: 'bold',
        // color: 'grey',
        // color: colors.colorSettingsText
        // color: "white"
    },

    col: {
        marginTop: 10,
        marginLeft: width <= 320 ? 10 : 15,
        marginRight: width <= 320 ? 10 : 15,
    },

    summary: {
        flexDirection: 'row',
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    backdrop: {
        bottom: 0,
        top: 0,
        left: 0,
        right: 0,
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, .4)'
    },
    header: {
        padding: 15,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'flex-start',

    },

    profileImg: {
        width: avatarSize,
        height: avatarSize,
        borderWidth: 1,
        resizeMode: 'cover',
        borderRadius: dimens.getRadius(avatarSize),
        marginRight: 15,
        borderColor: colors.lightBlue
    },
    profileImgContainer: {
        width: avatarSize,
        height: avatarSize,
        marginEnd: 15,
    },
    addPhotoButton: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        width: 25,
        height: 25,
        borderRadius: dimens.getRadius(30),
        backgroundColor: 'red',
        bottom: 5,
        right: 0
    }


});

function mapStateToProps({user}) {
    return {
        user
    }
}


export default connect(mapStateToProps)(Profile);