import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ActivityIndicator,
    Easing,
    Alert,

    Animated,
    Platform, Dimensions
} from 'react-native';
import {theme} from '../utils/styles';
import * as colors from '../utils/colors';
import * as dimens from '../utils/dimens';

import {getMediaHeight} from "../utils/dimens";
import {ImagePicker, DocumentPicker, Video, FileSystem} from 'expo';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {MaterialIcons, MaterialCommunityIcons, Ionicons} from '@expo/vector-icons';
import moment from 'moment';
import {ProcessingManager, VideoPlayer} from 'react-native-video-processing';

import ProgressDialog from '../components/ProgressDialog';


class ProcessMedia extends React.Component {

    constructor(props) {
        super(props);
        const {media} = props.navigation.state.params;
        this.state = {
            source: media.uri,
            mediaType: 3,
            isPlaying: true,
            playBackEnded: false,
            fadeAnim: new Animated.Value(1),

            seekStart: 0,
            seekEnd: media.duration,


            position: 0,
            duration: media.duration,
            seekBarPosition: 0,
            playControlVisible: false

        };

        this.fadeAnimDuration = 600;

        this.shouldHidePlayBackBtn = true;
    }

    static navigationOptions = ({navigation}) => {

        const action = navigation.state.params.headerRight ? navigation.state.params.headerRight.trimMedia : null;
        const showLoading = navigation.state.params ? navigation.state.params.showLoading : null;

        return {
            headerRight:
                <TouchableOpacity
                    onPress={action}
                    style={{marginEnd: 15}}>
                    {showLoading ?
                        <ActivityIndicator color={colors.colorLoader}/> :
                        <Text style={{color: colors.white, fontWeight: 'bold'}}>CROP</Text>
                    }
                </TouchableOpacity>
        }
    };

    componentDidMount() {

        const {media} = this.props.navigation.state.params;

        const params = {
            headerRight: {
                trimMedia: this.trimMedia,
                showLoading: false,
            }
        };

        const initialStatus = {
            shouldPlay: true, 
            rate: 1.0, 
            isMuted: false
        }

        // this.refs.video.loadAsync(media.uri,  initialStatus, true)

        Animated.timing(this.state.fadeAnim, {toValue: 0, duration: 2000}).start();

     


        this.props.navigation.setParams(params);
    }


    _handleSlide(slider, value) {
        const {seekStart, seekEnd} = this.state;

        this.setState({
            [slider]: value
        })

    }

    onValuesChange = ([seekStart, seekEnd]) => {

        // this.refs.video.setPositionAsync(seekStart);

        // this.setState({
        //     seekStart,
        //     seekEnd,
        // })
    };

    onValuesChangeFinish = ([seekStart, seekEnd]) => {

        this.setState({
            seekStart,
            seekEnd,
            playBackEnded: false
        }, _ => {
            if (this.state.isPlaying) {
                this.refs.video.playFromPositionAsync(seekStart).then(() => {
                    this.refs.video.playAsync().then(_ => {
                    });
                });
            }
        })
    };

    onPlaybackStatusUpdate = (status) => {

        const {seekEnd, playBackEnded} = this.state;

        if (!playBackEnded) {
            if (status.positionMillis <= seekEnd) {
                this.setState({
                    seekBarPosition: status.positionMillis,
                    // isPlaying: true,
                });
            }


            if (status.positionMillis >= seekEnd) {
                // alert("stop")

                this.refs.video.pauseAsync().then(() => {
                    this.setState({
                        isPlaying: false,
                        playBackEnded: true,
                        playControlVisible: true
                    }, () => {
                        Animated.timing(this.state.fadeAnim, {
                            toValue: 1,
                            duration: this.fadeAnimDuration,
                        }).start();
                    });
                });
            }
        }

        // console.log(status);

    };

    _handlePlayBackBtnPressed = () => {
        const {isPlaying, playBackEnded} = this.state;

        if (isPlaying) {
            this.shouldHidePlayBackBtn = false;
            this.refs.video.pauseAsync().then(() => {
                this.setState({
                    isPlaying: false,
                    playBackEnded: false
                });
            });
        }

        if (!isPlaying) {
            this.shouldHidePlayBackBtn = true;
            if (!playBackEnded) {
                this.refs.video.playFromPositionAsync(this.state.seekStart).then(() => {
                    this.refs.video.playAsync().then(() => {
                        this.setState({isPlaying: true, playBackEnded: false})
                    });
                })

            } else {
                this.refs.video.playFromPositionAsync(this.state.seekStart).then(() => {
                    this.refs.video.playAsync().then(() => {
                        this.setState({isPlaying: true, playBackEnded: false})
                    });
                });
            }

            setTimeout(() => {
                if (this.shouldHidePlayBackBtn) {
                    Animated.timing(this.state.fadeAnim, {
                        toValue: 0,
                        duration: this.fadeAnimDuration,
                    }).start();
                    this.setState({playControlVisible: false})
                }
            }, 2000)
        }

    };

    _handleTogglePlayControl = () => {

        const {fadeAnim, playControlVisible} = this.state;
        Animated.timing(fadeAnim, {
            toValue: !this.state.playControlVisible ? 0 : 1,
            duration: this.fadeAnimDuration,
        }).start();

        this.setState((state) => ({
            playControlVisible: !state.playControlVisible
        }))


    };

    trimMedia = () => {

        const {source, seekStart, seekEnd} = this.state;

        const {onMediaTrimmed, media} = this.props.navigation.state.params;

      
        if (Platform.OS === "android") {
            var RNGRP = require('react-native-get-real-path');
            RNGRP.getRealPathFromURI(source).then(filePath => {
                this.finishCropping(filePath);
            });
        } else {
            this.finishCropping(source);
        }
    };

    finishCropping = (source) => {
        const {onMediaTrimmed, media} = this.props.navigation.state.params;
        const {seekStart, seekEnd, duration} = this.state;


        const options = {
            startTime: Number(moment(seekStart).format("s")),
            endTime: Number(moment(seekEnd).format("s")),
        };


        // If user did not trimmed the video, send it back without processing it
        if (seekStart === 0 && seekEnd === duration) {
            this.refs.progressDialog.dismiss().then(_ => {
                onMediaTrimmed(media.uri, media);
                this.props.navigation.goBack();
            });


        } else {
            this.refs.progressDialog.show("Trimming video");
            ProcessingManager.trim(source, options)
                .then((newSource) => {
                    this.refs.progressDialog.dismiss().then(_ => {
                        onMediaTrimmed(newSource, media);
                        this.props.navigation.goBack();
                    });

                })
                .catch(err => {
                    this.refs.progressDialog.dismiss().then(_ => {
                        Alert.alert('Error', "This file can't be trimmed")
                    })
                });


        }


    };

    render() {
        const {source, mediaType, fadeAnim, seekStart, seekEnd, duration, playControlVisible, isPlaying, seekBarPosition, playBackEnded} = this.state;
        const {media} = this.props.navigation.state.params;

        const resizedHeight = getMediaHeight(media.width, media.height);


        return (
            <View style={styles.container}>


                <TouchableWithoutFeedback
                    onPress={this._handleTogglePlayControl}>
                    <View style={[theme.center, {flex: 1}]}>
                        <Video
                            ref="video"
                            progressUpdateIntervalMillis={500}
                            source={{uri: source}}
                            onPlaybackStatusUpdate={this.onPlaybackStatusUpdate}
                            rate={1.0}
                            volume={1.0}
                            shouldPlay={true}
                            // isLooping={true}
                            isMuted={false}
                            resizeMode="contain"
                            style={{flex: 1, width: dimens.viewportWidth, height: resizedHeight}}/>

                        {/*{playControlVisible && (*/}
                        <Animated.View style={[styles.playAction, theme.center, {
                            opacity: fadeAnim,
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0
                        }]}>
                            <TouchableOpacity
                                onPress={this._handlePlayBackBtnPressed}
                                style={[styles.playButton, theme.center]}>
                                <View style={styles.icon}>
                                    <MaterialIcons color={colors.white} size={40}
                                                   name={playBackEnded ? "replay" : isPlaying ? "pause" : "play-arrow"}/>
                                </View>


                            </TouchableOpacity>
                        </Animated.View>
                        {/*)}*/}
                    </View>
                </TouchableWithoutFeedback>


                <View style={styles.footer}>
                    <View style={styles.seekBar}>
                        <Text style={[theme.font, styles.duration, {marginRight: 15}]}>
                            {moment(seekBarPosition).format("mm:ss")}
                        </Text>
                        <MultiSlider
                            step={1}
                            sliderLength={dimens.viewportWidth - 150}
                            max={duration}
                            onValuesChangeFinish={this.onValuesChangeFinish}
                            markerStyle={styles.markerStyle}
                            onValuesChange={this.onValuesChange}
                            pressedMarkerStyle={styles.markerStyle}
                            trackStyle={styles.trackStyle}
                            isMarkersSeparated={true}
                            values={[seekStart, seekEnd]}
                        />
                        <Text style={[theme.font, styles.duration, {marginLeft: 15}]}>
                            {moment(duration).format("mm:ss")}
                        </Text>

                    </View>


                </View>

                <ProgressDialog ref="progressDialog"/>


            </View>
        )
    }
}


const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',

        backgroundColor: '#000',

    },

    footer: {

        justifyContent: 'flex-end',
        // marginBottom: 15,
    },


    header: {
        alignItems: 'center',
        paddingStart: 10,
        paddingEnd: 10,
        paddingBottom: 15,

    },

    playButton: {
        backgroundColor: 'rgba(0, 0, 0, .5  )',
        width: 60,
        height: 60,
        borderRadius: dimens.getRadius(60),
        elevation: 12,
        /*  width: 40,
          height: 40,
          borderRadius: dimens.getRadius(40),
          backgroundColor: 'rgba(0, 0, 0, .8  )'*/
    },

    duration: {
        // fontWeight: 'bold',
        color: colors.white,
        fontSize: 12,

    },

    markerStyle: {
        width: 20,
        height: 30,
        // shadowOpacity: 0,
        borderRadius: 1,
        marginTop: 15,
        borderWidth: 2,
        borderColor: '#838486',
        backgroundColor: '#EAEAEA',
    },
    trackStyle: {
        height: 15,
        borderRadius: 1,
        backgroundColor: '#d5d8e8',
    },
    seekBar: {

        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'center'
    },

    mediaContainer: {
        justifyContent: 'center'
        // flex: 1,

    },

    backArrow: {
        marginEnd: 15,
    },
});

export default ProcessMedia;