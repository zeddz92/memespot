import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    FlatList,
    Image,
    TouchableOpacity,
    ScrollView,
    Platform,
    KeyboardAvoidingView
} from 'react-native';
import * as colors from '../utils/colors';
import * as dimens from '../utils/dimens';
import {Constants} from 'expo';

import {theme} from "../utils/styles";

import Media from '../components/Media';
import {MaterialIcons, MaterialCommunityIcons, Ionicons} from '@expo/vector-icons';


const MediaVisualizer = (props) => {


    const mockMedia = {

        media: {
            width: 658,
            height: 425,
            type: 1,
            url: 'https://static.timesofisrael.com/www/uploads/2016/05/%D7%90%D7%9D%D7%A6.jpg',
            thumbnail: 'patht-to-thumnail',
            extension: 'jpg'
        }


    };

    const {data} = props.navigation.state.params;
    const {user, medias} = data;



    return (
        <View style={styles.container}>

            <View style={[styles.header, theme.row]}>
                <TouchableOpacity
                    style={styles.backArrow}
                    onPress={() => props.navigation.goBack()}>
                    {Platform.OS === 'android' ? (
                        <MaterialIcons color={colors.white} name="arrow-back" size={25}/>
                    ) : (
                        <Ionicons color={colors.white} name="ios-arrow-back" size={30}/>)
                    }

                </TouchableOpacity>
                <Image
                    style={styles.profile_image}
                    source={{uri: user.profile_picture_url}}/>
                <View style={{flex: 1}}>
                    <Text style={[theme.font, styles.username]}>@{user.username}<Text
                        style={[styles.timeElapsed, theme.font]}> • {data.time}</Text></Text>
                    <Text style={[theme.font, styles.text]}>{data.text ?  data.text : data.title}</Text>
                </View>
            </View>

            {medias && (
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}  style={[{flex: 1}]}>
                    <View style={[theme.center, {flex: 1}]}>
                        <Media style={theme.center} padding={0} item={data}/>

                    </View>
                </ScrollView>
            )}


        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        // padding: 15,
        paddingTop: 15,
        paddingTop: Constants.statusBarHeight,
        flex: 1,
        backgroundColor: colors.black,
    },
    username: {
        fontSize: 16,
        color: colors.white,
        fontWeight: 'bold'
    },

    backArrow: {
        marginEnd: 15,
    },
    headerText: {},
    profile_image: {
        width: 35,
        height: 35,
        resizeMode: 'cover',
        borderRadius: dimens.getRadius(35),
        marginRight: 10
    },

    header: {
        alignItems: 'center',
        paddingStart: 10,
        paddingEnd: 10,
        paddingBottom: 15,

    },

    text: {
        fontSize: 14,
        color: colors.white,
    },

});

export default MediaVisualizer;