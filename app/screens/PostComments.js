import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    FlatList,
    Image,
    TouchableOpacity,
    Alert,
    Share,
    Clipboard,
    ScrollView,
    Easing,
    Animated,
    ActivityIndicator,
    Platform,
    Keyboard,
    KeyboardAvoidingView
} from 'react-native';
import Comment from '../components/Comment';
import * as dimens from '../utils/dimens';
import * as colors from '../utils/colors';
import {MaterialCommunityIcons, MaterialIcons, Entypo} from '@expo/vector-icons';

import ProgressDialog from '../components/ProgressDialog';


import {ImagePicker} from 'expo';
import {api} from "../utils/api";
import {theme} from "../utils/styles";
import {connect} from 'react-redux';
import * as commentActions from "../actions/comment";
import ActionSheet from "../components/ActionSheet";
import SnackBar from "../components/SnackBar";
import {viewportWidth} from "../utils/dimens";


const Post = (props) => {

    // const {item} = props;
    const {item} = props;

    return (
        <View style={styles.post}>
            <Image
                style={styles.profileImg}
                source={{uri: item.user.profile_picture_url}}/>
            <View style={styles.container}>
                <Text style={[styles.font, styles.username]}>{item.user.username}</Text>
                <Text style={[styles.font, styles.title]}>{item.title}</Text>
            </View>
        </View>
    )
};


class PostComments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            post: props.navigation.state.params.post || {user: {}},
            comments: [],
            replies: {},
            loaders: {},
            exclude: {},
            footerLoading: false,

            isPosting: false,
            refreshing: false,
            animated: new Animated.Value(0),
            updatingCommentAnimation: new Animated.Value(0),
            showLoading: false,
            postLoading: !props.navigation.state.params.post,
            error: null,
            replyingComment: null,
            updatingComment: null,
            textInputComment: "",
            imageFromGallery: null,
            currentPage: 0,
            lastPage: 0,
        };

        this.callback = null;

        this.inputs = {};
    }

    handleCopyToClipBoard = (comment) => {

        Clipboard.setString(comment.text);

        this.refs.snackBar.show("Copied to clipboard");
    };

    handleShare = (comment) => {

        Share.share({
            title: "title",
            message: "https://www.reddit.com/r/Showerthoughts/comments/8ti8k6/your_future_self_is_talking_shit_about_you/e17vv66?utm_source=reddit-android",
            url: "https://www.gettyimages.ca/gi-resources/images/Homepage/Hero/UK/CMS_Creative_164657191_Kingfisher.jpg"
        }, {dialogTitle: "dialog title", subject: "subject"});

    };
    handleDeletePressed = (comment, parent, removeChild) => {
        const {user} = this.props;
        const {post} = this.props.navigation.state.params;

        Alert.alert(
            'Delete',
            'Are you sure you want to delete your comment?',
            [
                {text: 'Cancel', onPress: () => console.log("cancel")},
                {
                    text: 'Ok', onPress: () => {

                        if (comment.id !== parent.id) {
                            removeChild(comment, parent);
                        }
                        this.setState((state) => ({
                            comments: state.comments.filter(c => (c.id !== comment.id))
                        }), () => {
                            api.comment.destroy(user.data.id, post.id, comment.id).then(response => {
                                // alert(JSON.stringify(response));
                            }).catch(error => {
                                alert(JSON.stringify(error));
                            });
                        });


                    }
                },
            ],
            {cancelable: false}
        )

    };

    attemptStrikeRequest = (comment, parent, reasonId, removeChild) => {
        const {user} = this.props;


        Alert.alert(
            '',
            'Thanks. We\'ll have a look at your report and we will remove the comment if the content is inappropriate',
            [
                {
                    text: 'Ok', onPress: () => {
                        if (comment.id !== parent.id) {
                            removeChild(comment, parent);
                        } else {
                            this.setState((state) => ({
                                comments: state.comments.filter(c => (c.id !== comment.id))
                            }))
                        }
                    }
                }
            ],
            {cancelable: false}
        );

        api.comment.strike(user.data.id, comment.id, reasonId).then(response => {
            // alert(JSON.stringify(response));
            if (!response.error) {
                if (comment.id !== parent.id) {
                    removeChild(comment, parent);
                }
            }

        }).catch(error => {
            alert(JSON.stringify(error));

        });
    };

    handleReportPressed = (comment, parent, removeChild) => {
        const options = {
                title: 'Report Comment',
                buttons: [
                    {
                        text: "Spam",
                        icon: "delete",
                        onPress: () => this.attemptStrikeRequest(comment, parent, 1, removeChild)
                    },
                    {
                        text: "Harassment or Bullying",
                        icon: "emoticon-devil",
                        onPress: () => this.attemptStrikeRequest(comment, parent, 2, removeChild)
                    },
                    {
                        text: "Hate speech",
                        icon: "bullhorn",
                        onPress: () => this.attemptStrikeRequest(comment, parent, 3, removeChild)
                    }

                ]
            }
        ;

        this.refs.actionSheet.show(options);
    };


    handleViewMorePressed = (comment, parent, updateChild, removeChild) => {

        const {user} = this.props;

        const actions = [
            {
                text: "Copy Text",
                icon: "content-copy",
                onPress: () => this.handleCopyToClipBoard(comment)
            },
            /*  {
                  text: "Share",
                  icon: "share",
                  onPress: () => this.handleShare(comment)
              },*/
            {
                text: "Report",
                icon: "flag",
                onPress: () => this.handleReportPressed(comment, parent, removeChild)
            }
        ];

        const userActions = [
            {
                text: "Edit",
                icon: "pencil",
                onPress: () => {
                    this.callback = updateChild;
                    this.setState({
                        replyingComment: null,
                        textInputComment: comment.text,
                        updatingComment: comment,

                    }, () => {
                        Animated.timing(this.state.animated, {
                            toValue: 1,
                            duration: 250,
                        }).start(() => {
                            this.inputs["textInputComment"].focus();
                        });
                    })
                }
            },
            {
                text: "Copy Text",
                icon: "content-copy",
                onPress: () => this.handleCopyToClipBoard(comment)
            },
            /*  {
                  text: "Share",
                  icon: "share",
                  onPress: () => this.handleShare(comment)
              },*/
            {
                text: "Delete",
                icon: "delete",
                onPress: () => this.handleDeletePressed(comment, parent, removeChild)
            }
        ];

        const options = {
            buttons: user.data.id === comment.user.id ? userActions : actions
        }

        this.refs.actionSheet.show(options);

    };

    componentDidMount() {

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));


        const {post, postId} = this.props.navigation.state.params;

        if (postId) {
            this.setState({postLoading: true});
            api.post.getPost(postId).then(response => {
                if (!response.error) {
                    this.setState({post: response.data, postLoading: false}, () => this.makeRequest());
                }

            }).catch(error => {

            })
        } else {
            this.setState({showLoading: true});
            this.makeRequest();
        }
        // alert(post.id);
        // this.inputs["textInputComment"].focus();
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    keyboardDidShow(e) {

        const {replyingComment, replyingCommentParent} = this.state;

        this.refs.commentList.scrollToItem({item: replyingCommentParent})

    }

    keyboardDidHide(e) {
        // this.setState({bottom: 0})
        // this.setState({replyingComment: null})
    }


    renderComment = ({item, index}) => {
        return (
            <Comment
                replies={this.state.replies}
                viewMore={this.handleViewMorePressed}
                doFeedBack={this.doFeedBack}
                onReply={this._handleReplyBtnPressed} item={item}/>
        )
    };

    doFeedBack = (comment, feedback, handleCommentFeedback) => {
        const {user, navigation} = this.props;
        const {post} = this.props.navigation.state.params;

        if (user.data.id === 0) {
            navigation.navigate("Login");
            return;
        }

        const userFeedback = comment.user_feedback ? comment.user_feedback.action : 0;

        const likes = (feedback === 0 || feedback === 2) && userFeedback === 1 ? comment.likes - 1 :
            feedback === 1 && (userFeedback === 0 || userFeedback === 2) ? comment.likes + 1 : comment.likes;
        const dislikes = (feedback === 0 || feedback === 1) && userFeedback === 2 ? comment.dislikes - 1 :
            feedback === 2 && (userFeedback === 0 || userFeedback === 1) ? comment.dislikes + 1 : comment.dislikes;

        const updatedComment = {...comment, likes, dislikes, user_feedback: {action: feedback}};
        handleCommentFeedback(updatedComment);

        this.setState((state) => ({
            comments: state.comments.map(c => (
                c.id === comment.id
                    ? updatedComment
                    : c
            ))
        }), () => {
            api.comment.feedback(user.data.id, comment.id, feedback).then(response => {

                // alert(JSON.stringify(response));
                if (!response.error) {
                    this.setState((state) => ({
                        isPosting: false,
                        replyingComment: null,
                    }));


                }


            }).catch(error => {
                alert(JSON.stringify(error))
            })
        });






        // this.props.makeFeedback(user.data.id, post.id, comment.id, feedback).then(response => {
        //     // alert(JSON.stringify(response))
        //
        // }).catch(error => {
        //     alert(JSON.stringify(error))
        // });
        // api.post.feedback(user.data.id, post.id, feedback).then(response => {
        //     alert(JSON.stringify(response))
        //
        // }).catch(err => alert(JSON.stringify(err)))
    };

    renderFooter = () => {
        if (!this.state.footerLoading) return null;
        return (
            <View style={styles.footerLoader}>
                <ActivityIndicator animating size="large"/>
            </View>
        )
    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            // mediaTypes: 'All',
            // aspect: [4, 3],
            base64: true,
            exif: true,
        });

        // alert(result.base64);

        if (!result.cancelled) {
            const base64Image = `data:image/jpeg;base64,${result.base64}`;
            this.setState({imageFromGallery: result.uri});
        }

    };

    _handleReplyBtnPressed = (comment, parent, callback) => {

        const {user, navigation} = this.props;

        const {comments} = this.state;

        if (user.data.id === 0) {
            navigation.navigate("Login");
            return;
        }


        this.setState({
            updatingComment: null,
            replyingComment: comment,
            replyingCommentParent: parent,
            textInputComment: `@${comment.user.username} `
        }, () => {
            Animated.timing(this.state.animated, {
                toValue: 1,
                duration: 250,
            }).start(() => {
                this.inputs["textInputComment"].focus();
            });

        });

        this.callback = callback;


        // setTimeout(()=> {
        //     this.refs.commentList.scrollToItem({item: comment})
        // }, 3000)


    };

    makeRequest = () => {
        const {page, post} = this.state;
        const {user} = this.props;

        if (!post.id) return;
        // this.setState({showLoading: true});
        // const { post } = this.props.navigation.state.params;

        api.comment.getAll(user.data.id, post.id, page).then(response => {
            if (!response.error) {
                this.setState((state) => ({
                    comments: page === 1 ? response.data : [...state.comments, ...response.data],
                    refreshing: false,
                    currentPage: response.current_page,
                    lastPage: response.last_page,
                    footerLoading: false,
                    showLoading: false,
                    showFooter: false,
                    replyingComment: null,
                    error: response.code !== 200 ? {message: response.error, code: response.code} : null
                }));
            } else {
                this.setState((state) => ({
                    refreshing: false,
                    showLoading: false,
                    footerLoading: false,
                    currentPage: 0,
                    lastPage: 0,
                    showFooter: false,
                }));
            }

        }).catch(error => {
            this.setState((state) => ({
                error,
                refreshing: false,
                showLoading: false,
                footerLoading: false,
                currentPage: 0,
                lastPage: 0,
                showFooter: false,
            }));
        })

        // this.props.getComments(user.data.id, post.id, page).then(response => {
        //
        //     // console.log(response);
        //
        //     if (!response.error) {
        //         const payload = response.payload.response;
        //         this.setState((state) => ({
        //             comments: response.error ? [] : page === 1 ? payload.data : [...state.comments, ...payload.data],
        //             refreshing: false,
        //             currentPage: payload.current_page,
        //             lastPage: payload.last_page,
        //             showLoading: false,
        //             showFooter: false,
        //             replyingComment: null,
        //             error: payload.code !== 200 ? {message: payload.error, code: payload.code} : null
        //         }));
        //     } else {
        //         // alert(JSON.stringify(response.error))
        //     }
        //
        // }).catch(error => {
        //     this.setState((state) => ({
        //         error,
        //         refreshing: false,
        //         showLoading: false,
        //         currentPage: 0,
        //         lastPage: 0,
        //         showFooter: false,
        //     }));
        // });

    };

    buildComment = () => {
        const {user} = this.props;
        const {post} = this.props.navigation.state.params;
        const {textInputComment, imageFromGallery, replyingComment} = this.state;

        const comment = {
            id: 0,
            parent_id: replyingComment ? replyingComment.id : null,
            text: textInputComment,
            user_id: user.data.id,
            time: "...",
            isPosting: true,
            replies_count: 0,
            likes: 0,
            dislikes: 0,
            user: user.data,
            first_reply: null,
        };

        return comment;

    };

    updateComment = () => {
        const {user} = this.props;
        const {post} = this.props.navigation.state.params;
        const {textInputComment, imageFromGallery, updatingComment} = this.state;

        this.setState((state) => ({
            textInputComment: "",
            isPosting: true,
            imageFromGallery: null
        }));

        const updateComment = {
            ...updatingComment,
            text: textInputComment,
            isPosting: true,
        };

        if (this.callback) {
            this.callback(updateComment, updatingComment.id);
        }

        api.comment.update(post.id, updatingComment.id, user.data.id, textInputComment, imageFromGallery).then(response => {

            if (!response.error) {

                this.setState((state) => ({
                    isPosting: false,
                    comments: state.comments.map(comment => (
                        comment.id === updatingComment.id ? response.data : comment
                    )),
                    updatingComment: null,
                }));

                if (this.callback) {
                    this.callback(response.data, updatingComment.id);
                    this.callback = null;
                }
            }
        }).catch(error => {
            alert(JSON.stringify(error));

        });
    };

    createComment = () => {

        const {textInputComment, imageFromGallery, replyingComment, updatingComment} = this.state;

        if (updatingComment) {
            this.updateComment();
            return;
        }

        const {user} = this.props;
        const {post} = this.props.navigation.state.params;

        const comment = this.buildComment();
        if (replyingComment) {
            this.callback(comment);
        }

        this.setState((state) => ({
            textInputComment: "",
            isPosting: true,
            imageFromGallery: null,
            comments: !replyingComment ? [comment, ...state.comments] : state.comments,
        }));


        // this.refs.progressDialog.show("Replying...");

        const replyingCommentId = replyingComment ? replyingComment.id : null;

        this.props.createComment(post.id, user.data.id, textInputComment, imageFromGallery, replyingCommentId).then(response => {


            if (!replyingComment) {
                this.refs.commentList.scrollToOffset({
                    offset: 0,
                    animated: true
                });
            }


            this.setState((state) => ({
                isPosting: false,
                comments: !replyingComment ? state.comments.map(comment => (
                    comment.id === 0 ? response.payload.data : comment
                )) : state.comments,
                replyingComment: null,
            }));


            if (this.callback) {
                this.callback(response.payload.data);
                this.callback = null;
            }


        }).catch(error => {
            this.refs.progressDialog.dismiss().then(_ => {
                alert(JSON.stringify(error));

            });
        })
    }

    _handleRefresh = () => {
        this.setState({
            page: 1,
            refreshing: true,
        }, () => {
            this.makeRequest();
        })
    };

    _handleLoadMore = () => {
        const {currentPage, lastPage} = this.state;
        if (currentPage < lastPage) {
            this.setState({
                page: this.state.page + 1,
                footerLoading: true
            }, () => {
                this.makeRequest();
            })
        }
    };


    render() {


        const {user, comments} = this.props;
        // const { post } = this.props.navigation.state.params;
        const {postLoading, post, textInputComment, imageFromGallery, refreshing, showLoading, isPosting, replyingComment, updatingComment} = this.state;

        const transformStyle = [{
            translateY: this.state.animated.interpolate({
                inputRange: [0, 1],
                outputRange: [100, 1]
            })
        }];

        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior='padding'
                keyboardVerticalOffset={dimens.keyboardVerticalOffset}>

                {postLoading ? <ActivityIndicator style={theme.loader} size="large" color="#0000ff"/> :
                    <Post item={post}/>}


                {!postLoading && showLoading ?
                    <View style={{flex: 1}}>
                        <ActivityIndicator style={theme.loader} size="large" color="#0000ff"/>
                    </View> :
                    <FlatList
                        ref="commentList"
                        data={this.state.comments}
                        renderItem={this.renderComment}
                        keyExtractor={(item, index) => item.id.toString()}
                        refreshing={refreshing}
                        onRefresh={this._handleRefresh}
                        onEndReached={this._handleLoadMore}
                        onEndReachedThreshol={10}
                        ListFooterComponent={this.renderFooter}
                    />
                }

                {user.data.id && (
                    <View>

                        {updatingComment && (
                            <Animated.View style={[styles.replyingContainer, {transform: transformStyle}]}>
                                {updatingComment.medias.length > 0 && (
                                    <Image
                                        style={styles.updateCommentMediaImg}
                                        source={{uri: updatingComment.medias[0].media_url}}/>
                                )}


                                <View style={{flex: 1}}>
                                    <Text
                                        style={[theme.font, {fontWeight: 'bold', fontSize: 12}]}>Updating
                                        Comment</Text>
                                    <Text style={[theme.font, {
                                        color: '#717171',
                                        fontSize: 12
                                    }]}>{updatingComment.text}</Text>
                                </View>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({
                                            updatingComment: null,
                                            textInputComment: "",
                                            imageFromGallery: null
                                        }, () => {
                                            Animated.timing(this.state.animated, {
                                                toValue: 0,
                                                duration: 250,
                                            }).start();
                                        })
                                    }}
                                    style={{justifyContent: 'flex-end'}}>
                                    <MaterialIcons name="close" size={20} color={colors.colorActions}/>

                                </TouchableOpacity>

                            </Animated.View>

                        )}


                        {replyingComment && (
                            <Animated.View style={[styles.replyingContainer, {transform: transformStyle}]}>
                                <Image
                                    style={styles.replyingProfileImg}
                                    source={{uri: replyingComment.user.profile_picture_url}}/>

                                <View style={{flex: 1}}>
                                    <Text
                                        style={[theme.font, {
                                            fontWeight: 'bold',
                                            fontSize: 12
                                        }]}>@{replyingComment.user.username}</Text>
                                    <Text style={[theme.font, {
                                        color: '#717171',
                                        fontSize: 12
                                    }]}>{replyingComment.text}</Text>
                                </View>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({
                                            replyingComment: null,
                                            textInputComment: "",
                                            imageFromGallery: null
                                        }, () => {
                                            Animated.timing(this.state.animated, {
                                                toValue: 0,
                                                duration: 250,
                                            }).start();
                                        })
                                    }}
                                    style={{justifyContent: 'flex-end'}}>
                                    <MaterialIcons name="close" size={20} color={colors.colorActions}/>

                                </TouchableOpacity>

                            </Animated.View>
                        )}

                        <View style={styles.commentSection}>

                            <View style={[styles.row, {flex: 1, paddingEnd: 55}]}>
                                <Image
                                    style={styles.profileImg}
                                    source={user.data.profile_picture_url ? {uri: user.data.profile_picture_url} : require('../assets/img/profile.png')}/>
                                <TextInput
                                    ref={input => {
                                        this.inputs['textInputComment'] = input;
                                    }}
                                    onChangeText={(text) => this.setState({textInputComment: text})}
                                    /*    onFocus={this.inputFocused.bind(this, "newMessage")}
                                        onBlur={() => {this.refs.scrollView.scrollTo(0,0)}}*/
                                    style={styles.input}
                                    value={textInputComment}
                                    underlineColorAndroid='transparent'
                                    placeholder="Add a comment"/>
                            </View>


                            <View style={[styles.row]}>
                                {!updatingComment && (
                                    <TouchableOpacity
                                        disabled={isPosting}
                                        onPress={this._pickImage}>
                                        {imageFromGallery ?
                                            <Image source={{uri: imageFromGallery}} resizeMode="contain"
                                                   style={[styles.commentSectionIcon, {width: 30, height: 30}]}/> :
                                            <Entypo style={styles.commentSectionIcon} name="camera" size={25}
                                                    color={colors.colorActions}/>
                                        }
                                    </TouchableOpacity>
                                )}

                                <TouchableOpacity
                                    disabled={isPosting}
                                    onPress={this.createComment}>
                                    <MaterialIcons name="send" size={25} color={colors.colorActions}/>
                                </TouchableOpacity>

                            </View>


                        </View>

                    </View>

                )}
                <ProgressDialog ref="progressDialog"/>

                <ActionSheet ref="actionSheet"/>
                <SnackBar ref="snackBar"/>


            </KeyboardAvoidingView>
        );
    }

}

const
    styles = StyleSheet.create({
        replyingContainer: {
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
            paddingStart: 15,
            backgroundColor: '#F9F7F9'
        },
        container: {
            flex: 1,
            backgroundColor: '#fff',
            /*  alignItems: 'center',
              justifyContent: 'center',*/
        },

        font: {
            color: '#37393D',
            fontFamily: Platform.OS === 'android' ? 'notoserif' : 'Avenir'
        },


        input: {
            width: '100%'
            // flex: 1,
            // minWidth: 150,
        },

        username: {
            fontSize: 16,
            fontWeight: 'bold'
        },

        title: {
            // fontSize: 16,
            fontWeight: '600',
            color: 'grey'
        },


        commentSectionIcon: {
            marginEnd: 15
        },
        post: {
            flex: 1,
            flexDirection: 'row',
            borderBottomWidth: .2,
            padding: 15,
            borderColor: '#cbcbcb',

        },
        row: {
            flexDirection: 'row',
            alignItems: 'center'
        },

        updateCommentMediaImg: {
            width: 35,
            height: 35,
            marginEnd: 15,

        },
        replyingProfileImg: {
            width: 35,
            height: 35,
            marginEnd: 15,
            borderRadius: dimens.getRadius(35)
        },
        profileImg: {
            width: 35,
            height: 35,
            marginEnd: 15,
            borderRadius: dimens.getRadius(35)
        },
        commentSection: {
            padding: 5,
            paddingStart: 15,
            paddingEnd: 15,
            borderTopWidth: .2,
            borderColor: '#cbcbcb',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between'
        }
    });

function

mapStateToProps({user, comment}, ownProps) {

    const {post} = ownProps.navigation.state.params;
    return {
        comments: [],
        user
    }
}

function

mapDispatchToProps(dispatch) {
    return {
        createComment: (postId, userId, text, media = null, parentId = null) => dispatch(commentActions.createComment(postId, userId, text, media, parentId)),
        getComments: (userId, postId, page) => dispatch(commentActions.getComments(userId, postId, page)),
        makeFeedback: (userId, postId, commentId, feedback) => dispatch(commentActions.makeFeedback(userId, postId, commentId, feedback))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)

(
    PostComments
)
;
