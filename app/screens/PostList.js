import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert,
    ScrollView,
    FlatList,
    InteractionManager,
    VirtualizedList,
    ActivityIndicator
} from 'react-native';
import Post from '../components/Post';
import {posts} from '../utils/mocks/posts';
import SvgUri from 'react-native-svg-uri';
import * as colors from '../utils/colors';
import * as dimens from '../utils/dimens';
import * as mediaActions from '../actions/media';
import Home from './Home';
import {connect} from 'react-redux';
import * as postActions from "../actions/post";
import {api} from '../utils/api';

import ActionSheet from '../components/ActionSheet';


const HOT = "likes";
const FRESH = "created_at";
const TRENDING = "";

class PostList extends React.Component {


    // static navigationOptions = {
    //     header: null
    // }


    constructor(props) {
        super(props);

        const usrId = props.screenProps ?  props.screenProps.userId : null;


        this.state = {
            posts: usrId ? [] : props.post,
            refreshing: false,
            showLoading: props.post.length <= 0,
            showFooter: false,
            page: 1,
            error: null,
            currentPage: 0,
            lastPage: 0,
        };

        this.feedRefs = {};
        this.offset = 0;

        this.navigation = props.screenProps ? props.screenProps.navigation : props.navigation;
    }


    attemptStrikeRequest = (post, reasonId) => {
        const {user} = this.props;

        Alert.alert(
            '',
            'Thanks. We\'ll have a look at your report and we will remove the post if the content is inappropriate',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.setState((state) => ({
                            posts: state.posts.filter(p => p.id !== post.id)
                        }), () => this.props.removePost(post.id));

                    }
                }
            ],
            {cancelable: false}
        );

        api.post.strike(user.data.id, post.id, reasonId).then(response => {
            // alert(JSON.stringify(response));
            if (!response.error) {

            }

        }).catch(error => {
            alert(JSON.stringify(error));

        });
    };

    handleReportPressed = (post) => {
        const options = {
                title: 'Report Post',
                buttons: [
                    {
                        text: "Spam",
                        icon: "delete",
                        onPress: () => this.attemptStrikeRequest(post, 1)
                    },
                    {
                        text: "Harassment or Bullying",
                        icon: "emoticon-devil",
                        onPress: () => this.attemptStrikeRequest(post, 2)
                    },
                    {
                        text: "Hate speech",
                        icon: "bullhorn",
                        onPress: () => this.attemptStrikeRequest(post, 3)
                    }

                ]
            }
        ;

        this.refs.actionSheet.show(options);
    };

    handleSaveToFavoritePressed = (post) => {
        const {user} = this.props;
        // alert("fav");
        this.setState((state) => ({
            posts: state.posts.map((p) => (
                p.id === post.id
                    ? {...p, favorite: !post.favorite}
                    : p
            ))
        }), () => {
            this.props.handleFavorite(post.id, !post.favorite);
            api.post.handleFavorites(user.data.id, post.id, !post.favorite).then(response => {
                // alert(JSON.stringify(response));
                if (!response.error) {

                }
            })
        });

    }

    handleDeletePressed = (post) => {
        const {user} = this.props;

        Alert.alert(
            'Delete',
            'Are you sure you want to delete your post?',
            [
                {text: 'Cancel', onPress: () => console.log("cancel")},
                {
                    text: 'Ok', onPress: () => {

                        this.setState((state) => ({
                            posts: state.posts.filter(p => p.id !== post.id)
                        }), () => this.props.removePost(post.id));

                        api.post.destroy(user.data.id, post.id).then(response => {
                            // alert(JSON.stringify(response));
                        }).catch(error => {
                            alert(JSON.stringify(error));
                        });
                    }
                },
            ],
            {cancelable: false}
        )

    };

    handleViewMorePressed = (post) => {

        const {user} = this.props;

        const actions = [
            {
                text: "Report",
                icon: "flag",
                onPress: () => this.handleReportPressed(post)
            }
        ];

        const userActions = [
            {
                text: "Delete",
                icon: "delete",
                onPress: () => this.handleDeletePressed(post)
            }
        ];

        const options = {
            buttons: user.data.id === post.user.id ? userActions : actions
        }

        this.refs.actionSheet.show(options);

    };

    goToComments = (post) => {
        this.navigation.navigate("PostComments", {post});

    };

    renderPost = ({item, index}) => {
        return (
            <Post
                ref={(ref) => {
                    if (item.medias[0].type === 3) {
                        this.props.registerMedia(item.id);
                        this.feedRefs[`REF_FEED_${item.id}`] = ref
                    }
                }}
                saveToFavorite={this.handleSaveToFavoritePressed}
                viewMore={this.handleViewMorePressed}
                rootNavigation={this.navigation}
                item={item}
                doFeedBack={this.doFeedBack}
                goToComments={this.goToComments}/>
        )
    };

    _handleRefresh = () => {
        this.setState({
            page: 1,
            refreshing: true,
        }, () => {
            this.makePostsRequest();
        })
    };

    _handleLoadMore = () => {
        const {page, lastPage, currentPage} = this.state;
        if (currentPage < lastPage) {
            this.setState({
                page: this.state.page + 1,
                showFooter: true,
            }, () => {
                this.makePostsRequest();
            })
        }

    };

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {search} = this.props;
        if (search !== nextProps.search) {
            this.setState({
                posts: [],
                showLoading: true,
            });


            this.makePostsRequest(nextProps.search)

        }
    }

    show(thing) {
        // alert(thing);
    }

    doFeedBack = (post, feedback) => {
        const {user} = this.props;
        if (user.data.id === 0) {
            this.navigation.navigate("Login");
            return;
        }

        const userFeedback = post.user_feedback ? post.user_feedback.action : 0;

        const likes = (feedback === 0 || feedback === 2) && userFeedback === 1 ? post.likes - 1 :
            feedback === 1 && (userFeedback === 0 || userFeedback === 2) ? post.likes + 1 : post.likes;
        const dislikes = (feedback === 0 || feedback === 1) && userFeedback === 2 ? post.dislikes - 1 :
            feedback === 2 && (userFeedback === 0 || userFeedback === 1) ? post.dislikes + 1 : post.dislikes;

        this.setState((state) => ({
            posts: state.posts.map((p) => (
                p.id === post.id
                    ? {...p, likes, dislikes, user_feedback: {action: feedback}}
                    : p
            ))
        }), () => {
            this.props.makeFeedback(user.data.id, post.id, feedback).then(response => {
                // alert(JSON.stringify(response))
            }).catch(error => {
                alert(JSON.stringify(error))
            });
        });

        // api.post.feedback(user.data.id, post.id, feedback).then(response => {
        //     alert(JSON.stringify(response))
        //
        // }).catch(err => alert(JSON.stringify(err)))
    };

    makePostsRequest(newSearch) {
        const {page} = this.state;

        const {category, user, search} = this.props;
        const {posts} = this.state;

        if (posts.length === 0) {
            this.setState({showLoading: true});
        }


        let userId = user.data ? user.data.id : 0;
        const usrId = this.props.screenProps ? this.props.screenProps.userId : null;
        if (usrId && userId !== usrId) {
            userId = usrId;
        }


        const criteria = newSearch || search ? typeof newSearch !== "undefined" ? newSearch : search : "";
        const openedProfilePage = (category === "OWN" || category === "COMMENTS" || category === "FAVORITES") && usrId;


        api.post.getPosts(userId, criteria, page, category).then(response => {
            if (!response.error) {
                this.setState((state) => ({
                    posts: page === 1 ? response.data : [...state.posts, ...response.data],
                    refreshing: false,
                    currentPage: response.current_page,
                    lastPage: response.last_page,
                    showLoading: false,
                    showFooter: false,
                    error: response.code !== 200 ? {message: response.error, code: response.code} : null
                }), () => {
                    if (!openedProfilePage) {
                        this.props.receivePosts(response, category, false);
                    }
                });
            }

        }).catch(error => {
            this.setState((state) => ({
                error,
                refreshing: false,
                showLoading: false,
                currentPage: 0,
                lastPage: 0,
                showFooter: false,
            }));
        });

    }


    componentDidMount() {

        const usrId = this.props.screenProps?  this.props.screenProps.userId : null;

        this.setState({
            posts: usrId ? [] : this.props.post
        }, () => this.makePostsRequest());

        if(this.props.screenProps) {
            this.props.screenProps.navigation.addListener('didBlur', this.stopAllMediaPlayback);
            this.props.navigation.addListener('didBlur', this.stopAllMediaPlayback);
        }

    }

    stopAllMediaPlayback = () => {
        this.state.posts.map(post => {
            this.props.playControlsMedia(String(post.id), false);
        })
    };

    onViewableItemsChanged = ({viewableItems, changed}) => {

        changed.forEach(item => {
            const {isViewable, key} = item;
            const ref = this.feedRefs[`REF_FEED_${key}`];
            this.props.playControlsMedia(key, isViewable);
             // if(ref)
             // ref.toggleItemHidden(isViewable);

        });
        /*viewableItems.forEach((item) => {
            if(isViewable) {
                console.log(key);
                const ref = this[`swiperRef_${key}`];
                if(ref) {
                    ref.togglePlayVideo;

                }
            }
        });*/
    }

    _onScroll = event => {
        const currentOffset = event.nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);

        let showHeader = 0;


        if (Math.abs(dif) < 3) {
            return;
            // showHeader = true;
            // console.log('unclear');
        } else if (dif < 0) {
            showHeader = true;
            // console.log('up', dif);
        } else {
            showHeader = false;
            // console.log('down', dif);
        }

        const {handleShowHeader, onScroll} = this.props.screenProps;
        if(onScroll) {
            onScroll(event)
        }

        // if (showHeader !== 0)
        //     handleShowHeader(showHeader, dif, currentOffset, this.offset, event);

        this.offset = currentOffset;
    };

    renderFooter = () => {
        if (!this.state.showFooter) return null;
        return (
            <View style={styles.footerLoader}>
                <ActivityIndicator animating size="large"/>
            </View>
        )
    }

    render() {
        const {refreshing, posts, showLoading} = this.state;
        // console.log(this.props.post);
        // const {post} = this.props;
        // alert(JSON.stringify(post));
        return (
            <View
                style={[styles.container, {backgroundColor: showLoading ? colors.white : '#e8eaf6'}]}>
                {showLoading ?
                    <ActivityIndicator style={styles.loader} size="large" color="#0000ff"/> :
                    <FlatList
                        scrollEventThrottle={16}
                        removeClippedSubviews={true}
                        data={this.state.posts}
                        renderItem={this.renderPost}
                        onScroll={this.props.screenProps? this.props.screenProps.onScroll: null}
                        keyExtractor={(item) => item.id.toString()}
                        onViewableItemsChanged={this.onViewableItemsChanged}
                        refreshing={refreshing}
                        onRefresh={this._handleRefresh}
                        onEndReached={this._handleLoadMore}
                        onEndReachedThreshol={100}
                        ListFooterComponent={this.renderFooter}
                    />
                }


                <ActionSheet ref="actionSheet"/>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e8eaf6',
        /*  alignItems: 'center',
          justifyContent: 'center',*/
    },
    loader: {
        marginTop: 15,
    },

    footerLoader: {
        paddingVertical: 20,
    }
});

function mapStateToProps({post, user}, ownProps) {
    return {
        post: post[ownProps.category] || [],
        user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerMedia: (id) => dispatch(mediaActions.registerMedia(id)),
        handleFavorite: (id, action) => dispatch(postActions.handleFavorite(id, action)),
        removePost: (id) => dispatch(postActions.removePost(id)),
        playControlsMedia: (id, isViewable) => dispatch(mediaActions.playControlsMedia(id, isViewable)),
        receivePosts: (response, category, error) => dispatch(postActions.receivePosts(response, category, error)),

        getPosts: (userId, criteria, page, category) => dispatch(postActions.getPosts(userId, criteria, page, category)),
        makeFeedback: (userId, postId, feedback) => dispatch(postActions.makeFeedback(userId, postId, feedback))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(PostList);