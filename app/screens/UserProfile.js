import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Dimensions, Alert} from 'react-native';

import ProfileTabs from '../navigation/ProfileTabs';

import * as dimens from '../utils/dimens';
import {theme} from '../utils/styles';
import * as colors from '../utils/colors';
import {MaterialCommunityIcons, MaterialIcons, SimpleLineIcons} from '@expo/vector-icons';
import {connect} from "react-redux";
import {api} from "../utils/api";
import SnackBar, {DURATION} from '../components/SnackBar';
import * as userActions from "../actions/user";

// import ProfilePicture from '../components/ProfilePicture';


class UserProfile extends React.PureComponent {

    constructor(props) {
        super(props);

        const {navigation} = props;
        const {user} = navigation.state.params;

        this.state = {
            showLoading: true,
            user: user || {}
        }
    }

    handleFollow = (follow) => {
        const {user} = this.props;
        this.props.follow(user.data.id, follow.id).then(response => {


            if (response.error) {
                this.refs.snackBar.show(this.getError(response.error), DURATION.LENGTH_LONG)
            }


        }).catch(err => {
            alert("error following")
        });

    };

    handleUnFollow = (follow) => {
        Alert.alert(
            'MemePot',
            `Do you want to stop following ${follow.username}?`,
            [
                {text: 'Nop', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {
                    text: 'Stop Following', onPress: () => {
                        const {user} = this.props;
                        this.props.unFollow(user.data.id, follow.id).then(response => {

                            if (response.error) {
                                this.refs.snackBar.show(this.getError(response.error), DURATION.LENGTH_LONG)
                            }

                        }).catch(err => {
                            alert("error unfollowing")
                        });
                    }
                },
            ],
            {cancelable: false}
        )

    };


    // const {data} = props.navigation.state.params;

    // const data = props.navigation.state.params ? props.navigation.state.params : null;
    // const onCommentPressed = () => {
    //     this.props.navigation.navigate("PostComments");
    // };

    _handleShowFollowers = (criteria) => {
        const {navigation} = this.props;
        const {user} = navigation.state.params;

        this.props.navigation.navigate("Followers", {criteria, userId: user.id});
    };

    componentDidMount() {
        const {navigation} = this.props;
        const {user, userId} = navigation.state.params;

        api.user.getUser(userId || user.id).then(response => {
            if (!response.error) {
                this.setState({showLoading: false, user: response.data})
            }

        }).catch(error => {

        })


    }

    render() {
        const {navigation} = this.props;
        const {user} = this.state;

        let loggedUser = this.props.user;

        const UserSummary = () => {

            return (
                <View style={styles.summary}>

                    <View style={[styles.center, styles.col, {marginStart: 0}]}>
                        <Text style={[theme.font, styles.number]}>{user.posts_count}</Text>
                        <Text style={[styles.label]}>Posts</Text>
                    </View>

                    <TouchableOpacity onPress={() => this._handleShowFollowers("FOLLOWERS")}>
                        <View style={[styles.center, styles.col]}>
                            <Text style={[theme.font, styles.number]}>{user.followers_count}</Text>
                            <Text style={[theme.font, styles.label]}>Followers</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this._handleShowFollowers("FOLLOWING")}>
                        <View style={[styles.center, styles.col]}>
                            <Text style={[theme.font, styles.number]}>{user.following_count}</Text>
                            <Text style={[theme.font, styles.label]}>Following</Text>
                        </View>
                    </TouchableOpacity>


                </View>
            );
        };

        if(!this.state.user.id) {
            return null;
        }

        return (
            <View style={[styles.container, {flex: 1}]}>
                <ScrollView
                    scrollEnabled={true} contentContainerStyle={{flex: 1}}
                    automaticallyAdjustContentInsets={true}
                    style={[styles.container, {flex: 1}]}>
                    <View style={styles.header}>
                        <TouchableOpacity style={styles.profileImgContainer}>
                            <Image style={styles.profileImg}
                                   source={user.profile_picture_url ? {uri: user.profile_picture_url} : require('../assets/img/profile.png')}/>
                        </TouchableOpacity>
                        <View style={{flex: 1,}}>
                            <View style={{
                                flexDirection: 'row',

                                justifyContent: 'space-between',
                                marginBottom: 5
                            }}>
                                <View>
                                    <Text style={[theme.font, styles.name]}>{user.name}</Text>
                                    <Text style={[theme.font, styles.userName]}>@{user.username}</Text>
                                </View>

                                <View style={[styles.headerActions]}>

                                    {loggedUser.data && loggedUser.data.id !== 0 && loggedUser.data.id !== user.id  && (
                                        <TouchableOpacity
                                            onPress={() => loggedUser.following[user.id] ? this.handleUnFollow(user) : this.handleFollow(user)}
                                            style={{margin: 5, marginRight: 15,}}>
                                            <SimpleLineIcons
                                                name={loggedUser.following[user.id] ? "user-following" : "user-follow"}
                                                size={22} color={colors.colorLink}/>
                                        </TouchableOpacity>

                                    )}

                                </View>
                            </View>

                            <UserSummary {...this.props} handleShowFollowers={this._handleShowFollowers}/>


                        </View>
                    </View>
                    <Text style={[theme.font, styles.shortBio]}>{user.short_bio}</Text>


                    <ProfileTabs onNavigationStateChange={(prevState, currentState) => {

                    }} screenProps={{userId: user.id, navigation: this.props.navigation}}/>
                </ScrollView>
            </View>

        );
    }


}


const avatarSize = 70;
var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        /*  alignItems: 'center',
          justifyContent: 'center',*/
    },

    shortBio: {
        padding: 15,
        paddingTop: 0,
        // fontWeight: 'bold',
        color: '#373C3F'
    },
    font: {},

    profileTabs: {
        borderBottomWidth: 5,

    },

    settingsIcon: {
        marginRight: 15,
    },

    headerActions: {},
    number: {
        // color: "white",
        fontSize: 18,
        fontWeight: 'bold'
    },

    name: {
        fontSize: 16,
        fontWeight: 'bold',
        // color: "white"
    },

    backgroundImg: {
        bottom: 0,
        top: 0,
        left: 0,
        right: 0,
        position: 'absolute',
        resizeMode: 'cover'
    },

    userName: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.black,

    },

    label: {
        fontWeight: 'bold',
        // color: 'grey',
        // color: colors.colorSettingsText
        // color: "white"
    },

    col: {
        marginTop: 10,
        marginLeft: width <= 320 ? 10 : 15,
        marginRight: width <= 320 ? 10 : 15,
    },

    summary: {
        flexDirection: 'row',
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    backdrop: {
        bottom: 0,
        top: 0,
        left: 0,
        right: 0,
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, .4)'
    },
    header: {
        padding: 15,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'flex-start',

    },

    profileImg: {
        width: avatarSize,
        height: avatarSize,
        borderWidth: 1,
        resizeMode: 'cover',
        borderRadius: dimens.getRadius(avatarSize),
        marginRight: 15,
        borderColor: colors.lightBlue
    },
    profileImgContainer: {
        width: avatarSize,
        height: avatarSize,
        marginEnd: 15,
    },
    addPhotoButton: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        width: 25,
        height: 25,
        borderRadius: dimens.getRadius(30),
        backgroundColor: 'red',
        bottom: 5,
        right: 0
    }


});

function mapStateToProps({user}) {
    return {
        user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        follow: (userId, followerId) => dispatch(userActions.follow(userId, followerId)),
        unFollow: (userId, followerId) => dispatch(userActions.unFollow(userId, followerId)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);